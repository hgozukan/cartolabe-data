"""
Using Pipeline API with YAML file
===================================

This notebook demonstrates the usage of `Pipeline` API by loading pipeline
from **pipeline.yaml** file.

For creating a pipeline using `Pipeline` constructor, please see the notebook
[pipeline_lisn_lsa_kmeans.py](./pipeline_lisn_lsa_kmeans.py).

YAML files that define configuration for the `Pipeline` are located in the `../conf` directory. For each dataset, there exists a directory under the conf directory, and in the dataset directory, we have `dataset.yaml` and `pipeline.yaml` files. 

We can list all the datasets where a pipeline is defined:
"""

from pathlib import Path

ROOT_DIR = Path.cwd().parent

CONF_DIR = ROOT_DIR / "conf"

""
# !ls $CONF_DIR

###############################################################################
# We will work with **lisn** dataset file. 

# !ls $CONF_DIR/lisn

###############################################################################
# Here we see both a `dataset.yaml` and `pipeline.yaml`. `dataset.yaml` is used by `pipeline.yaml` to initialize `lisn` dataset. Let's see the contents of this file: 

# !cat $CONF_DIR/lisn/dataset.yaml

###############################################################################
# Let's view the contents of the `pipeline.yaml` file:

# !cat $CONF_DIR/lisn/pipeline.yaml

###############################################################################
# Load Pipeline from YAML file
# ------------------------------------------
#
# We can load this pipeline using `cartodata.pipeline.loader.load_pipeline`.
#
# We will run this in the project's root directory (parent directory of the
# `examples` directory) as the paths in the **pipeline.yaml** file are relative
# to the project's root directory.

from cartodata.pipeline.loader import load_pipeline  # noqa
import os  # noqa
from pathlib import Path # noqa

# run in parent directory
os.chdir("../")

current_dir = Path(".").absolute()
conf_dir = current_dir / "conf"

pipeline = load_pipeline("lisn", conf_dir)

###############################################################################
# We can simply run the pipeline with the configuration specified in `pipeline.yaml` file. 
#
# We do not want to use the dump files from a previous run, so we set `force=True` and to see the results of the run, we would like to save the plots.

pipeline.run(save_plots=True, force=True)

###############################################################################
# This runs the following steps:
#
# - loads the dataset data from file
# - generates entity matrices for all natures and saves them
# - executes projection and saves the results
# - executes 2D projection and saves the results
# - creates clusters
# - finds neighbors
# - saves all data to `export.feather` file
#
# The results are saved under `pipeline.working_dir`.

for file in pipeline.working_dir.iterdir():
    print(file)

###############################################################################
# Let's view the `export.feather` file:

import pandas as pd  # noqa

df = pd.read_feather(pipeline.working_dir / "export.feather")
df.head()

###############################################################################
# As we have saved the plots, we can view them.
#
# We have 4 level clusters as defined in **pipeline.yaml** file corresponding
# to
#
# - hl_clusters,
# - ml_clusters,
# - ll_clusters,
# - vll_clusters.

for file in pipeline.working_dir.glob("*.png"):
    print(file)

""
import matplotlib.pyplot as plt  # noqa

image_names = []
for nature in pipeline.clustering.natures:
    image_title_parts = pipeline.title_parts_clus(nature)
    image_names.append("_".join(image_title_parts) + ".png")

rows = 2
columns = 2

f, ax = plt.subplots(rows, columns, figsize=(30, 10*rows))

for i, image_name in enumerate(image_names):
    img = plt.imread(pipeline.working_dir / image_name)

    row_num = i // rows
    col_num = i - row_num * columns

    ax[row_num][col_num].imshow(img)
    ax[row_num][col_num].axis('off')

plt.tight_layout()
plt.show()

###############################################################################
# We have run the pipeline with the configuration values in the **pipeline.yaml** file.

###############################################################################
# Make modifications to Pipeline loaded from YAML
# -----------------------------------------------------------------------
#
# We can make modifications on the `pipeline` instance and rerun the pipeline
# using `pipeline.run` or alternatively we can run each step individually.
#
# In the rest of this notebook, we will run each step ourselves changing some parameters.
#
# The current pipeline has natures:

pipeline.natures

###############################################################################
# In **dataset.yaml** we can see that words column uses `CorpusColumn`. Let's verify:

dataset_columns = pipeline.dataset.columns
dataset_columns

###############################################################################
# The third column of the dataset is of type `cartodata.pipeline.columns.CorpusColumn`. We will view the documentation for `CorpusColumn`.

from cartodata.pipeline.columns import CorpusColumn  # noqa

help(CorpusColumn)

###############################################################################
# CorpusColumn uses custom `PunctuationCountVectorizer` to build the n-grams.
# Let's say instead of using `PunctuationCountVectorizer` we would like to use
# `sklearn.feature_extraction.TfidfVectorizer`.
#
# We will define a custom column class as follows:

import pandas as pd  # noqa
import numpy as np  # noqa
from sklearn.feature_extraction.text import TfidfVectorizer  # noqa

from cartodata.operations import normalize_tfidf  # noqa
from cartodata.pipeline.columns import CorpusColumn  # noqa


class TfidfCorpusColumn(CorpusColumn):

    def __init__(self, nature, column_names, stopwords, english=True,
                 nb_grams=4, min_df=10, max_df=0.05, vocab_sample=None, 
                 max_features=None, strip_accents="unicode",
                 lowercase=True, min_word_length=5, normalize=True,
                 input='content', binary=True, encoding="utf-8"):

        super().__init__(nature, column_names, stopwords, english, nb_grams,
                         min_df, max_df, vocab_sample, max_features,
                         strip_accents, lowercase, min_word_length, normalize)
        self.binary = binary
        self.encoding = encoding
        self.input = input

    def load(self, dataset):

        df = dataset.df
        stopwords = self._load_stopwords(
            dataset.input_dir, self.stopwords, self.english, dataset.name
        )
        corpus = df[self.column_names].apply(
            lambda row: ' . '.join(row.values.astype(str)), axis=1)

        tf_vectorizer = TfidfVectorizer(
            input=self.input, ngram_range=(1, self.nb_grams),
            min_df=self.min_df, max_df=self.max_df, binary=True,
            strip_accents=self.strip_accents, encoding=self.encoding,
            stop_words=stopwords)

        matrix = tf_vectorizer.fit_transform(corpus)

        scores = pd.Series(np.bincount(matrix.indices,
                                       minlength=matrix.shape[1]),
                           index=tf_vectorizer.get_feature_names_out())
        if self.normalize:
            matrix = normalize_tfidf(matrix)

        return matrix, scores


###############################################################################
# `dataset_columns` contains a shallow copy of the columns of the dataset.
# Now we will initiate a new words_column to replace the existing one.

words_column = TfidfCorpusColumn(nature="words",
                                 column_names=["en_abstract_s", "en_title_s", 
                                               "en_keyword_s", "en_domainAllCodeLabel_fs"],
                                 stopwords="stopwords.txt", nb_grams=4, min_df=10, 
                                 max_df=0.05, min_word_length=5, normalize=True)

dataset_columns[4] = words_column
dataset_columns

""
pipeline.working_dir

###############################################################################
# We see that the type of the words column has changed. Now we have to set it
# to the dataset (as the dataset_columns is a shallow copy) and verify
# pipeline's natures and dataset's columns.

pipeline.dataset.set_columns(dataset_columns)

print(pipeline.natures)

""
print(pipeline.dataset.columns)

###############################################################################
# In the `pipeline.working_dir` we have the dumps from the previous run.

for file in pipeline.working_dir.iterdir():
    print(file)

###############################################################################
# We do not want to use the dumps from previous run. We can either remove the
# files or run `pipeline.generate_entity_matrices` with `force=True`.
#
# We will generate the entity matrices with the new words column and save:

matrices, scores = pipeline.generate_entity_matrices(force=True)

###############################################################################
# In the **pipeline.yaml** file, the pipeline is configured to use `cartodata.pipeline.projectionnd.LSAProjection`.

pipeline.projection_nd

###############################################################################
# Instead of LSAProjection, this time we would like to use
# `cartodata.pipeline.projectionnd.LDAProjection`.

from cartodata.pipeline.projectionnd import LDAProjection  # noqa

projection_nd = LDAProjection(num_dims=80)
pipeline.set_projection_nd(projection_nd)

matrices_nD = pipeline.do_projection_nD(force=True)

""
for file in pipeline.working_dir.glob("*.lda"):
    print(file)

###############################################################################
# Then we will project to 2D. We should not forget to set `force=True`
# to make sure that 2D projection is rerun and does not use the saved dumps.

matrices_2D = pipeline.do_projection_2D(force=True)

###############################################################################
# View the generated map:

from matplotlib.colors import TABLEAU_COLORS  # noqa
from matplotlib.lines import Line2D  # noqa

labels = tuple(pipeline.natures)
colors = list(TABLEAU_COLORS)[:len(matrices)]

pipeline.plot_map(matrices_2D, labels, colors)

###############################################################################
# Now we have all necessary matrices to create clusters and neighbors.
# First we will create clusters:

(clus_nD, clus_2D, clus_scores, cluster_labels,
cluster_eval_pos, cluster_eval_neg) = pipeline.do_clustering(force=True)

###############################################################################
# We will view only medium levels clusters:

ml_index = 1
clus_scores_ml = clus_scores[ml_index]
clus_mat_ml = clus_2D[ml_index]

fig_title = (
    f"{pipeline.dataset.name} {pipeline.dataset.version} "
    f"{pipeline.clustering.natures[ml_index]} {pipeline.projection_nd.key}"
)

fig_ml, ax_ml = pipeline.plot_map(matrices_2D, labels, colors,
                                  title=fig_title,
                                  annotations=clus_scores_ml.index,
                                  annotation_mat=clus_mat_ml)

###############################################################################
# And save the plot:

pipeline.save_plots()

###############################################################################
# Let's display two cluster images generated by two runs of the pipeline side
# by side:

image_title_parts = pipeline.title_parts_clus("ml_clusters")
img1 = plt.imread(pipeline.working_dir / image_names[1])
img2 = plt.imread(pipeline.working_dir / ("_".join(image_title_parts) + ".png"))

f, ax = plt.subplots(1, 2, figsize=(20, 10))

ax[0].imshow(img1)
ax[1].imshow(img2)

ax[0].axis('off')
ax[1].axis('off')

plt.tight_layout()
plt.show()

###############################################################################
# On the left we see the plot of medium level clusters using LSA and UMAP, and
# on the right we see the results using LDA and UMAP.
#
#
# Then we will find neighbors:

pipeline.find_neighbors()

###############################################################################
# Now we are ready to export data to **export.feather** file.

pipeline.export()

df = pd.read_feather(pipeline.working_dir / "export.feather")
df.head()

###############################################################################
# It is also possible to change the export configuration.

export_natures = pipeline.export_natures

###############################################################################
# The **pipeline.yaml** file defines export configuration for only **articles** and **authors**.

for nature in export_natures:
    print(nature.key)

###############################################################################
# Let's say we would like to add year data to **labs** nature. 
#
# The original dataset contains a column `producedDateY_i` which contains the year that the article is published. We can add this data as metadata for the point but updating column name with a more clear alternative `year`.
#
# We can map year data in the file to labs using `EntityMetadataMapColumn`.

from cartodata.pipeline.exporting import EntityMetadataMapColumn # noqa

meta_year_lab = EntityMetadataMapColumn(entity="labs", column="producedDateY_i", 
                                        as_column="year")

###############################################################################
# Then we will initialize `ExportNature` for **labs** using `meta_year_lab`.

from cartodata.pipeline.exporting import ExportNature # noqa

ex_lab = ExportNature(key="labs",
                         add_metadata=[meta_year_lab])

new_export_natures = export_natures + [ex_lab]

""
pipeline.export(new_export_natures)

""
df = pd.read_feather(pipeline.working_dir/ "export.feather")

df[df["nature"] == "labs"].head(5)
