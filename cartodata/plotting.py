import matplotlib.pyplot as plt
import matplotlib.patheffects as pe

# COLORS = ['b', 'r', 'c', 'y', 'm', 'g']
COLORS = ['blue', 'red', 'magenta', 'gold',
          'purple', 'slateblue', "orangered", "olive", "grey"]


def plot_map(matrices, labels, colors=None, title=None, annotations=None,
             annotation_mat=None, annotation_color='black'):

    if colors is None:
        # from matplotlib.colors import TABLEAU_COLORS
        # colors = list(TABLEAU_COLORS)[:len(matrices)]
        colors = list(COLORS)[:len(matrices)]

    fig, ax = plt.subplots(figsize=(12, 8), dpi=80)

    axes = []

    for i, m in enumerate(matrices):
        axes.append(ax.scatter(m[0, :], m[1, :],
                               color=colors[i],
                               s=10,
                               alpha=0.35,
                               label=labels[i]))

    # set title
    if title is not None:
        ax.set_title(title)

    # set legend
    ax.legend(tuple(axes), labels, fancybox=True, shadow=True)

    if annotations is not None and annotation_mat is not None:
        for i in range(len(annotations)):
            ax.annotate(annotations[i],
                        (annotation_mat[0, i], annotation_mat[1, i]),
                        color=annotation_color,
                        path_effects=[
                            pe.withStroke(linewidth=4, foreground="white")
            ])

    return fig, ax


def save_fig(fig, fname, **kwargs):

    fig.savefig(fname, **kwargs)
    plt.close()


def close_plots():
    plt.close('all')
