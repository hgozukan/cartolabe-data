import logging
from pathlib import Path

from cartodata.model_selection.scoring import (
    Neighbors, NeighborsND, Neighbors2D, TrustworthinessSklearn,
    TrustworthinessUmap, Clustering, FinalScore, Comparative
)
from cartodata.model_selection.utils import Result, Results
from cartodata.phases import (
    PhaseProjectionND, PhaseProjection2D, PhaseClustering, PhasePost
)
from cartodata.pipeline.columns import Columns
from cartodata.pipeline.loader import load_dataset
from cartodata.plotting import plot_map, save_fig

logger = logging.getLogger(__name__)

PARAM_LIST_DATASET = [
    "filename",
    "fileurl",
    "version",
    "prev_version"
]


class BaseExperiment:
    """A base class for an experiment.
    """
    def __init__(self,  dataset_name, dataset_version, top_dir, conf_dir,
                 input_dir, nature, source, selector, score_list=None,
                 **score_params):
        """
        Parameters
        ----------
        nature: str
        source: str
        top_dir: str or Path
            the top directory inside which experiment artifacts will be
        generated
        selector: cartodata.model_selection.selector.ParameterSelector
            the selector that provides the next set of parameters for the
        experiment
        score_list: list of cartodata.model_selection.evaluators.Evaluators,
                    default=None
        score_params: kwargs
            The parameters that will be used by evaluators should be specified
            as kwargs in the format ``evaluator_key__parameter_name=value``
            For example ``name_list`` parameter for ``final_score`` should be
            specified as:

            ``final_score__name_list=["neighbors_nd_articles_authors",
                                      "neighbors_2d_articles_authors",
                                      "clu_score"]

        """
        assert nature is not None and nature != ""
        assert source is not None and source != ""
        assert top_dir is not None and top_dir != ""
        assert selector is not None

        self.dataset_name = dataset_name
        self.dataset_version = dataset_version
        self.conf_dir = conf_dir
        self.input_dir = input_dir

        self.nature = nature
        self.source = source

        self.top_dir = Path(top_dir)
        self.score_dir = self.top_dir / "scores"
        if not self.score_dir.exists():
            self.score_dir.mkdir(parents=True)

        self.selector = selector
        self.score_list = score_list

        self.score_params = score_params
        self.results = Results(self.score_dir)

    def _load_dataset(self, params):
        """
        Gets dataset name from next_params and loads the `dataset_name.yaml`
        file in conf_dir. Updates parameters of the dataset columns with the
        values specified in next_params.

        For a column type, a column parameter in the specified in `params` will
        update the parameter value in all columns of the same type. For
        example, if `filter_min_score` field exists in the `params`, it will
        update `filter_min_score` value for all columns of type Column.CS. It
        is also possible to update only certain column's value by specifying
        `nature_filter_min_score`. It will update the `filter_min_score` value
        only for the column with `nature` and type Columns.CS.

        For example `authors_filter_min_score` will update only for the columns
        whose nature is `authors` and type is `Columns.CS`.

        For each column type, the field that can be updated by params are as
        follows:

        Columns.CS type
        ---------------
        - `filter_min_score`

        Columns.CORPUS
        --------------
        -  `min_df`
        - `max_df`
        - `nb_grams`
        - `max_features`
        - `min_word_length`
        - `vocab_sample`

        Columns.FILTER
        --------------
        - `filter_value`

        Parameters
        ----------
        params: dict
            set of parameters to be used for configuring dataset columns.

        Returns
        -------
        cartodata.pipeline.datasets.Dataset
        """
        dataset = load_dataset(self.dataset_name, self.conf_dir, False)
        dataset.version = self.dataset_version

        for column in dataset.columns:
            if column.type == Columns.CS:
                self._set_column_value(params, column, "filter_min_score")
            elif column.type == Columns.CORPUS:
                self._set_column_value(params, column, "column_names")
                self._set_column_value(params, column, "min_df")
                self._set_column_value(params, column, "max_df")
                self._set_column_value(params, column, "vocab_sample")
                self._set_column_value(params, column, "max_features")
                self._set_column_value(params, column, "min_word_length")
                self._set_column_value(params, column, "nb_grams")
            elif column.type == Columns.FILTER:
                self._set_column_value(params, column, "filter_value")

        dataset_params = self._load_kwargs(params, PARAM_LIST_DATASET)

        fileurl = dataset_params.get("fileurl", None)
        filename = dataset_params.get("filename", None)

        if fileurl is not None:
            dataset.fileurl = fileurl
        if filename is not None:
            dataset.filename = filename

        return dataset

    def _set_column_value(self, params, column, field):
        """Checks if the `nature_field` exists in the specified `params`. If
        it does not exist, checks if  `field` exists in the `params`. If
        a value is found, updates the column's field value with value.
        """
        field_value = params.get(
                f"{column.nature}__{field}", None
        )
        if field_value is None:
            field_value = params.get(field, None)
        if field_value != "None" and field_value is not None:
            setattr(column, field, field_value)

    def _load_kwargs(self, next_params, param_list):
        kwargs = {}
        for param in param_list:
            value = next_params.get(param, None)
            if value is not None and value != 'None':
                kwargs[param] = value
        return kwargs

    def run(self, nbmax):
        """Runs the experiment for the specified `nbmax` number of times.

        :param nbmax: maximum number of experiments to run.
        """

        # sets number of executed rows in the params so that it is not
        # considered in stop condition with nbmax while running the experiment.
        self.selector.set_initial_executed()

        while not self.selector.stop(nbmax):
            logger.info("######### Running next set of parameters ##########")
            next_params = self.selector.next()
            logger.info(next_params)

            self.result = Result()
            self.run_steps(next_params)

            logger.info("------ Finished run for current parameters --------")

        self.selector.to_csv(self.top_dir / "experiment.csv")
        logger.info("Completed run!")
        return self.results

    def run_steps(next_params):
        raise NotImplementedError

    def _score_exists_in_list(self, score):
        if self.score_list is None or score in self.score_list:
            return True
        return False

    def add_nD_scores(self, key_nD=None, dir_mat=None, dir_nD=None,
                      run_params={}):
        logger.info("Running nD scoring.....")
        phase_result = Result()

        self.can_reuse_scores = True
        # NeighborsND
        if self._score_exists_in_list(
                Neighbors) or self._score_exists_in_list(NeighborsND):
            neighbors_score_params = Neighbors.get_kwargs(self.score_params)

            # Load Neighbors scores from file for PhaseProjectionND
            result = Neighbors.load_scores_from_file(
                PhaseProjectionND, self.nature, self.source, dir_nD
            )
            if result is None or (
                    result.score_params != neighbors_score_params):
                result = Neighbors.load_and_evaluate(
                        self.nature, self.source, key_nD, dir_mat, dir_nD,
                        **neighbors_score_params
                )
                result.append_run_params(run_params)
                result.append_score_params(neighbors_score_params)
                self.can_reuse_scores = False

            self.persist_result(result, PhaseProjectionND, phase_result,
                                dir_nD)

        return phase_result

    def add_2D_scores(self, all_natures, dir_mat, key_nD, key_2D, dir_nD,
                      dir_2D, plots=None, run_params={}):
        logger.info("Running 2D scoring.....")
        phase_result = Result()

        # Neighbors2D
        if self._score_exists_in_list(
                Neighbors) or self._score_exists_in_list(Neighbors2D):
            neighbors_score_params = Neighbors.get_kwargs(self.score_params)

            # Load Neighbors scores from file for PhaseProjection2D
            result = Neighbors.load_scores_from_file(
                PhaseProjection2D, self.nature, self.source, dir_2D
            )
            if result is None or not self.can_reuse_scores or (
                    result.score_params != neighbors_score_params):
                result = Neighbors.load_and_evaluate(
                        self.nature, self.source, key_2D, dir_mat, dir_2D,
                        **neighbors_score_params
                )

                result.append_run_params(run_params)
                result.append_score_params(neighbors_score_params)
                self.can_reuse_scores = False

            self.persist_result(result, PhaseProjection2D, phase_result,
                                dir_2D)

        # Comparative
        if self._score_exists_in_list(Comparative):
            name = f"{Neighbors.KEY}_{self.nature}_{self.source}"
            key1 = PhaseProjectionND.prefix(name)
            key2 = PhaseProjection2D.prefix(name)

            # Load Comparative scores from file for PhaseProjection2D
            result = Comparative.load_scores_from_file(
                PhaseProjection2D, key1, key2, dir_2D
            )
            if result is None or not self.can_reuse_scores:
                assert (phase_result.has_score(key2) and
                        self.result.has_score(key1)), (
                            f"{key1} and {key2} should be calculated for"
                            " comparative results!"
                )
                scores1 = self.result.raw_scores[key1]
                scores2 = phase_result.raw_scores[key2]

                result = Comparative.evaluate(
                        key1, key2, scores1, scores2
                    )
                self.can_reuse_scores = False

            self.persist_result(result, PhaseProjection2D, phase_result,
                                dir_2D)

        # TrustworthinessSklearn
        if self._score_exists_in_list(TrustworthinessSklearn):
            trust_score_params = TrustworthinessSklearn.get_kwargs(
                self.score_params
            )
            # Load Trustworthiness scores from file for PhaseProjection2D
            result = TrustworthinessSklearn.load_scores_from_file(
                PhaseProjection2D, dir_2D
            )
            # TODO should get score params from file and compare
            if result is None or not self.can_reuse_scores:
                result = TrustworthinessSklearn.load_and_evaluate(
                    all_natures, key_nD, key_2D, dir_nD, dir_2D,
                    **trust_score_params
                )
                result.append_run_params(run_params)
                result.append_score_params(trust_score_params)

            self.persist_result(result, PhaseProjection2D, phase_result,
                                dir_2D)

        # TrustworthinessUmap
        if self._score_exists_in_list(TrustworthinessUmap):
            trust_score_params = TrustworthinessUmap.get_kwargs(
                self.score_params
            )
            # Load Trustworthiness scores from file for PhaseProjection2D
            result = TrustworthinessUmap.load_scores_from_file(
                PhaseProjection2D, dir_2D
            )
            if result is None or not self.can_reuse_scores:
                result = TrustworthinessUmap.load_and_evaluate(
                    all_natures, key_nD, key_2D, dir_nD, dir_2D,
                    **trust_score_params
                )
                result.append_run_params(run_params)
                result.append_score_params(trust_score_params)

            self.persist_result(result, PhaseProjection2D, phase_result,
                                dir_2D)

        if plots is not None and plots != []:
            self.add_plots_to_scoring(plots, ["2D"])

        return phase_result

    def add_clustering_scores(self, all_natures, cluster_natures, key_nD,
                              key_2D, dir_mat, dir_nD, dir_2D, dir_clus,
                              words_index, plots=None, run_params={}):
        logger.info("Running clustering scoring.....")
        phase_result = Result()

        if self._score_exists_in_list(Clustering):
            cluster_score_params = Clustering.get_kwargs(self.score_params)

            # Load Clustering scores from file for PhaseClustering
            result = Clustering.load_scores_from_file(
                PhaseClustering, cluster_natures, dir_clus
            )
            if result is None or not self.can_reuse_scores or (
                    result.score_params != cluster_score_params
            ):
                result = Clustering.load_and_evaluate(
                    all_natures, cluster_natures, key_nD, key_2D, dir_mat,
                    dir_nD, dir_2D, words_index, dir_clus,
                    **Clustering.get_kwargs(self.score_params)
                )
                result.append_run_params(run_params)
                result.append_score_params(cluster_score_params)
                self.can_reuse_scores = False

            self.persist_result(result, PhaseClustering, phase_result,
                                dir_clus)

        if plots is not None and plots != []:
            self.add_plots_to_scoring(plots, cluster_natures)

        return phase_result

    def add_post_scores(self, dir_clus, run_params={}):
        logger.info("Running post scoring.....")
        phase_result = Result()

        if self._score_exists_in_list(FinalScore):
            final_score_params = FinalScore.get_kwargs(self.score_params)
            # Load Final score from file PhasePost
            result = FinalScore.load_scores_from_file(PhasePost, dir_clus)

            if result is None or not self.can_reuse_scores or (
                    result.score_params != final_score_params):
                result = FinalScore.evaluate(self.result.scores,
                                             **final_score_params)

                result.append_run_params(run_params)
                result.append_score_params(final_score_params)
                self.can_reuse_scores = False

            self.persist_result(result, PhasePost, phase_result, dir_clus)

        return phase_result

    def save_plots(self, natures, matrices, dir_dump, title_parts,
                   annotations=None, annotation_mat=None, file_ext=".png"):
        labels = tuple(natures)

        fig_title = " ".join(title_parts)
        fig, ax = plot_map(matrices, labels, title=fig_title,
                           annotations=annotations,
                           annotation_mat=annotation_mat)

        filename = fig_title.replace(" ", "_") + file_ext
        filepath = dir_dump / filename
        fig.savefig(filepath)

        return (fig, filename)

    def add_plots_to_scoring(self, plots, natures):
        for i, nature in enumerate(natures):
            plot_i = plots[i]
            fig = plot_i[0]
            fname = plot_i[1]
            fig_dir = self.score_dir / nature
            if not fig_dir.exists():
                fig_dir.mkdir()
            save_fig(fig, (fig_dir / fname))

    def persist_result(self, result, phase, phase_result, dump_dir):
        result.prefix(phase)
        result.persist_desc_scores(dump_dir)
        result.persist_raw_scores(dump_dir)

        phase_result.append(result)

    def finish_iteration(self, next_params, dir_clus):
        id_dict = {"id": next_params["id"]}
        id_dict.update(self.result.run_params)
        self.result.run_params = id_dict
        dir_dump = dir_clus.relative_to(self.top_dir.parent)
        self.results.append(self.result, dir_dump)

        return self.results
