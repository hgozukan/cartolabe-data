from pytest import approx
from unittest import TestCase

# import numpy as np
import pandas as pd

from cartodata.phases import (
    PhaseProjectionND, PhaseProjection2D, PhaseClustering, PhasePost
)
from cartodata.model_selection.scoring import (
    Neighbors, Comparative,
    # TrustworthinessSklearn, TrustworthinessUmap,
    Clustering, FinalScore
)
from cartodata.neighbors import NEIGHBORS_FILENAME_FORMAT
from cartodata.operations import (
    load_matrices_from_dumps, load_scores
)
from cartodata.pipeline.clustering import KMeansClustering

from cartodata.tests.conftest import (
    DESC_SCORES_ND_INDEX, DESC_SCORES_2D_INDEX,
    DATASET_DIR, FILES_DIR, NATURE, SOURCE
)

NATURES = [
    'articles', 'authors', 'teams', 'labs', 'words'
]
WORDS_INDEX = 4

HL_INDEX = pd.Index([
        "interaction, cognitive science",
        "social, language",
        "problem, mathematics",
        "learning, physics",
        "computing, query",
        "networking internet architecture, quantitative methods",
        "optimization, evolutionary",
        "logic science, formal",
    ])

hl_cluster_series = pd.Series(
    index=HL_INDEX,
    data=[586, 518, 513, 474, 774, 416, 537, 444]
)

hl_cluster_labels = [
    'social', 'language', 'learning', 'physics', 'computing', 'query',
    'networking internet architecture', 'quantitative methods', 'interaction',
    'cognitive science', 'logic science', 'formal', 'problem', 'mathematics',
    'optimization', 'evolutionary'
]

hl_cluster_eval_pos = pd.Series(
    index=HL_INDEX,
    data=[0.500000, 0.814672, 0.555556, 0.337553,
          0.801034, 0.545673, 0.713222, 0.738739]
)

hl_cluster_eval_neg = pd.Series(
    index=HL_INDEX,
    data=[0.106922, 0.129771, 0.130913, 0.015143,
          0.072077, 0.049943, 0.243625, 0.123422]
)

ML_INDEX = pd.Index([
    'design, visualizations',
    'logic science, proof',
    'learning',
    'mathematics, problem',
    'networking internet architecture, protocols',
    'visualization, image',
    'interaction',
    'sciences, biological',
    'optimization, monte carlo search',
    'artificial intelligence, semantic',
    'performance, programming languages',
    'physics',
    'social, wikipedia',
    'graph',
    'query, quantum',
    'systems, modeling simulation',
    'cluster computing, services',
    'evolutionary, population',
    'matrix adaptation evolution strategy, multi armed',
    'language',
    'structure, quantitative methods',
    'testing, linear systems',
    'movement, speech',
    'mobile robots, robots'], dtype='object')

ml_cluster_labels = [
    'artificial intelligence', 'semantic',
    'matrix adaptation evolution strategy', 'multi armed', 'mathematics',
    'problem', 'design', 'visualizations', 'logic science', 'proof',
    'networking internet architecture', 'protocols', 'learning',
    'optimization', 'monte carlo search', 'sciences', 'biological', 'systems',
    'modeling simulation', 'movement', 'speech', 'physics', 'testing',
    'linear systems', 'visualization', 'image', 'mobile robots', 'robots',
    'interaction', 'query', 'quantum', 'cluster computing', 'services',
    'language', 'social', 'wikipedia', 'graph', 'performance',
    'programming languages', 'evolutionary', 'population', 'structure',
    'quantitative methods'
]

RANDOM_STATE = 42


class NeighborsBase():

    def test_evaluate(self):
        # Test for recompute=True
        scores_source = load_scores([SOURCE], DATASET_DIR)[0]
        matrix_source = load_matrices_from_dumps([SOURCE], "mat",
                                                 DATASET_DIR)[0]

        neighbors_filename = (
            DATASET_DIR / NEIGHBORS_FILENAME_FORMAT.format(
                NATURE, NATURE
            )
        )

        scores_nature = load_scores([NATURE], DATASET_DIR)[0]
        matrix_nature_xD = load_matrices_from_dumps(
                [NATURE], self.projection_key, DATASET_DIR
        )[0]
        result = Neighbors.evaluate(
            NATURE, SOURCE, matrix_source, scores_source,
            neighbors_filename, DATASET_DIR,
            scores_nature, matrix_nature_xD,
            min_score=30, recompute=True
        )
        self.check_result(result)

        # Test for recompute=False
        scores_nature = load_scores([NATURE], DATASET_DIR)[0]
        matrix_nature_xD = load_matrices_from_dumps(
                [NATURE], self.projection_key, DATASET_DIR
        )[0]
        result = Neighbors.evaluate(
            NATURE, SOURCE, matrix_source, scores_source,
            neighbors_filename, DATASET_DIR,
            min_score=30, recompute=False
        )
        self.check_result(result)

    def test_sample(self):
        min_score = 30
        sample_size = 20
        scores_source = load_scores([SOURCE], DATASET_DIR)[0]
        matrix_source = load_matrices_from_dumps([SOURCE], "mat",
                                                 DATASET_DIR)[0]
        use = scores_source >= min_score

        source_transpose, scores_source_r, use_r = Neighbors.sample(
            matrix_source.T, scores_source, use, sample_size=sample_size,
            min_score=min_score, random_state=42
        )
        # Albert Cohen is the first entry in the scores_source_r
        # Gets its original position in scores_matrix and compare
        # corresponding row in transpose of matrix_source and source_transpose
        loc_original = scores_source.index.get_loc("Albert Cohen")
        assert (matrix_source.T.tolil().rows[loc_original] ==
                source_transpose.tolil().rows[0])

        rows = source_transpose.tolil().rows
        for row in rows:
            assert len(row) >= min_score

        assert len(scores_source_r) == sample_size
        assert source_transpose.shape[0] == len(scores_source_r)
        assert len(use_r) == len(scores_source_r)


class TestNeighborsND(NeighborsBase, TestCase):
    """A test class to test
    cartodata.model_selection.scoring.NeighborsND class."""

    def setUp(self):
        self.projection_key = "lsa"

    def test_load_scores_from_file(self):
        score_name = "neighbors_articles_authors"
        result = Neighbors.load_scores_from_file(
            PhaseProjectionND, NATURE, SOURCE, FILES_DIR,
            filename="phase2_scores_all.csv"
        )
        assert len(result.scores) == 1
        assert result.scores[score_name] == approx(0.5468, abs=1e-4)

        assert len(result.desc_scores) == 1
        desc_scores = result.desc_scores[f"{score_name}_det"]
        assert desc_scores[1] == approx(0.5468, abs=1e-4)
        assert len(desc_scores[0]) == 48
        assert result.run_params == {'key': 'bert',
                                     'num_dims': 768,
                                     'normalize': True,
                                     'family': 'all-MiniLM-L6-v2',
                                     'max_length': 256}
        assert result.score_params == {'min_score': 30,
                                       'recompute': True,
                                       'sample_size': None,
                                       'n_neighbors': 10,
                                       'random_state': 42}
        assert len(result.raw_scores) == 1
        assert len(result.raw_scores[score_name]) == 48

        result = Neighbors.load_scores_from_file(
            PhaseProjectionND, NATURE, SOURCE, FILES_DIR,
            filename="scores_all.csv"
        )
        assert result is None

    def check_result(self, result):
        score_name = f"{Neighbors.KEY}_{NATURE}_{SOURCE}"
        result.scores[score_name] == approx(0.48, abs=1e-2)

        name_desc = f"{score_name}_det"
        desc_scores, value = result.desc_scores[name_desc]
        assert value == approx(0.48, abs=1e-2)

        # checks only first 3 values as following these values, sort order
        # might change for the authors with same score
        for i, value in enumerate(desc_scores.index[:3]):
            assert DESC_SCORES_ND_INDEX[i] == value


class TestNeighbors2D(NeighborsBase, TestCase):
    """A test class to test
    cartodata.model_selection.scoring.Neighbors2D class."""

    def setUp(self):
        self.projection_key = "umap"

    def test_load_scores_from_file(self):
        score_name = "neighbors_articles_authors"
        result = Neighbors.load_scores_from_file(
            PhaseProjection2D, NATURE, SOURCE, FILES_DIR,
            filename="phase3_scores_all.csv"
        )
        assert len(result.scores) == 1
        assert result.scores[score_name] == approx(0.5289, abs=1e-4)

        assert len(result.desc_scores) == 1
        desc_scores = result.desc_scores[f"{score_name}_det"]
        assert desc_scores[1] == approx(0.5289, abs=1e-4)
        assert len(desc_scores[0]) == 48
        assert result.run_params == {'key': 'umap',
                                     'n_neighbors': 10,
                                     'min_dist': 0.1,
                                     'metric': 'euclidean',
                                     'init': 'spectral',
                                     'learning_rate': 1.0,
                                     'repulsion_strength': 1.0}
        assert result.score_params == {'min_score': 30,
                                       'recompute': True,
                                       'sample_size': None,
                                       'n_neighbors': 10,
                                       'random_state': 42}
        assert len(result.raw_scores) == 1
        assert len(result.raw_scores[score_name]) == 48

        result = Neighbors.load_scores_from_file(
            PhaseProjection2D, NATURE, SOURCE, FILES_DIR,
            filename="scores_all.csv"
        )
        assert result is None

    def check_result(self, result):
        score_name = f"{Neighbors.KEY}_{NATURE}_{SOURCE}"
        result.scores[score_name] == approx(0.2469, abs=1e-2)

        name_desc = f"{score_name}_det"
        desc_scores, value = result.desc_scores[name_desc]
        assert value == approx(0.4442, abs=1e-2)

        # checks only first 3 values as following these values, sort order
        # might change for the authors with same score
        for i, value in enumerate(desc_scores.index[:3]):
            assert DESC_SCORES_2D_INDEX[i] == value


# class TestTrustworthinessSklearn(TestCase):
#     """A test class to test
#     cartodata.model_selection.scoring.TrustworthinessSklearn
#     class."""

#     def test_load_scores_from_file(self):
#         score_name = TrustworthinessSklearn.KEY
#         result = TrustworthinessSklearn.load_scores_from_file(
#             PhaseProjection2D, FILES_DIR, filename="phase3_scores_all.csv"
#         )
#         assert len(result.scores) == 1
#         assert result.scores[score_name] == approx(0.9495, abs=1e-4)

#         assert len(result.desc_scores) == 0
#         assert len(result.raw_scores) == 0

#         # TODO this should be fixed, find a way to persist score params.
#         assert result.score_params == {}

#         result = TrustworthinessSklearn.load_scores_from_file(
#             PhaseProjection2D, FILES_DIR, filename="scores_all.csv"
#         )
#         assert result is None

#     def test_evaluate(self):
#         matrices_nD = load_matrices_from_dumps(
#             ['articles', 'authors', 'teams', 'labs', 'words'], "lsa",
#             DATASET_DIR
#         )
#         matrices_2D = load_matrices_from_dumps(
#             ['articles', 'authors', 'teams', 'labs', 'words'], "umap",
#             DATASET_DIR
#         )

#         global_matrix_nD = np.asarray(np.hstack(matrices_nD).T)
#         global_matrix_2D = np.asarray(np.hstack(matrices_2D).T)

#         n_neighbors = 5
#         metric = "euclidean"
#         result = TrustworthinessSklearn.evaluate(
#             global_matrix_nD, global_matrix_2D,
#             n_neighbors=n_neighbors, metric=metric
#         )
#         score_name = TrustworthinessSklearn.KEY
#         assert result.scores[score_name] == approx(0.98, abs=1e-2)


# class TestTrustworthinessUmap(TestCase):
#     """A test class to test
#     cartodata.model_selection.scoring.TrustworthinessUmap class."""

#     def test_load_scores_from_file(self):
#         score_name = TrustworthinessUmap.KEY
#         result = TrustworthinessUmap.load_scores_from_file(
#             PhaseProjection2D, FILES_DIR, filename="phase3_scores_all.csv"
#         )
#         assert len(result.scores) == 1
#         assert result.scores[score_name] == approx(0.8971, abs=1e-4)

#         assert len(result.raw_scores) == 1

#         # TODO this should be fixed, find a way to persist score params.
#         assert result.score_params == {}

#         result = TrustworthinessUmap.load_scores_from_file(
#             PhaseProjection2D, FILES_DIR, filename="scores_all.csv"
#         )
#         assert result is None

#     def test_evaluate(self):
#         matrices_nD = load_matrices_from_dumps(
#             ['articles', 'authors', 'teams', 'labs', 'words'], "lsa",
#             DATASET_DIR
#         )
#         matrices_2D = load_matrices_from_dumps(
#             ['articles', 'authors', 'teams', 'labs', 'words'], "umap",
#             DATASET_DIR
#         )

#         global_matrix_nD = np.asarray(np.hstack(matrices_nD).T)
#         global_matrix_2D = np.asarray(np.hstack(matrices_2D).T)

#         n_neighbors = 5
#         metric = "euclidean"
#         scores = [1., 0.9819, 0.9818, 0.9819, 0.9819, 0.9211]

#         result = TrustworthinessUmap.evaluate(
#             global_matrix_nD, global_matrix_2D,
#             n_neighbors=n_neighbors, metric=metric
#         )
#         score_name = TrustworthinessUmap.KEY
#         assert result.scores[score_name] == approx(0.92, abs=1e-2)
#         assert np.allclose(result.raw_scores[score_name], scores, atol=1e-4)


class TestClustering(TestCase):
    """A test class to test cartodata.model_selection.scoring.Clustering
    class."""

    def setUp(self):
        self.scores = load_scores(NATURES, DATASET_DIR)
        self.matrices = load_matrices_from_dumps(NATURES, "mat", DATASET_DIR)
        self.matrices_2D = load_matrices_from_dumps(NATURES, "umap",
                                                    DATASET_DIR)
        self.matrices_nD = load_matrices_from_dumps(NATURES, "lsa",
                                                    DATASET_DIR)

    def test_load_scores_from_file(self):
        cluster_natures = ["hl", "ll"]
        result = Clustering.load_scores_from_file(
            PhaseClustering, cluster_natures, FILES_DIR,
            filename="phase4_scores_all.csv"
        )

        score_names, desc_score_names = Clustering.get_score_keys(
            len(cluster_natures)
        )
        assert len(result.scores) == len(score_names)
        # assert result.scores[score_name] == approx(0.5290, abs=1e-4)

        assert len(result.desc_scores) == len(desc_score_names)
        desc_scores = result.desc_scores[f"{desc_score_names[0]}_det"]
        assert desc_scores[1] == approx(0.4403, abs=1e-4)
        assert len(desc_scores[0]) == 9

        desc_scores = result.desc_scores[f"{desc_score_names[1]}_det"]
        assert desc_scores[1] == approx(0.5712, abs=1e-4)
        assert len(desc_scores[0]) == 27

        assert result.run_params == {'key': 'hdbscan', 'base_factor': 3}
        assert result.score_params == {'iter_stab': 2,
                                       'remove_stab': [0, 0.01, 0.03,
                                                       0.1, 0.25],
                                       'metric': 'euclidean',
                                       'random_state': None}

        result = Neighbors.load_scores_from_file(
            PhaseProjection2D, NATURE, SOURCE, FILES_DIR,
            filename="scores_all.csv"
        )
        assert result is None

    def test_evaluate(self):
        nb_clusters = 8
        clustering = KMeansClustering(n=nb_clusters, base_factor=3,
                                      natures=["hl_clusters"],
                                      random_state=RANDOM_STATE)

        (c_nD, c_2D, c_scores, cluster_labels,
         cluster_eval_pos, cluster_eval_neg) = clustering.create_clusters(
             self.matrices,
             self.matrices_2D,
             self.matrices_nD,
             self.scores,
             WORDS_INDEX
             )

        result = Clustering.evaluate(
            cluster_labels, clustering_table=self.matrices_2D[0],
            naming_table=self.matrices_2D[WORDS_INDEX],
            natural_space_naming_table=self.matrices[WORDS_INDEX],
            naming_scores=self.scores[WORDS_INDEX],
            naming_profile_table=self.matrices_nD[WORDS_INDEX],
            c_scores=c_scores, cluster_eval_pos=cluster_eval_pos,
            cluster_eval_neg=cluster_eval_neg, iter_stab=2,
            remove_stab=[0, .01, .03, .1, .25],  metric='euclidean',
            random_state=RANDOM_STATE
        )

        scores = result.scores

        assert scores['nb_clust_0'] == nb_clusters
        self.assertAlmostEqual(scores['silhouette_0'], 0.43569416)
        self.assertAlmostEqual(scores['avg_word_couv_0'], 0.62580589)
        self.assertAlmostEqual(scores['med_word_couv_0'], 0.63438857)
        self.assertAlmostEqual(scores['avg_word_couv_minus_0'], 0.51682870)
        self.assertAlmostEqual(scores['big_small_ratio_0'], 1.860576923)
        self.assertAlmostEqual(scores["stab_clus_0"], 0.55)

        desc_scores = result.desc_scores

        frame, value = desc_scores["clus_eval_pos_0_det"]
        assert frame.index.equals(HL_INDEX)
        self.assertAlmostEqual(value, 0.62580589)

        # 24 clusters
        nb_clusters = 24
        clustering = KMeansClustering(n=nb_clusters, base_factor=3,
                                      natures=["ml_clusters"],
                                      random_state=RANDOM_STATE)

        (c_nD, c_2D, c_scores, cluster_labels,
         cluster_eval_pos, cluster_eval_neg) = clustering.create_clusters(
             self.matrices,
             self.matrices_2D,
             self.matrices_nD,
             self.scores,
             WORDS_INDEX
             )

        result = Clustering.evaluate(
            cluster_labels, clustering_table=self.matrices_2D[0],
            naming_table=self.matrices_2D[WORDS_INDEX],
            natural_space_naming_table=self.matrices[WORDS_INDEX],
            naming_scores=self.scores[WORDS_INDEX],
            naming_profile_table=self.matrices_nD[WORDS_INDEX],
            c_scores=c_scores, cluster_eval_pos=cluster_eval_pos,
            cluster_eval_neg=cluster_eval_neg, iter_stab=2,
            remove_stab=[0, .01, .03, .1, .25],  metric='euclidean',
            random_state=RANDOM_STATE
        )

        scores = result.scores

        assert scores['nb_clust_0'] == nb_clusters
        self.assertAlmostEqual(scores['silhouette_0'], 0.4400361)
        self.assertAlmostEqual(scores['avg_word_couv_0'], 0.73771741)
        self.assertAlmostEqual(scores['med_word_couv_0'], 0.76213387)
        self.assertAlmostEqual(scores['avg_word_couv_minus_0'], 0.6514651)
        self.assertAlmostEqual(scores['big_small_ratio_0'], 8.375)
        self.assertAlmostEqual(scores["stab_clus_0"], 0.54166666)

        desc_scores = result.desc_scores

        frame, value = desc_scores["clus_eval_pos_0_det"]
        assert frame.index.equals(ML_INDEX)
        self.assertAlmostEqual(value, 0.73771741)


class TestComparative(TestCase):
    """A test class to test
    cartodata.model_selection.scoring.Comparative
    class."""

    def test_load_scores_from_file(self):
        name = "neighbors_articles_authors"
        key1 = PhaseProjectionND.prefix(name)
        key2 = PhaseProjection2D.prefix(name)
        score_base_name = Comparative.score_base_name(key1, key2)

        result = Comparative.load_scores_from_file(
            PhaseProjection2D, key1, key2, FILES_DIR,
            filename="phase3_scores_all.csv"
        )
        assert len(result.scores) == 2
        keys = list(result.scores.keys())
        assert result.scores[keys[0]] == approx(-0.0179, abs=1e-4)
        assert result.scores[keys[1]] == approx(-0.0193, abs=1e-4)

        assert len(result.desc_scores) == 1
        desc_scores = result.desc_scores[f"{score_base_name}_det"]
        assert desc_scores[1] == approx(-0.0179, abs=1e-4)
        assert len(desc_scores[0]) == 48
        assert result.run_params == {}
        assert result.score_params == {}
        assert len(result.raw_scores) == 1
        assert result.raw_scores[score_base_name].shape == (48, 3)

        result = Comparative.load_scores_from_file(
            PhaseProjection2D, key1, key2, FILES_DIR,
            filename="scores_all.csv"
        )
        assert result is None

    def test_evaluate(self):
        key = "key"
        key1 = f"phase1__{key}"
        key2 = f"phase2__{key}"
        scores1 = pd.Series([1, 2, 3, 4])
        scores2 = pd.Series([4, 8, 4, 3])
        diff = pd.Series(["3 : -1.00", "2 : 1.00", "0 : 3.00", "1 : 6.00"],
                         index=[3, 2, 0, 1])

        result = Comparative.evaluate(key1, key2, scores1, scores2)

        name = f"phase1_phase2_{key}"
        assert result.scores[f"{name}_dim_mean"] == approx(2.25)
        assert result.scores[f"{name}_dim_median"] == approx(2.0)

        scdec, value = result.desc_scores[f"{name}_dim_det"]
        assert scdec.equals(diff)


class TestFinalScore(TestCase):
    """A test class to test
    cartodata.model_selection.scoring.FinalScore
    class."""

    def test_load_scores_from_file(self):
        score_name = FinalScore.KEY
        result = FinalScore.load_scores_from_file(
            PhasePost, FILES_DIR, filename="phase4_scores_all.csv"
        )
        assert len(result.scores) == 1
        assert result.scores[score_name] == approx(0.4638, abs=1e-4)

        assert len(result.desc_scores) == 1
        desc_scores = result.desc_scores[f"{score_name}_det"]
        assert desc_scores[1] == approx(0.4638, abs=1e-4)
        assert len(desc_scores[0]) == 3
        assert result.score_params == {'name_list':
                                       ['2_nD__neighbors_articles_authors',
                                        '3_2D__neighbors_articles_authors',
                                        '4_clus__clu_score']}

        result = FinalScore.load_scores_from_file(
            PhasePost, FILES_DIR, filename="scores_all.csv"
        )
        assert result is None

    def test_evaluate(self):
        scores = {
            "score1": 15,
            "score2": 25,
            "score3": 30
        }
        result = FinalScore.evaluate(scores, name_list=["score1", "score2"])

        score_name = FinalScore.KEY
        assert result.scores[score_name] == 20
        df = result.desc_scores[f"{score_name}_det"][0]
        df.iloc[0] = 15
        df.iloc[1] = 25
        assert result.desc_scores[f"{score_name}_det"][1] == 20

        result = FinalScore.evaluate(scores,
                                     name_list=["score1", "score2", "score3"])
        assert result.scores[score_name] == approx(23.33, abs=1e-2)
        df = result.desc_scores[f"{score_name}_det"][0]
        df.iloc[0] = 15
        df.iloc[1] = 25
        df.iloc[1] = 30
        assert result.desc_scores[f"{score_name}_det"][1] == approx(
            23.33, abs=1e-2
        )
