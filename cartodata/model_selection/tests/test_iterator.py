import pandas as pd
from pathlib import Path
import pytest
from unittest import TestCase

from cartodata.model_selection.iterator import (
    RandomIterator, GridIterator
)


class ParameterIteratorBase():

    def test_cons(self):
        assert self.params_iterator1.params_frame.equals(
            self.params_iterator3.params_frame
        )

        assert "selected" in self.params_iterator1.params_frame.columns
        assert "id" in self.params_iterator1.params_frame.columns
        assert "selected" in self.params_iterator2.params_frame.columns
        assert "id" in self.params_iterator2.params_frame.columns
        assert "selected" in self.params_iterator3.params_frame.columns
        assert "id" in self.params_iterator3.params_frame.columns

    def test_is_dict_correct(self):
        dict_correct = {
            "robustseed": [0],
            "authors_filter_min_score": [4],
            "filter_min_score": [6],
            "projection_nD": ["lsa"],
            "num_dims": [50, 100],
        }

        dict_incorrect1 = {
            "robustseed": 0,
            "authors_filter_min_score": [4],
        }
        dict_incorrect2 = {
            "robustseed": [0],
            "authors_filter_min_score": [4],
            "filter_min_score": [6, {}],
            "projection_nD": ["lsa"],
            "num_dims": [50, 100],
        }
        dict_incorrect3 = {
            "robustseed": [0],
            "authors_filter_min_score": [4],
            "projection_nD": [{"key": ["lsa"], "num_dims": [100, 200]},
                              {"key": "lda", "num_dims": 100}],
            "num_dims": [50, 100],
        }
        assert self.params_iterator1._is_dict_correct(dict_correct)
        assert not self.params_iterator1._is_dict_correct(dict_incorrect1)
        assert not self.params_iterator1._is_dict_correct(dict_incorrect2)
        assert not self.params_iterator1._is_dict_correct(dict_incorrect3)

    def test_has_child_dict(self):
        dict_no = {
            "robustseed": [0],
            "authors_filter_min_score": [4],
            "filter_min_score": [6],
            "projection_nd": ["lsa"],
            "num_dims": [50, 100],
        }

        dict_child = {
            "robustseed": [0],
            "authors_filter_min_score": [4],
            "projection_nd": [{"key": ["lsa"], "num_dims": [100, 200]}],
        }

        assert not self.params_iterator1._has_child_dict(dict_no)
        assert self.params_iterator1._has_child_dict(dict_child)

    def test_flatten_dict(self):
        dict_no = {
            "robustseed": [0],
            "authors_filter_min_score": [4],
            "filter_min_score": [6],
            "projection_nd": ["lsa"],
            "num_dims": [50, 100],
        }
        dict_child = {
            "robustseed": [0],
            "authors_filter_min_score": [4],
            "projection_nd": [{"key": ["lsa"], "num_dims": [100, 200]}],
        }

        assert self.params_iterator1._flatten_dict(dict_no) == dict_no
        assert self.params_iterator1._flatten_dict(dict_child) == dict_child

    def test_load_from_dict(self):
        dict_child = {
            "robustseed": [0],
            "authors_filter_min_score": [4],
            "projection_nD": [{"key": ["lsa"], "num_dims": [100, 200]}],
        }

        assert len(self.params_iterator1._load_from_dict(dict_child)) == 2

        dict_child2 = {
            "robustseed": [0],
            "authors_filter_min_score": [4],
            "projection_nD": [
                {"key": ["lsa"], "num_dims": [100, 200]},
                {"key": ["bert"], "family": ["x", "y"]}
            ],
        }

        assert len(self.params_iterator1._load_from_dict(dict_child2)) == 4

    def test_columns(self):
        assert list(self.params_iterator1.columns) == [
            "id", "robustseed", "dataset", "min_df", "max_df", "max_features",
            "selected"
        ]

    def test_has_next(self):
        assert self.params_iterator1.has_next() is True
        assert self.params_iterator2.has_next() is True
        assert self.params_iterator3.has_next() is True

        for i in range(len(self.params_iterator1.params_frame)):
            self.params_iterator1.next()

        assert self.params_iterator1.has_next() is False

        self.params_iterator2.next()
        self.params_iterator2.next()
        assert self.params_iterator2.has_next() is False

    def test_stop(self):
        assert self.params_iterator1.stop(3) is False
        self.params_iterator1.next()
        self.params_iterator1.next()
        self.params_iterator1.next()

        assert self.params_iterator1.stop(3) is True

        assert self.params_iterator2.stop(3) is False
        self.params_iterator2.next()
        self.params_iterator2.next()
        assert self.params_iterator2.stop(3) is True

        assert self.params_iterator3.stop(1) is False
        self.params_iterator3.next()
        assert self.params_iterator3.stop(1) is True


class TestRandomIterator(TestCase, ParameterIteratorBase):

    def setUp(self):
        self.df1 = pd.DataFrame({
            "id": ["73db4607d91c6f9fc235",
                   "d50f5bae0a6d83d3ff91",
                   "4ac1a61025d7faf462f0",
                   "4af83d390b8e47d39fff"],
            "robustseed": [0, 0, 1, 1],
            "dataset": ["lisn", "ups", "lisn", "ups"],
            "min_df": [10, 10, 20, 20],
            "max_df": [0.5, 0.5, 1.0, 1.0],
            "max_features": [None, None, None, None],
            "selected": [False, False, False, False]
            })
        self.params_iterator1 = RandomIterator(df=self.df1, seed=42)

        self.df2 = pd.DataFrame({
            "id": ["73db4607d91c6f9fc235",
                   "d50f5bae0a6d83d3ff91",
                   "4ac1a61025d7faf462f0",
                   "4af83d390b8e47d39fff"],
            "robustseed": [0, 0, 1, 1],
            "dataset": ["lisn", "ups", "lisn", "ups"],
            "min_df": [10, 10, 20, 20],
            "max_df": [0.5, 0.5, 1.0, 1.0],
            "max_features": [None, None, None, None],
            "selected": [True, False, True, False]
            })
        self.params_iterator2 = RandomIterator(df=self.df2, seed=42)

        dir_path = Path(__file__).parent
        self.params_iterator3 = RandomIterator(
            csv_filepath=dir_path/"exp.csv", seed=42, index_col=None
        )

        self.params_iterator4 = RandomIterator(params_dict={
            "robustseed": [0],
            "authors_filter_min_score": [4],
            "filter_min_score": [6],
            "projection_nd": ["lsa"],
            "num_dims": [50, 100],
            "projection_2d": ["umap"],
            "n_neighbors": [10, 20, 50],
            "min_dist": [0.1, 0.25, 0.5],
            "metric": ["euclidean"]
        }, seed=42)

    def test_cons_error(self):
        """Tests RandomSelector for errors"""
        with pytest.raises(AssertionError,
                           match="Either a dataframe, .csv file path *"):
            RandomIterator(None, None)

    def test_next(self):
        # expected index 2
        params = self.params_iterator1.next()
        assert params.get("id") == "4ac1a61025d7faf462f0"
        assert params.get("robustseed") == 1
        assert params.get("dataset") == "lisn"
        assert params.get("min_df") == 20
        assert params.get("max_df") == 1
        assert "selected" not in params.keys()

        # expected index 0
        params = self.params_iterator1.next()
        assert params.get("id") == "73db4607d91c6f9fc235"
        assert params.get("robustseed") == 0
        assert params.get("dataset") == "lisn"
        assert params.get("min_df") == 10
        assert params.get("max_df") == 0.5
        assert "selected" not in params.keys()

        # expected index 1
        params = self.params_iterator1.next()
        assert params.get("id") == "d50f5bae0a6d83d3ff91"
        assert params.get("robustseed") == 0
        assert params.get("dataset") == "ups"
        assert params.get("min_df") == 10
        assert params.get("max_df") == 0.5
        assert "selected" not in params.keys()

        # expected index 3
        params = self.params_iterator1.next()
        assert params.get("id") == "4af83d390b8e47d39fff"
        assert params.get("robustseed") == 1
        assert params.get("dataset") == "ups"
        assert params.get("min_df") == 20
        assert params.get("max_df") == 1.0
        assert "selected" not in params.keys()

        params = self.params_iterator1.next()
        assert params is None

        # expected index 4
        params = self.params_iterator4.next()
        assert params.get("id") == "982161d9a4513b85988f"
        assert params.get("robustseed") == 0
        assert params.get("authors_filter_min_score") == 4
        assert params.get("filter_min_score") == 6
        assert params.get("projection_nd") == "lsa"
        assert params.get("num_dims") == 100
        assert params.get("projection_2d") == "umap"
        assert params.get("n_neighbors") == 10
        assert params.get("min_dist") == 0.25
        assert params.get("metric") == "euclidean"
        assert "selected" not in params.keys()


class TestGridIterator(TestCase, ParameterIteratorBase):

    def setUp(self):
        self.df1 = pd.DataFrame({
            "id": ["73db4607d91c6f9fc235",
                   "d50f5bae0a6d83d3ff91",
                   "4ac1a61025d7faf462f0",
                   "4af83d390b8e47d39fff"],
            "robustseed": [0, 0, 1, 1],
            "dataset": ["lisn", "ups", "lisn", "ups"],
            "min_df": [10, 10, 20, 20],
            "max_df": [0.5, 0.5, 1.0, 1.0],
            "max_features": [None, None, None, None],
            "selected": [False, False, False, False]
            })
        self.params_iterator1 = GridIterator(df=self.df1)

        self.df2 = pd.DataFrame({
            "id": ["73db4607d91c6f9fc235",
                   "d50f5bae0a6d83d3ff91",
                   "4ac1a61025d7faf462f0",
                   "4af83d390b8e47d39fff"],
            "robustseed": [0, 0, 1, 1],
            "dataset": ["lisn", "ups", "lisn", "ups"],
            "min_df": [10, 10, 20, 20],
            "max_df": [0.5, 0.5, 1.0, 1.0],
            "max_features": [None, None, None, None],
            "selected": [True, False, True, False]
            })
        self.params_iterator2 = GridIterator(df=self.df2)

        dir_path = Path(__file__).parent
        self.params_iterator3 = GridIterator(
            csv_filepath=dir_path/"exp.csv", index_col=None
        )

        self.params_iterator4 = GridIterator(params_dict={
            "robustseed": [0],
            "authors_filter_min_score": [4],
            "filter_min_score": [6],
            "projection_nd": ["lsa"],
            "num_dims": [50, 100],
            "projection_2d": ["umap"],
            "n_neighbors": [10, 20, 50],
            "min_dist": [0.1, 0.25, 0.5],
            "metric": ["euclidean"]
        })

    def test_cons_error(self):
        """Tests GridSelector for errors"""
        with pytest.raises(AssertionError,
                           match="Either a dataframe, .csv file path *"):
            GridIterator(None, None)

    def test_next(self):
        # For params_iterator1
        # expected index 0
        params = self.params_iterator1.next()
        assert params.get("id") == "73db4607d91c6f9fc235"
        assert params.get("robustseed") == 0
        assert params.get("dataset") == "lisn"
        assert params.get("min_df") == 10
        assert params.get("max_df") == 0.5
        assert "selected" not in params.keys()

        # expected index 1
        params = self.params_iterator1.next()
        assert params.get("id") == "d50f5bae0a6d83d3ff91"
        assert params.get("robustseed") == 0
        assert params.get("dataset") == "ups"
        assert params.get("min_df") == 10
        assert params.get("max_df") == 0.5
        assert "selected" not in params.keys()

        # expected index 2
        params = self.params_iterator1.next()
        assert params.get("id") == "4ac1a61025d7faf462f0"
        assert params.get("robustseed") == 1
        assert params.get("dataset") == "lisn"
        assert params.get("min_df") == 20
        assert params.get("max_df") == 1.0
        assert "selected" not in params.keys()

        # expected index 3
        params = self.params_iterator1.next()
        assert params.get("id") == "4af83d390b8e47d39fff"
        assert params.get("robustseed") == 1
        assert params.get("dataset") == "ups"
        assert params.get("min_df") == 20
        assert params.get("max_df") == 1.0
        assert "selected" not in params.keys()

        params = self.params_iterator1.next()
        assert params is None

        # for params_iterator2
        # expected index 1
        params = self.params_iterator2.next()
        assert params.get("id") == "d50f5bae0a6d83d3ff91"
        assert params.get("robustseed") == 0
        assert params.get("dataset") == "ups"
        assert params.get("min_df") == 10
        assert params.get("max_df") == 0.5
        assert "selected" not in params.keys()

        # expected index 3
        params = self.params_iterator2.next()
        assert params.get("id") == "4af83d390b8e47d39fff"
        assert params.get("robustseed") == 1
        assert params.get("dataset") == "ups"
        assert params.get("min_df") == 20
        assert params.get("max_df") == 1.0
        assert "selected" not in params.keys()

        params = self.params_iterator2.next()
        assert params is None

        # expected index 4
        params = self.params_iterator4.next()
        assert params.get("id") == "46d343df397295d11ac5"
        assert params.get("robustseed") == 0
        assert params.get("authors_filter_min_score") == 4
        assert params.get("filter_min_score") == 6
        assert params.get("projection_nd") == "lsa"
        assert params.get("num_dims") == 50
        assert params.get("projection_2d") == "umap"
        assert params.get("n_neighbors") == 10
        assert params.get("min_dist") == 0.1
        assert params.get("metric") == "euclidean"
        assert "selected" not in params.keys()
