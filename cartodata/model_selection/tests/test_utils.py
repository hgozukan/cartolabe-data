from pytest import approx
from unittest import TestCase

from cartodata.model_selection.utils import (
    Result, load_scores_from_file, load_desc_scores_from_file,
    load_raw_scores_from_file
)
from cartodata.phases import PhaseProjectionND, PhaseProjection2D
from cartodata.tests.conftest import FILES_DIR


def test_load_scores_from_file():
    score_name = "neighbors_articles_authors"
    result = load_scores_from_file(PhaseProjectionND, [score_name], FILES_DIR,
                                   "phase2_scores_all.csv")
    assert len(result.scores) == 1
    assert result.scores[score_name] == approx(0.5468, abs=1e-4)

    result = load_scores_from_file(PhaseProjectionND, [score_name], FILES_DIR,
                                   "phase3_scores_all.csv")

    assert len(result.scores) == 1
    assert result.scores[score_name] == approx(0.5468, abs=1e-4)

    result = load_scores_from_file(PhaseProjection2D, [score_name], FILES_DIR,
                                   "phase3_scores_all.csv")
    assert len(result.scores) == 1
    assert result.scores[score_name] == approx(0.5289, abs=1e-4)


def test_load_desc_scores_from_file():
    score_name = "neighbors_articles_authors"

    # PhaseProjectionND
    result = Result()
    load_desc_scores_from_file(result, PhaseProjectionND, [score_name],
                               FILES_DIR, load_params=True)

    assert len(result.desc_scores) == 1
    desc_scores = result.desc_scores[f"{score_name}_det"]
    assert desc_scores[1] == approx(0.5468, abs=1e-4)
    assert len(desc_scores[0]) == 48
    assert result.run_params == {'key': 'bert',
                                 'num_dims': 768,
                                 'normalize': True,
                                 'family': 'all-MiniLM-L6-v2',
                                 'max_length': 256}
    assert result.score_params == {'min_score': 30,
                                   'recompute': True,
                                   'sample_size': None,
                                   'n_neighbors': 10,
                                   'random_state': 42}

    # PhaseProjection2D
    result = Result()
    load_desc_scores_from_file(result, PhaseProjection2D, [score_name],
                               FILES_DIR, load_params=True)

    assert len(result.desc_scores) == 1
    desc_scores = result.desc_scores[f"{score_name}_det"]
    assert desc_scores[1] == approx(0.5289, abs=1e-4)
    assert len(desc_scores[0]) == 48
    assert result.run_params == {'key': 'umap',
                                 'n_neighbors': 10,
                                 'min_dist': 0.1,
                                 'metric': 'euclidean',
                                 'init': 'spectral',
                                 'learning_rate': 1.0,
                                 'repulsion_strength': 1.0}
    assert result.score_params == {'min_score': 30,
                                   'recompute': True,
                                   'sample_size': None,
                                   'n_neighbors': 10,
                                   'random_state': 42}

    # PhaseProjection2D without params
    result = Result()
    load_desc_scores_from_file(result, PhaseProjection2D, [score_name],
                               FILES_DIR, load_params=False)

    assert len(result.desc_scores) == 1
    desc_scores = result.desc_scores[f"{score_name}_det"]
    assert desc_scores[1] == approx(0.5289, abs=1e-4)
    assert len(desc_scores[0]) == 48
    assert result.run_params == {}
    assert result.score_params == {}


def test_load_raw_scores_from_file():
    score_name = "neighbors_articles_authors"

    # PhaseProjectionND
    result = Result()
    load_raw_scores_from_file(result, PhaseProjectionND, [score_name],
                              FILES_DIR)

    assert len(result.raw_scores) == 1
    assert len(result.raw_scores[score_name]) == 48

    # PhaseProjection2D
    result = Result()
    load_raw_scores_from_file(result, PhaseProjection2D, [score_name],
                              FILES_DIR)

    assert len(result.raw_scores) == 1
    assert len(result.raw_scores[score_name]) == 48


class TestResult(TestCase):

    def setUp(self):
        self.params = {"a": 5, "b": 4}
        self.result = Result()

    def test_constructor(self):
        assert self.result.run_params == {}
        assert self.result.score_params == {}

        assert self.result.scores == {}
        assert self.result.desc_scores == {}
        assert self.result.raw_scores == {}

    def test_reset(self):
        self.result.add_score("score", 25)
        self.result.add_desc_score("desc_score", 25, 25)
        self.result.add_raw_score("score", 25)

        assert self.result.scores != {}
        assert self.result.desc_scores != {}
        assert self.result.scores != {}

        self.result.reset()
        assert self.result.scores == {}
        assert self.result.desc_scores == {}
        assert self.result.raw_scores == {}

    def test_prefix(self):
        self.result.add_score("score", 25)
        self.result.add_desc_score("desc_score", 25, 25)
        self.result.add_raw_score("score", 25)

        self.result.prefix(PhaseProjectionND)

        assert (
            self.result.scores[PhaseProjectionND.prefix("score")] is not None
            )
        assert (
            self.result.desc_scores[PhaseProjectionND.prefix("desc_score")]
            is not None
            )
        assert (
            self.result.raw_scores[PhaseProjectionND.prefix("score")]
            is not None
            )

    def test_add_score(self):
        self.result.add_score("score", 25)

        assert len(self.result.scores.keys()) == 1
        assert self.result.scores["score"] == 25

    def test_add_desc_score(self):
        self.result.add_desc_score("desc_score", 25, 25)
        assert len(self.result.desc_scores.keys()) == 1
        assert self.result.desc_scores["desc_score"] == (25, 25)

    def test_add_raw_score(self):
        self.result.add_raw_score("score", 25)

        assert len(self.result.raw_scores.keys()) == 1
        assert self.result.raw_scores["score"] == 25

    def test_has_score(self):
        self.result.add_score("score", 25)
        assert self.result.has_score("score")

        assert not self.result.has_score("invalid_score")

    def test_append_score_params(self):
        assert self.result.score_params == {}

        params = {"param1": 25}
        self.result.append_score_params(params)

        assert self.result.score_params == params

    def test_append_run_params(self):
        assert self.result.run_params == {}

        params = {"param1": 25}
        self.result.append_run_params(params)

        assert self.result.run_params == params

    def test_append(self):
        score_params = {"score_param": 25}
        run_params = {"run_param": 15}

        self.result.add_score("score1", 15)
        self.result.add_score("score2", 25)

        result = Result()
        result.add_score("score1", 20)
        result.add_score("score3", 40)
        result.add_raw_score("raw1", "raw_trial")
        result.add_desc_score("desc1", 24, 45)

        result.append_score_params(score_params)
        result.append_run_params(run_params)

        self.result.append(result)

        assert self.result.scores == {"score1": 20, "score2": 25, "score3": 40}
        assert self.result.desc_scores == {"desc1": (24, 45)}
        assert self.result.raw_scores == {"raw1": "raw_trial"}
        assert self.result.run_params == run_params
        assert self.result.score_params == score_params
