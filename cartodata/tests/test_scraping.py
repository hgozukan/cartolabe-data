from unittest import TestCase

from ..scraping import build_query_url


class TestHalScraping(TestCase):

    def test_build_query_url(self):
        base_url = 'http://api.com'
        query = '*:*'
        fields = ['authFullName_s', 'en_abstract_s', 'en_keyword_s',
                  'en_title_s', 'structAcronym_s',
                  'producedDateY_i', 'producedDateM_i',
                  'halId_s', 'docid', 'en_domainAllCodeLabel_fs']
        filters = {
            "structAcronym_t": "LRI",
            # "docid": "[1526319 TO *]"
        }
        cursor_mark = '*'
        rows = '5000'
        sort = 'docid asc'

        url = build_query_url(base_url, query, fields, filters, rows,
                              cursor_mark, sort)
        self.assertEqual(
            url,
            'http://api.com/?q=*:*&fl=authFullName_s,en_abstract_s,'
            'en_keyword_s,en_title_s,'
            'structAcronym_s,producedDateY_i,producedDateM_i,halId_s,'
            'docid,en_domainAllCodeLabel_fs'
            '&fq=structAcronym_t:LRI&rows=5000&cursorMark=%2A&sort=docid asc'
        )
