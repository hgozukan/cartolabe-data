from unittest import TestCase

import numpy as np
import pandas as pd
import scipy.sparse as scs

from ..loading import (
    load_identity_column, load_comma_separated_column, normalize_term,
    load_text_column, PunctuationCountVectorizer
)


class TestLoading(TestCase):

    def test_load_identity_column(self):
        titles = ['Multi-prover verification of floating-point programs',
                  'Hardware-independent proofs of numerical programs',
                  'Viewing a World of Annotations through AnnoVIP']
        df = pd.DataFrame({'en_title_s': titles})

        tab, scores = load_identity_column(df, 'en_title_s')

        self.assertTrue((scores.index == titles).all())
        self.assertTrue((scores.values == [1, 1, 1]).all())
        self.assertEqual(type(tab), scs.dia_matrix)
        np.testing.assert_allclose(
            tab.todense(), [[1, 0, 0], [0, 1, 0], [0, 0, 1]])

    def test_load_comma_separated_column(self):
        authors = [
            'Philippe Caillou,Samir Aknine,  Suzanne Pinson',
            'Bruno Cessac,  Hélène Paugam-Moisy  ,  Thierry Viéville',
            'Samuel Thiriot,Zach Lewkovicz,Philippe Caillou,Jean-Daniel Kant',
            'Zach Lewkovicz,Samuel Thiriot,Philippe Caillou'
        ]
        df = pd.DataFrame({'authFullName_s': authors})

        tab, scores = load_comma_separated_column(df, 'authFullName_s')

        formatted_names = ['Philippe Caillou', 'Samir Aknine',
                           'Suzanne Pinson', 'Bruno Cessac',
                           'Hélène Paugam-Moisy', 'Thierry Viéville',
                           'Samuel Thiriot', 'Zach Lewkovicz',
                           'Jean-Daniel Kant']
        self.assertTrue((scores.index == formatted_names).all())
        self.assertTrue((scores.values == [3, 1, 1, 1, 1, 1, 2, 2, 1]).all())
        self.assertEqual(type(tab), scs.csr_matrix)
        np.testing.assert_allclose(
            tab.todense(), [[1, 1, 1, 0, 0, 0, 0, 0, 0],
                            [0, 0, 0, 1, 1, 1, 0, 0, 0],
                            [1, 0, 0, 0, 0, 0, 1, 1, 1],
                            [1, 0, 0, 0, 0, 0, 1, 1, 0]]
        )

    def test_load_comma_separated_column_whitelist(self):
        strucs = ['TAO,CNRS,Inria,UP11,LRI,UP11,Inria,CNRS',
                  'AVIZ,Inria,ISIR,CNRS,ILDA,LRI,UP11,Inria,CNRS,Inria']
        teams = ['tao', 'aviz', 'ilda']
        df = pd.DataFrame({'structAcronym_s': strucs})

        tab, scores = load_comma_separated_column(df, 'structAcronym_s', teams)

        self.assertTrue((scores.index == ['TAO', 'AVIZ', 'ILDA']).all())
        self.assertTrue((scores.values == [1, 1, 1]).all())
        np.testing.assert_allclose(tab.todense(), [[1, 0, 0],
                                                   [0, 1, 1]])

        tab, scores = load_comma_separated_column(
            df, 'structAcronym_s', blacklist=teams)
        labs = ['CNRS', 'Inria', 'UP11', 'LRI', 'ISIR']

        self.assertTrue((scores.index == labs).all())
        self.assertTrue((scores.values == [4, 5, 3, 2, 1]).all())
        np.testing.assert_allclose(tab.todense(), [[2, 2, 2, 1, 0],
                                                   [2, 3, 1, 1, 1]])

    def test_load_text_column(self):
        abstracts = [
            ('Many empirical studies emphasize the role of social networks in '
             'job search.'),
            ('In this short note, we investigate. Responsibility of '
             'Pseudoknotted')

        ]
        df = pd.DataFrame({'text': abstracts})

        tab, scores = load_text_column(df['text'], 4, 1, 1)

        words = ['emphasize', 'emphasize social', 'emphasize social networks',
                 'emphasize social networks search',
                 'empirical', 'empirical studies',
                 'empirical studies emphasize',
                 'empirical studies emphasize social',
                 'investigate', 'networks', 'networks search',
                 'pseudoknotted', 'responsibility',
                 'responsibility pseudoknotted', 'search',
                 'short', 'social', 'social networks',
                 'social networks search', 'studies',
                 'studies emphasize', 'studies emphasize social',
                 'studies emphasize social networks']
        self.assertTrue((scores.index == words).all())
        self.assertTrue((scores.values == [1 for _ in words]).all())
        self.assertEqual(type(tab), scs.csr_matrix)
        np.testing.assert_allclose(
            tab.todense(), [[1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0,
                             0, 1, 0, 1, 1, 1, 1, 1, 1, 1],
                            [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1,
                             1, 0, 1, 0, 0, 0, 0, 0, 0, 0]]
        )

    def test_normalize_term(self):
        self.assertEqual(normalize_term(
            'Philippe Caillou'), 'philippe caillou')
        self.assertEqual(normalize_term(
            'Jean-Daniel Fekete'), 'jeandaniel fekete')
        self.assertEqual(normalize_term('89@&é $$%az'), '89e az')

        whitelist = ['aviz', 'ayin', 'beagle', 'bigs',
                     'biocore', 'biovision', 'bipop', 'bonsai']
        self.assertIsNone(normalize_term('tao', whitelist))
        self.assertEqual(normalize_term('Aviz%%', whitelist), 'aviz')

        self.assertIsNone(normalize_term('Aviz%%', blacklist=whitelist))
        self.assertEqual(normalize_term('TAO@+', blacklist=whitelist), 'tao')

        self.assertIsNone(normalize_term('Aviz%*$', banned_keywords=whitelist))

        self.assertIsNone(
            normalize_term(('Institut National de Recherche en Informatique '
                            'et Automatique'), filter_acronyms=True
                           )
        )


class TestPunctuationCountVectorizer(TestCase):

    def test_analyzer(self):
        vectorizer = PunctuationCountVectorizer(
            min_df=1, max_df=1, ngram_range=(1, 4), strip_accents='unicode',
            token_pattern='[a-zA-Z0-9]{5,}'
        )

        analyzer = vectorizer.build_analyzer()

        n_grams = analyzer(
            'In this short note, we investigate. Responsibility of '
            'Pseudoknotted'
        )
        self.assertEqual(n_grams, ['short', 'investigate', 'responsibility',
                                   'pseudoknotted',
                                   'responsibility pseudoknotted'])

        n_grams = analyzer('Associative wave equation,Time domain,Thin slots')
        self.assertEqual(
            n_grams, ['associative', 'equation', 'associative equation',
                      'domain', 'slots']
        )
