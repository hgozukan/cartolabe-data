import numpy as np
import pandas as pd
import scipy.sparse as scs
import pytest
from unittest.mock import patch
from unittest import TestCase

from cartodata.pipeline.columns import (
    Column, Columns, IdentityColumn, CSColumn, FilterColumn, CorpusColumn,
    TfidfCorpusColumn
)


class TestColumn(TestCase):

    def test_column_cons(self):
        """Tests Column constructor."""
        column = Column(Columns.IDENTITY, "nature", "column_name")

        assert column.type == Columns.IDENTITY
        assert column.nature == "nature"
        assert column.column_name == "column_name"

    def test_column_cons_error(self):
        """Tests Column constructor for invalid values."""
        with pytest.raises(AssertionError,
                           match="Please specify a valid nature."):
            Column(Columns.IDENTITY, "", "column_name")

        with pytest.raises(AssertionError,
                           match="Please specify a valid nature."):
            Column(Columns.IDENTITY, None, "column_name")

        with pytest.raises(AssertionError,
                           match="Please specify a valid column_name."):
            Column(Columns.IDENTITY, "nature", "")
        with pytest.raises(AssertionError,
                           match="Please specify a valid column_name."):
            Column(Columns.IDENTITY, "nature", None)

    def test_identity_column_cons(self):
        """Tests IdentityColumn constructor."""
        column = IdentityColumn("nature", "identity")
        assert column.nature == "nature"
        assert column.column_name == "identity"
        assert column.type == Columns.IDENTITY

    @patch("cartodata.pipeline.datasets.CSVDataset")
    def test_identity_column_load(self, dataset):
        """Tests IdentityColumn.load method."""
        column = IdentityColumn("articles", "en_title_s")

        titles = ['Multi-prover verification of floating-point programs',
                  'Hardware-independent proofs of numerical programs',
                  'Viewing a World of Annotations through AnnoVIP']
        df = pd.DataFrame({'en_title_s': titles})

        dataset.df = df

        matrix, scores = column.load(dataset)

        assert (scores.index == titles).all()
        assert (scores.values == [1, 1, 1]).all()
        assert type(matrix) == scs.dia_matrix
        assert np.allclose(matrix.todense(), [[1, 0, 0], [0, 1, 0], [0, 0, 1]])

    def test_cs_column_cons(self):
        """Tests CSColumn constructor."""
        column = CSColumn("nature", "cs")
        assert column.nature == "nature"
        assert column.column_name == "cs"
        assert column.type == Columns.CS
        assert column.whitelist is None
        assert column.blacklist is None
        assert column.banned_keywords is None
        assert not column.filter_acronyms
        assert column.separator == ","
        assert column.filter_min_score == 0

        column = CSColumn(
            "nature", "cs", whitelist="whitelist.csv",
            blacklist="blacklist.csv", filter_acronyms=True, separator=";",
            filter_min_score=5
        )
        assert column.nature == "nature"
        assert column.column_name == "cs"
        assert column.type == Columns.CS
        assert column.whitelist == "whitelist.csv"
        assert column.blacklist == "blacklist.csv"
        assert column.banned_keywords is None
        assert column.filter_acronyms
        assert column.separator == ";"
        assert column.filter_min_score == 5

    @patch("cartodata.pipeline.datasets.CSVDataset")
    def test_cs_column_load(self, dataset):
        """Tests CSColumn.load method."""
        column = CSColumn("authors", "authFullName_s")

        authors = [
            'Philippe Caillou,Samir Aknine,  Suzanne Pinson',
            'Bruno Cessac,  Hélène Paugam-Moisy  ,  Thierry Viéville',
            'Samuel Thiriot,Zach Lewkovicz,Philippe Caillou,Jean-Daniel Kant',
            'Zach Lewkovicz,Samuel Thiriot,Philippe Caillou'
        ]

        dataset.df = pd.DataFrame({'authFullName_s': authors})

        matrix, scores = column.load(dataset)

        formatted_names = ['Philippe Caillou', 'Samir Aknine',
                           'Suzanne Pinson', 'Bruno Cessac',
                           'Hélène Paugam-Moisy', 'Thierry Viéville',
                           'Samuel Thiriot', 'Zach Lewkovicz',
                           'Jean-Daniel Kant']

        assert (scores.index == formatted_names).all()
        assert (scores.values == [3, 1, 1, 1, 1, 1, 2, 2, 1]).all()
        assert type(matrix) == scs.csr_matrix
        assert np.allclose(matrix.todense(), [[1, 1, 1, 0, 0, 0, 0, 0, 0],
                                              [0, 0, 0, 1, 1, 1, 0, 0, 0],
                                              [1, 0, 0, 0, 0, 0, 1, 1, 1],
                                              [1, 0, 0, 0, 0, 0, 1, 1, 0]])

        # Test with filter_min_score = 1
        column = CSColumn("authors", "authFullName_s", filter_min_score=1)

        matrix, scores = column.load(dataset)

        formatted_names = ['Philippe Caillou', 'Samuel Thiriot',
                           'Zach Lewkovicz']

        assert (scores.index == formatted_names).all()
        assert (scores.values == [3, 2, 2]).all()
        assert type(matrix) == scs.csr_matrix
        assert np.allclose(matrix.todense(), [[1, 0, 0],
                                              [0, 0, 0],
                                              [1, 1, 1],
                                              [1, 1, 1]])

        # Test whitelist
        strucs = ['TAO,CNRS,Inria,UP11,LRI,UP11,Inria,CNRS',
                  'AVIZ,Inria,ISIR,CNRS,ILDA,LRI,UP11,Inria,CNRS,Inria']
        teams = ['tao', 'aviz', 'ilda']

        dataset.df = pd.DataFrame({'structAcronym_s': strucs})

        column = CSColumn("labs", "structAcronym_s", whitelist=teams)

        matrix, scores = column.load(dataset)

        assert (scores.index == ['TAO', 'AVIZ', 'ILDA']).all()
        assert (scores.values == [1, 1, 1]).all()
        assert np.allclose(matrix.todense(), [[1, 0, 0],
                                              [0, 1, 1]])

        # Test blacklist
        labs = ['CNRS', 'Inria', 'UP11', 'LRI', 'ISIR']
        column = CSColumn("labs", "structAcronym_s", blacklist=teams)

        matrix, scores = column.load(dataset)

        assert (scores.index == labs).all()
        assert (scores.values == [4, 5, 3, 2, 1]).all()
        assert np.allclose(matrix.todense(), [[2, 2, 2, 1, 0],
                                              [2, 3, 1, 1, 1]])

    def test_filter_column_cons(self):
        """Tests FilterColumn constructor."""
        column = FilterColumn("nature", "column", "filter", "filter_value")
        assert column.nature == "nature"
        assert column.column_name == "column"
        assert column.type == Columns.FILTER
        assert column.filter_column == "filter"
        assert column.filter_value == "filter_value"
        assert column.isnan is None
        assert column.scores_index_column is None
        assert column.scores_value_column is None

        column = FilterColumn(
            "nature", "column", "filter", isnan=True,
            scores_index_column="scores_index",
            scores_value_column="scores_value"
        )
        assert column.nature == "nature"
        assert column.column_name == "column"
        assert column.type == Columns.FILTER
        assert column.filter_column == "filter"
        assert column.filter_value is None
        assert column.isnan
        assert column.scores_index_column == "scores_index"
        assert column.scores_value_column == "scores_value"

    def test_filter_column_cons_error(self):
        """Tests FilterColumn constructor for error."""
        with pytest.raises(AssertionError, match=(
                "Please specify wither filter_value or isnan!"
        )):
            FilterColumn("nature", "column", "filter")

    @patch("cartodata.pipeline.datasets.CSVDataset")
    def test_filter_column_load(self, dataset):
        """Tests FilterColumn.load method."""

        df = pd.DataFrame(
            {
                "authFullName_s": [
                    None,
                    "Bruno Cessac",
                    "Zach Lewkovicz",
                    "Zach Lewkovicz"
                ],
                "en_abstract_s": [
                    ("Many empirical studies emphasize the role of social "
                     "networks in job search. The social network implicated "
                     "in this process"),
                    ("In this short note, we investigate. Responsibility of "
                     "Pseudoknotted"),
                    ("Exactly Solvable Stochastic Processes for Traffic "
                     "Modelling"),
                    ("In the ground case, the procedure terminates and"
                     " provides a decision algorithm for the word problem.")
                ],
                "en_title_s": [
                    "Multi-prover verification of floating-point programs",
                    "Hardware-independent proofs of numerical programs",
                    "Viewing a World of Annotations through AnnoVIP",
                    "Combinatorial identification problems and graph powers"
                ]
            }
        )

        dataset.df = df

        column = FilterColumn("authors", "en_title_s",
                              "authFullName_s", isnan=True)

        matrix, scores = column.load(dataset)

        assert type(matrix) == scs.csr_matrix
        assert (scores.index == [
                "Multi-prover verification of floating-point programs"]).all()
        assert (scores.values == [1.0]).all()

        column = FilterColumn("authors", "en_title_s",
                              "authFullName_s", isnan=False)

        matrix, scores = column.load(dataset)

        assert type(matrix) == scs.csr_matrix
        assert (scores.index == [
            "Hardware-independent proofs of numerical programs",
            "Viewing a World of Annotations through AnnoVIP",
            "Combinatorial identification problems and graph powers"]).all()

        assert (scores.values == [1.0, 1.0, 1.0]).all()

        column = FilterColumn("authors", "en_title_s",
                              "authFullName_s", isnan=False,
                              scores_value_column="authFullName_s")

        matrix, scores = column.load(dataset)

        assert type(matrix) == scs.csr_matrix
        assert (scores.index == [
            "Hardware-independent proofs of numerical programs",
            "Viewing a World of Annotations through AnnoVIP",
            "Combinatorial identification problems and graph powers"]).all()

        assert (scores.values == ["Bruno Cessac",
                                  "Zach Lewkovicz",
                                  "Zach Lewkovicz"]).all()

        column = FilterColumn("authors", "en_title_s",
                              "authFullName_s", isnan=False,
                              scores_index_column="en_abstract_s",
                              scores_value_column="authFullName_s")

        matrix, scores = column.load(dataset)

        assert type(matrix) == scs.csr_matrix
        assert (scores.index == [
            ("In this short note, we investigate. Responsibility of "
             "Pseudoknotted"),
            ("Exactly Solvable Stochastic Processes for Traffic "
             "Modelling"),
            ("In the ground case, the procedure terminates and"
             " provides a decision algorithm for the word problem.")
        ]).all()

        assert (scores.values == ["Bruno Cessac",
                                  "Zach Lewkovicz",
                                  "Zach Lewkovicz"]).all()

    def test_corpus_column_cons(self):
        """Tests CorpusColumn constructor."""
        column = CorpusColumn("words", ["text"], None)

        assert column.nature == "words"
        assert column.column_names == ["text"]
        assert column.type == Columns.CORPUS
        assert column.stopwords is None
        assert column.english
        assert column.nb_grams == 4
        assert column.min_df == 1
        assert column.max_df == 1.0
        assert column.vocab_sample is None
        assert column.max_features is None
        assert column.strip_accents == "unicode"
        assert column.lowercase
        assert column.min_word_length == 5
        assert column.normalize

    @patch("cartodata.pipeline.datasets.CSVDataset")
    def test_corpus_column_load(self, dataset):
        """Tests CorpusColumn.load method."""
        column = CorpusColumn("words", ["text"])

        abstracts = [
            ('Many empirical studies emphasize the role of social networks in '
             'job search.'),
            ('In this short note, we investigate. Responsibility of '
             'Pseudoknotted')

        ]
        words = ['emphasize', 'emphasize social', 'emphasize social networks',
                 'emphasize social networks search',
                 'empirical', 'empirical studies',
                 'empirical studies emphasize',
                 'empirical studies emphasize social',
                 'investigate', 'networks', 'networks search',
                 'pseudoknotted', 'responsibility',
                 'responsibility pseudoknotted', 'search',
                 'short', 'social', 'social networks',
                 'social networks search', 'studies',
                 'studies emphasize', 'studies emphasize social',
                 'studies emphasize social networks']

        dataset.df = pd.DataFrame({'text': abstracts})

        matrix, scores = column.load(dataset)

        assert (scores.index == words).all()
        assert (scores.values == [1 for _ in words]).all()
        assert type(matrix) == scs.csr_matrix
        np.allclose(
            matrix.todense(), [[1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0,
                                0, 1, 0, 1, 1, 1, 1, 1, 1, 1],
                               [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1,
                                1, 0, 1, 0, 0, 0, 0, 0, 0, 0]]
        )

    def test_corpus_column_get_corpus(self):
        """Tests CorpusColumn.get_corpus method."""
        column = CorpusColumn("words", ["abstract", "title"])
        abstracts = [
            ('Many empirical studies emphasize the role of social networks in '
             'job search.'),
            ('In this short note, we investigate. Responsibility of '
             'Pseudoknotted')

        ]
        titles = [
            "title 1",
            "title 2"
        ]

        df = pd.DataFrame(
            {'abstract': abstracts, 'title': titles}
        )

        res = column.get_corpus(df)

        assert res[0] == ("Many empirical studies emphasize the role of social"
                          " networks in job search. . title 1"
                          )
        assert res[1] == ("In this short note, we investigate. Responsibility"
                          " of Pseudoknotted . title 2"
                          )

    def test_tfidf_column_cons(self):
        """Tests TfidfCorpusColumn constructor."""
        column = TfidfCorpusColumn("words", ["text"])

        assert column.nature == "words"
        assert column.column_names == ["text"]
        assert column.type == Columns.CORPUS
        assert column.stopwords is None
        assert column.english
        assert column.nb_grams == 4
        assert column.min_df == 1
        assert column.max_df == 1.0
        assert column.vocab_sample is None
        assert column.max_features is None
        assert column.strip_accents == "unicode"
        assert column.lowercase
        assert column.min_word_length == 5
        assert column.normalize
        assert column.binary
        assert column.encoding == "utf-8"

    @patch("cartodata.pipeline.datasets.CSVDataset")
    def test_tfidf_column_load(self, dataset):
        """Tests TfidfCorpusColumn.load method."""
        column = TfidfCorpusColumn("words", ["text"])

        abstracts = [
            ('Many empirical studies emphasize the role of social networks in '
             'job search.'),
            ('In this short note, we investigate. Responsibility of '
             'Pseudoknotted')

        ]

        dataset.df = pd.DataFrame({'text': abstracts})

        matrix, scores = column.load(dataset)

        assert type(matrix) == scs.csr_matrix
