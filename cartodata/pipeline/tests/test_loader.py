import pytest
from unittest import TestCase

from cartodata.pipeline.common import Pipeline, AlignedPipeline
from cartodata.pipeline.loader import load_pipeline

PIPELINE_DATASET = "lisn"


class TestLoader(TestCase):

    def test_load_pipeline(self):
        """Tests load_pipeline function."""

        pipeline = load_pipeline(PIPELINE_DATASET, "./conf")

        assert pipeline is not None
        assert isinstance(pipeline, Pipeline)
        assert pipeline.dataset.name == PIPELINE_DATASET
        assert pipeline.projection_2d is not None
        assert pipeline.projection_2d.key == "umap"
        assert pipeline.projection_nd is not None
        assert pipeline.clustering is not None
        assert pipeline.neighboring is not None

    def test_load_pipeline_aligned(self):
        """Tests load_pipeline function for loading an aligned pipeline."""

        pipeline = load_pipeline(PIPELINE_DATASET, "./conf", True)

        assert pipeline is not None
        assert isinstance(pipeline, AlignedPipeline)
        assert pipeline.dataset.name == PIPELINE_DATASET
        assert pipeline.projection_2d is not None
        assert pipeline.projection_2d.key == "aligned_umap"
        assert pipeline.projection_nd is not None
        assert pipeline.clustering is not None
        assert pipeline.neighboring is not None

    def test_load_pipeline_error(self):
        """Tests load_pipeline funtion for non existing file."""

        with pytest.raises(AssertionError,
                           match="Please specify a valid dataset file.*"):
            load_pipeline("test_file", "./conf")
