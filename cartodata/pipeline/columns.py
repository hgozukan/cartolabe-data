from enum import Enum
import logging

from sklearn.feature_extraction.text import ENGLISH_STOP_WORDS
from sklearn.feature_extraction.text import TfidfVectorizer

import pandas as pd
import numpy as np
from download import download

from cartodata.loading import (
    load_identity_column, load_comma_separated_column, load_text_column,
    load_filter_column
)
from cartodata.operations import filter_min_score, normalize_tfidf
from cartodata.pipeline.base import BaseEntity

logger = logging.getLogger(__name__)


class Columns(Enum):
    """
    Enumeration for column types.

    IDENTITY: Represents the main entity of the dataset.
    CS: Represents a column with comma separated values.
    CORPUS: Represents a column with text corpus. Can be a union of multiple
    columns in the dataset.

    """
    IDENTITY = 1
    CS = 2
    CORPUS = 3
    FILTER = 4


class Column(BaseEntity):
    """Base class for a dataset column.

    A dataset column represents an entity in the dataset. When the column is
    loaded, it generates an entity matrix and a scores matrix for that entity.


    Attributes
    ----------
    nature: str
        The nature of the column entity
    column_name: str
        The name of the column in the dataset
    type: Columns
        The type of this column


    Methods
    -------
    load()
        Loads and processes the column specified by `column_name` in the
    dataset and generates the entity and scores matrices.

    """

    def __init__(self, column_type, nature, column_name):
        """
        Attributes
        ----------
        nature: str
            The nature of the column entity
        column_name: str
            The name of the column in the dataset
        type: Columns
            The type of this column

        """

        assert column_type is not None, "Please specify a valid column_type!"

        assert nature is not None and nature != "", (
            "Please specify a valid nature."
        )
        assert column_name is not None and column_name != "", (
            "Please specify a valid column_name."
        )

        self.type = column_type
        self.nature = nature
        self.column_name = column_name
        self._all_params = []

    @property
    def params(self):
        return "_".join([self.nature, super().params])

    def load(self, dataset):
        """Loads and processes the column specified by `column_name` in the
        dataset and generates the entity and scores matrices.

        Parameters
        ----------
        dataset: cartodata.pipeline.datasets.Dataset
            The dataset from which the column will be loaded

        Returns
        -------
            A sparse matrix and a pandas series

        """
        raise NotImplementedError()


class IdentityColumn(Column):
    """A column class that represents the main entity in the dataset that
    uniquely identifies each row of the dataset.

    For `IdentityColumn` the entity matrix has the same number of columns as
    the number of rows. Each column corresponding to a row of the dataset.

    Attributes
    ----------
    nature: str
        The nature of the column entity
    column_name: str
        The name of the column that uniquely identifies the rows in the dataset
    type: Columns
        The type of this column. Its value should be Columns.IDENTITY

    Methods
    -------
    load()
        Loads and processes the column specified by `column_name` in the
    dataset and generates the entity and scores matrices.

    """

    yaml_tag = u'!IdentityColumn'

    def __init__(self, nature, column_name):
        """
        Parameters
        ----------
        nature: str
            The nature of the column entity
        column_name: str
            The name of the column that uniquely identifies the rows in the
        dataset

        """
        super().__init__(Columns.IDENTITY, nature, column_name)

    def load(self, dataset):
        """Loads and processes the column specified by `column_name` in the
        dataset and generates the entity and scores matrices.

        Parameters
        ----------
        dataset: cartodata.pipeline.datasets.Dataset
            The dataset from which the column will be loaded

        Returns
        -------
        scipy.sparse.dia_matrix, pandas.core.series.Series
            A sparse matrix and a pandas series

        """
        logger.info(f"Loading {self.nature} nature...")

        df = dataset.df
        return load_identity_column(df, self.column_name)


class CSColumn(Column):
    """A column class that represents an entity specified in a comma separated
    dataset column.

    For `CSColumn`, the entity matrix rows correspond to the rows of
    IdentityColumn. For each value in the comma separated column, a column
    is added to the matrix.

    Attributes
    ----------
    nature: str
        The nature of the column entity
    column_name: str
        The name of the column in the dataset
    type: Columns
        The type of this column. Its value should be Columns.CS
    whitelist: str
        The name of the CSV file that contains allowed values. Should
    be placed in the dataset's input directory.
    blacklist: str
        The name of the CSV file that contains forbidden values. Should
    be placed in the dataset's input directory.
    banned_keywords:
    filter_acronyms: bool
        Filters the values if True.
    separator: str
            The separator used to separate values.
    filter_min_score: int
        Filters the entities that appear less than `filter_min_score` times in
    the rows and does not create a column for these entities in the entity
    matrix and does not create an entry for these entities in scores.
    filter_nan: bool
            If True, removes the rows which have NAN values.

    Methods
    -------
    load()
        Loads and processes the column specified by `column_name` in the
    dataset and generates the entity and scores matrices.

    """
    yaml_tag = u'!CSColumn'

    def __init__(self, nature, column_name, whitelist=None, blacklist=None,
                 banned_keywords=None, filter_acronyms=False, separator=",",
                 filter_min_score=0, filter_nan=False):
        """
        Parameters
        ----------
        nature: str
            The nature of the column entity
        column_name: str
            The name of the column in the dataset
        whitelist: str, list, default=None
            The name of the CSV file that contains allowed values.
        Should be placed in the dataset's input directory.
            Or a list of strings to be whitelisted.
        blacklist: str, list, default=None
            The name of the CSV file that contains forbidden values.
        Should be placed in the dataset's input directory.
            Or a list of strings to be blacklisted.
        banned_keywords:, default=None
        filter_acronyms: bool, default False
            Filters the values if True.
        separator: str, default ","
            The separator used to separate values.
        filter_min_score: int, default=0
            Removes the entities that appear less than or equal to
        `filter_min_score` times in the rows and does not create a column for
        these entities in the entity matrix and does not create an entry for
        these entities in scores.
        filter_nan: bool, default False
            If True, removes the rows which have NAN values.

        """
        super().__init__(Columns.CS, nature, column_name)

        assert (whitelist is None or isinstance(whitelist, str)
                or isinstance(whitelist, list)), (
                    "Please specify a list of strings or a filename"
                    " that contains the list!")

        assert (blacklist is None or isinstance(blacklist, str)
                or isinstance(blacklist, list)), (
                    "Please specify a list of strings or a filename"
                    " that contains the list!")

        self.whitelist = whitelist
        self.blacklist = blacklist
        self.banned_keywords = banned_keywords
        self.filter_acronyms = filter_acronyms
        self.separator = separator

        self.filter_min_score = filter_min_score
        self.filter_nan = filter_nan
        self._add_to_params("filter_min_score")

    def load(self, dataset):
        """Loads and processes the column specified by `column_name` in the
        dataset and generates the entity and scores matrices.

        For `CSColumn`, the entity matrix rows correspond to the rows of
        IdentityColumn. For each value in the comma separated column, a column
        is added to the matrix and a row is added to scores.

        If specified, filters the entities that appear less than
        `filter_min_score` times in the rows and does not create a column for
        these entities in the entity matrix and does not create an entry for
        these entities in scores.

        If specified, selects the entries depending on the whitelist and
        blacklist values.

        Parameters
        ----------
        dataset: cartodata.pipeline.datasets.Dataset
            The dataset from which the column will be loaded

        Returns
        -------
        scipy.sparse.csr_matrix, pandas.core.series.Series
            A sparse matrix and a pandas series

        """
        logger.info(f"Loading {self.nature} nature...")

        df = dataset.df
        whitelist, blacklist = self._load_blacklist_whitelist(
            dataset.input_dir
        )

        matrix, scores = load_comma_separated_column(
            df, self.column_name, whitelist, blacklist, self.banned_keywords,
            self.filter_acronyms, self.separator
        )

        if self.filter_min_score > 0:
            logger.info(
                f"The matrix has {matrix.shape[1]} columns. Filtering "
                f"{self.nature} with less than {self.filter_min_score} "
                "entities."
            )
            matrix, scores = filter_min_score(matrix, scores,
                                              self.filter_min_score)
            logger.info(
                f"Working with {matrix.shape[1]} {self.nature}."
            )

        if self.filter_nan:
            indices_to_keep = np.where(scores.index != "nan")[0]
            matrix = matrix[:, indices_to_keep]
            scores = scores.drop(labels=["nan"])

        return matrix, scores

    def _load_blacklist_whitelist(self, input_dir):
        whitelist = self.whitelist
        blacklist = self.blacklist

        if isinstance(self.whitelist, str) and self.whitelist != "":
            whitelist = self._load_list(
                input_dir / self.whitelist
            )
        if isinstance(self.blacklist, str) and self.blacklist != "":
            blacklist = self._load_list(
                input_dir / self.blacklist
            )

        return whitelist, blacklist

    def _load_list(self, list_file):
        if list_file:
            with open(list_file) as file:
                list_file = file.read().split(',')
        return list_file


class FilterColumn(Column):
    """A column class that represents an entity generated by filtering a
    certain column in the dataset with a certain value.

    For `FilterColumn`, the entity matrix rows correspond to a subset of
    the rows of IdentityColumn.

    Attributes
    ----------
    nature: str
        The nature of the column entity
    column_name: str
        The name of the column that uniquely identifies the rows in the dataset
    filter_column: str
        The column name that will be used to filter the rows in the dataset
    filter_value: str, default=None
        The value that will be used in `filter_column` to filter the
    rows. If set to `None`, checks for isnan.
    isnan: bool
        Filters the the rows with `NaN` value if set to True; filters the
    rows with non `Nan` value id set to False.
    scores_index_column: str
        The column name to be used as index for the generated `scores`
    scores_value_column: str
        The column name to be used as value in the generated scores

    Methods
    -------
    load()
        Loads and processes the column specified by `column_name` in the
    dataset and generates the entity and scores matrices.

    """

    yaml_tag = u'!FilterColumn'

    def __init__(self, nature, column_name, filter_column, filter_value=None,
                 isnan=None, scores_index_column=None,
                 scores_value_column=None):
        """
        Parameters
        ----------
        nature: str
            The nature of the column entity
        column_name: str
            The name of the column that uniquely identifies the rows in the
        dataset
        filter_column: str
            The column name that will be used to filter the rows in the dataset
        filter_value: str, default=None
            The value that will be used in `filter_column` to filter the
        rows. If set to `None`, checks for isnan.
        isnan: bool, default=None
            Filters the the rows with `NaN` value if set to True; filters the
                 rows with non `Nan` value id set to False.
        scores_index_column: str, default=None
            The column name to be used as index for the generated `scores`
        scores_value_column: str, default=None
            The column name to be used as value in the generated scores

        """
        super().__init__(Columns.FILTER, nature, column_name)

        assert filter_value is not None or isnan is not None, (
            "Please specify wither filter_value or isnan!"
        )

        self.filter_column = filter_column
        self.filter_value = filter_value
        self.isnan = isnan
        self.scores_index_column = scores_index_column
        self.scores_value_column = scores_value_column
        self._add_to_params("filter_value")

    def load(self, dataset):
        """Loads and processes the column specified by `column_name` in the
        dataset and generates the entity and scores matrices.

        Parameters
        ----------
        dataset: cartodata.pipeline.datasets.Dataset
            The dataset from which the column will be loaded

        Returns
        -------
        scipy.sparse.dia_matrix, pandas.core.series.Series
            A sparse matrix and a pandas series

        """
        df = dataset.df

        return load_filter_column(
            df, self.column_name, self.filter_column, self.filter_value,
            self.isnan, self.scores_index_column, self.scores_value_column
        )


class CorpusColumn(Column):
    """A column class that represents an entity specified by multiple text
    columns in the dataset.

    For `CorpusColumn`, the entity matrix rows correspond to the rows of
    IdentityColumn. For each n-gram extracted from combined text of specified
    columns, a column is added to the matrix.

    Attributes
    ----------
    nature: str
        The nature of the column entity
    column_names: list
        The list of column names in the dataset
    type: Columns
        The type of this column. Its value should be Columns.CORPUS
    stopwords: str
       The name of the stopwords file, or the URL for file that starts with
    http. It should be located under input_dir of the dataset.
    english: bool
        If True it will get union of the specified stopwords file with
    `sklearn.feature_extraction.text.ENGLISH_STOP_WORDS`.
    nb_grams: int
        Maximum n for n-grams.
    min_df: float in range [0.0, 1.0] or int
        When building the vocabulary ignore terms that have a document
    frequency strictly lower than the given threshold. This value is also
    called cut-off in the literature. If float, the parameter represents a
    proportion of documents, integer absolute counts. This parameter is
    ignored if vocabulary is not None.
    max_df: float in range [0.0, 1.0] or int
        When building the vocabulary ignore terms that have a document
    frequency strictly higher than the given threshold (corpus-specific
    stopwords). If float, the parameter represents a proportion of documents,
    integer absolute counts. This parameter is ignored if vocabulary is not
    None.
    vocab_sample: int
        Sample size from the corpus to train the vectorizer
    max_features:int
        If not None, build a vocabulary that only consider the top
    max_features ordered by term frequency across the corpus. Otherwise, all
    features are used.
    strip_accents: {‘ascii’, ‘unicode’} or callable
    lowercase: bool
        Convert all characters to lowercase before tokenizing
    min_word_length: int
        Minimum word length
    normalize: bool
        Normalizes the returned matrix if set to True
    vocabulary: str or set
        Either a set of terms or a file name that contains the terms one in
    each line, or the URL for file that starts with http.

    Methods
    -------
    load()
        Loads and processes the column specified by `column_name` in the
    dataset and generates the entity and scores matrices.
    get_corpus(df)
        Creates a text column merging the values of the columns putting
    " . " between column values specified for this column.


    Corpus column uses custom
    `cartodata.loading.PunctuationCountVectorizer` which splits the
    document on "[.,;]" and builds n-grams from each segment separately.

    """

    yaml_tag = u'!CorpusColumn'

    def __init__(self, nature, column_names, stopwords=None, english=True,
                 nb_grams=4, min_df=1, max_df=1.0, vocab_sample=None,
                 max_features=None, strip_accents="unicode",
                 lowercase=True, min_word_length=5, normalize=True,
                 vocabulary=None):
        """
        Parameters
        ----------
        nature: str
            The nature of the column entity
        column_names: list
            The list of column names in the dataset
        type: Columns
            The type of this column. Its value should be Columns.CORPUS
        stopwords: str, default=None
           The name of the stopwords file, or the URL for file that starts with
        http. It should be located under input_dir of the dataset.
        english: bool, default=True
            If True it will get union of the specified stopwords file with
        `sklearn.feature_extraction.text.ENGLISH_STOP_WORDS`.
        nb_grams: int, default=4
            Maximum n for n-grams.
        min_df: float in range [0.0, 1.0] or int, default=1
            When building the vocabulary ignore terms that have a document
        frequency strictly lower than the given threshold. This value is also
        called cut-off in the literature. If float, the parameter represents a
        proportion of documents, integer absolute counts. This parameter is
        ignored if vocabulary is not None.
        max_df: float in range [0.0, 1.0] or int, default=1.0
            When building the vocabulary ignore terms that have a document
        frequency strictly higher than the given threshold (corpus-specific
        stopwords). If float, the parameter represents a proportion of
        documents, integer absolute counts. This parameter is ignored if
        vocabulary is not None.
        vocab_sample: int, default=None
            Sample size from the corpus to train the vectorizer
        max_features:int, default=None
            If not None, build a vocabulary that only consider the top
        max_features ordered by term frequency across the corpus. Otherwise,
        all features are used. If vocabulary is not None, this parameter is
        ignored.
        strip_accents: {‘ascii’, ‘unicode’} or callable, default=unicode
        lowercase: bool, default=True
            Convert all characters to lowercase before tokenizing
        min_word_length: int, default 5
            Minimum word length
        normalize: bool, default=True
            Normalizes the returned matrix if set to True
        vocabulary: str or set, default=None
            Either a set of terms or a file name that contains the terms one in
        each line, or the URL for file that starts with http.
        """
        self.type = Columns.CORPUS

        self.nature = nature
        self.column_names = column_names
        self.stopwords = stopwords
        self.english = english
        self.nb_grams = nb_grams
        self.min_df = min_df
        self.max_df = max_df
        self.vocab_sample = vocab_sample
        self.max_features = max_features
        self.strip_accents = strip_accents
        self.lowercase = lowercase
        self.min_word_length = min_word_length
        self.normalize = normalize
        self.vocabulary = vocabulary
        self._all_params = []
        self._add_to_params(["column_names", "min_df", "max_df",
                             "vocab_sample", "max_features", "min_word_length",
                             "nb_grams"])

    def load(self, dataset):
        logger.info(f"Loading {self.nature} nature...")

        df = dataset.df
        stopwords = self._load_stopwords(
            dataset.input_dir, self.stopwords, self.english, dataset.name
        )

        corpus = self.get_corpus(df)

        vocab_sample = self.vocab_sample
        if vocab_sample is not None:
            if hasattr(dataset, "slice_type"):
                if dataset.slice_type == "cumulative":
                    vocab_sample = (
                        vocab_sample * (dataset.index + 1)
                    ) // dataset.slice_count
                elif dataset.slice_type == "discrete":
                    vocab_sample = vocab_sample // dataset.slice_count

        vocabulary = self._load_vocabulary(
            self.vocabulary, dataset.input_dir, dataset.name
        )

        matrix, scores = load_text_column(
            corpus, self.nb_grams, self.min_df, self.max_df,
            vocab_sample, self.max_features, self.strip_accents,
            self.lowercase, stopwords, self.min_word_length,
            vocabulary=vocabulary
        )

        if self.normalize:
            matrix = normalize_tfidf(matrix)

        return matrix, scores

    def get_corpus(self, df):
        """Creates a text column merging the values of the columns putting
        " . " between column values specified for this column.

        Returns
        -------
        pandas.Series
            a Series that contains merged text from the columns in CospusColumn
        """
        return df[self.column_names].apply(
            lambda row: ' . '.join(row.values.astype(str)), axis=1)

    def _load_stopwords(self, input_dir, stopwords, english, datasetname):
        if stopwords is None or stopwords == "":
            return []
        if stopwords.startswith("http"):
            url = stopwords
            stopwords = f"stopwords_{datasetname}.txt"
            download(url, input_dir / stopwords, kind="file", replace=False)
        stopwords = input_dir / stopwords

        with open(stopwords, 'r') as stop_file:
            if english:
                stopwords = list(ENGLISH_STOP_WORDS.union(
                    set(stop_file.read().splitlines())))
            else:
                stopwords = list(set(stop_file.read().splitlines()))

        return stopwords

    def _load_vocabulary(self, vocabulary, input_dir, datasetname):

        if vocabulary is not None:
            if isinstance(vocabulary, str):

                if vocabulary.startswith("http"):
                    url = vocabulary
                    vocabulary = f"vocabulary_{datasetname}.txt"
                    download(url, input_dir / vocabulary,
                             kind="file", replace=False)
                filepath = input_dir / vocabulary
                vocabulary = pd.read_csv(filepath).iloc[:, 0].values
                vocabulary = np.array(vocabulary, dtype=str)
                vocabulary = set(vocabulary)

            return vocabulary

        return None


class TfidfCorpusColumn(CorpusColumn):
    """A column class that represents an entity specified by multiple text
    columns in the dataset.

    For `TfidfCorpusColumn`, the entity matrix rows correspond to the rows of
    IdentityColumn. For each n-gram extracted from combined text of specified
    columns, a column is added to the matrix.

    Corpus column uses `sklearn.feature_extraction.text.TfidfVectorizer`.

    Attributes
    ----------
    nature: str
        The nature of the column entity
    column_names: list
        The list of column names in the dataset
    type: Columns
        The type of this column. Its value should be Columns.CORPUS
    stopwords: str
       The name of the stopwords file, or the URL for file that starts with
    http. It should be located under input_dir of the dataset.
    english: bool, default=True
        If True it will get union of the specified stopwords file with
    `sklearn.feature_extraction.text.ENGLISH_STOP_WORDS`.
    nb_grams: int, default=4
        Maximum n for n-grams.
    min_df: loat in range [0.0, 1.0] or int, default=1
        When building the vocabulary ignore terms that have a document
    frequency strictly lower than the given threshold. This value is also
    called cut-off in the literature. If float, the parameter represents a
    proportion of documents, integer absolute counts. This parameter is
    ignored if vocabulary is not None.
    max_df: float in range [0.0, 1.0] or int, default=1.0
        When building the vocabulary ignore terms that have a document
    frequency strictly higher than the given threshold (corpus-specific
    stopwords). If float, the parameter represents a proportion of documents,
    integer absolute counts. This parameter is ignored if vocabulary is not
    None.
    vocab_sample: int
        Sample size from the corpus to train the vectorizer
    max_features:int, default=None
        If not None, build a vocabulary that only consider the top
    max_features ordered by term frequency across the corpus. Otherwise, all
    features are used.
    strip_accents: {‘ascii’, ‘unicode’} or callable, default=unicode
    lowercase: bool, default=True
        Convert all characters to lowercase before tokenizing
    min_word_length: int, default 5
        Minimum word length
    normalize: bool, default=True
        Normalizes the returned matrix if set to True
    binary: discrete probabilistic models that model binary events rather
    than integer counts.
    encoding: str, default=’utf-8’
        If bytes or files are given to analyze, this encoding is used to
    decode.
    input: {‘filename’, ‘file’, ‘content’}, default=’content’
    If 'filename', the sequence passed as an argument to fit is expected
    to be a list of filenames that need reading to fetch the raw content
    to analyze.
    If 'file', the sequence items must have a ‘read’ method (file-like
    object) that is called to fetch the bytes in memory.
    If 'content', the input is expected to be a sequence of items that can
    be of type string or byte.
    vocabulary: str or set
        Either a set of terms or a file name that contains the terms one in
    each line, or the URL for file that starts with http.

    Methods
    -------
    load()
        Loads and processes the column specified by `column_name` in the
    dataset and generates the entity and scores matrices.
    get_corpus(df)
        Creates a text column merging the values of the columns putting
    " . " between column values specified for this column.

    """

    yaml_tag = u'!TfidfCorpusColumn'

    def __init__(self, nature, column_names, stopwords=None, english=True,
                 nb_grams=4, min_df=1, max_df=1.0, vocab_sample=None,
                 max_features=None, strip_accents="unicode",
                 lowercase=True, min_word_length=5, normalize=True,
                 input='content', binary=True, encoding="utf-8",
                 vocabulary=None):
        """
        Parameters
        ----------
        nature: str
            The nature of the column entity
        column_names: list
            The list of column names in the dataset
        type: Columns
            The type of this column. Its value should be Columns.CORPUS
        stopwords: str, default=None
           The name of the stopwords file, or the URL for file that starts with
        http. It should be located under input_dir of the dataset.
        english: bool, default=True
            If True it will get union of the specified stopwords file with
        `sklearn.feature_extraction.text.ENGLISH_STOP_WORDS`.
        nb_grams: int, default=4
            Maximum n for n-grams.
        min_df: loat in range [0.0, 1.0] or int, default=1
            When building the vocabulary ignore terms that have a document
        frequency strictly lower than the given threshold. This value is also
        called cut-off in the literature. If float, the parameter represents a
        proportion of documents, integer absolute counts. This parameter is
        ignored if vocabulary is not None.
        max_df: float in range [0.0, 1.0] or int, default=1.0
            When building the vocabulary ignore terms that have a document
        frequency strictly higher than the given threshold (corpus-specific
        stopwords). If float, the parameter represents a proportion of
        documents, integer absolute counts. This parameter is ignored if
        vocabulary is not None.
        vocab_sample: int, default=None
            Sample size from the corpus to train the vectorizer
        max_features:int, default=None
            If not None, build a vocabulary that only consider the top
        max_features ordered by term frequency across the corpus. Otherwise,
        all features are used.
        strip_accents: {‘ascii’, ‘unicode’} or callable, default=unicode
        lowercase: bool, default=True
            Convert all characters to lowercase before tokenizing
        min_word_length: int, default 5
            Minimum word length
        normalize: bool, default=True
            Normalizes the returned matrix if set to True
        binary: discrete probabilistic models that model binary events rather
        than integer counts.
        encoding: str, default=’utf-8’
            If bytes or files are given to analyze, this encoding is used to
        decode.
        input: {‘filename’, ‘file’, ‘content’}, default=’content’
        If 'filename', the sequence passed as an argument to fit is expected
        to be a list of filenames that need reading to fetch the raw content
        to analyze.
        If 'file', the sequence items must have a ‘read’ method (file-like
        object) that is called to fetch the bytes in memory.
        If 'content', the input is expected to be a sequence of items that can
        be of type string or byte.
        vocabulary: str or set, default=None
            Either a set of terms or a file name that contains the terms one in
        each line, or the URL for file that starts with http.
        """

        super().__init__(nature, column_names, stopwords, english, nb_grams,
                         min_df, max_df, vocab_sample, max_features,
                         strip_accents, lowercase, min_word_length, normalize,
                         vocabulary)

        self.binary = binary
        self.encoding = encoding
        self.input = input

    def load(self, dataset):
        logger.info(f"Loading {self.nature} nature...")

        df = dataset.df
        stopwords = self._load_stopwords(
            dataset.input_dir, self.stopwords, self.english, dataset.name
        )

        corpus = self.get_corpus(df)

        vocabulary = self._load_vocabulary(
            self.vocabulary, dataset. input_dir, dataset.name
        )

        tf_vectorizer = TfidfVectorizer(
            input=self.input, ngram_range=(1, self.nb_grams),
            min_df=self.min_df, max_df=self.max_df, binary=True,
            strip_accents=self.strip_accents, encoding=self.encoding,
            stop_words=stopwords, vocabulary=vocabulary)

        matrix = tf_vectorizer.fit_transform(corpus)

        scores = pd.Series(np.bincount(matrix.indices,
                                       minlength=matrix.shape[1]),
                           index=tf_vectorizer.get_feature_names_out())
        if self.normalize:
            matrix = normalize_tfidf(matrix)

        return matrix, scores
