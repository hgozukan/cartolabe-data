import logging
from hashlib import blake2b
from pathlib import Path

from download import download
import pandas as pd
from shutil import copy

from cartodata.pipeline.base import BaseProjection
from cartodata.pipeline.columns import Columns

logger = logging.getLogger(__name__)


class Dataset(BaseProjection):
    """A dataset class that holds the dataset to be processed in a
    pandas.DataFrame.

    Attributes
    ----------
    key: str
        The key that is used when saving entity matrices generated from the
    dataset
    name: str
        The name of the dataset
    version: str
        The version of the dataset
    df: pandas.DataFrame
        The dataset as `pandas.DataFrame`
    columns: list
        A list of columns of type `cartodata.pipeline.columns.Column`. The
    first column in the list is expected to be of type
    `cartodata.pipeline.columns.IdentityColumn`
    top_dir: str
        The parent directory where dataset should create its working directory.
    input_dir: str
        The directory to look for files necessary to load dataset columns.

    Methods
    -------

    set_columns(columns)
        Sets the columns of the dataset.
    create_matrices_and_scores()
        Creates matrices and scores for each entity specified by the columns of
        the dataset.
    """

    yaml_tag = u'!Dataset'

    def __init__(self, name, version, df, columns=None, top_dir="./dumps",
                 input_dir="./datas"):
        """
        Parameters
        ----------
        name: str
            The name of the dataset
        version: str
            The version of the dataset
        df: pandas.DataFrame
            The dataset as `pandas.DataFrame`
        columns: list, default=None
            A list of columns of type `cartodata.pipeline.columns.Column`. The
        first column in the list is expected to be of type
        `cartodata.pipeline.columns.IdentityColumn`
        top_dir: str, default="./dumps"
            The parent directory where dataset should create its working
        directory.
        input_dir: str, default="./datas"
            The directory to look for files necessary to load dataset columns.

        """
        assert name is not None and name != "", (
            "Please specify a valid dataset name!"
        )

        assert version is not None and version != "", (
            "Please specify a valid dataset version!"
        )

        assert df is None or (df is not None and df.shape[0] != 0), (
            "Please specify a valid Dataframe!"
        )

        assert top_dir is not None and top_dir != "", (
            "Please specify a valid top directory!"
        )

        assert input_dir is not None and input_dir != "", (
            "Please specify a valid input directory!"
        )

        self.key = "mat"
        self.name = name
        self.version = version
        self.top_dir = Path(top_dir)

        self.input_dir = input_dir

        self._df = df
        self._columns = columns

        if columns is not None:
            self.set_columns(self._columns)
        else:
            self.main_index = -1
            self.corpus_index = -1

    @property
    def input_dir(self):
        return self._input_dir

    @input_dir.setter
    def input_dir(self, input_dir):
        if input_dir is not None:
            self._input_dir = Path(input_dir).absolute()

    @property
    def working_dir(self):
        if not hasattr(self, '_working_dir') or self._working_dir is None:
            self._create_working_dir()

        return self._working_dir

    @working_dir.setter
    def working_dir(self, working_dir):
        assert working_dir is not None and working_dir != "", (
            "Please specify a valid working directory!"
        )
        working_dir = Path(working_dir)

        if not working_dir.exists():
            working_dir.mkdir(parents=True)

        self._working_dir = working_dir

    @property
    def digest(self):
        h = blake2b(digest_size=8)
        h.update(self.params.encode())
        return h.hexdigest()

    def _load(self):
        self._create_working_dir()

    def _create_working_dir(self):
        """Creates the working directory for the dataset. Working directory is
        located under `top_directory` with path:
        top_directory / dataset_name / dataset_version.
        """
        self.working_dir = self.top_dir / self.name / self.version

    def update_top_dir(self, top_dir):
        assert top_dir is not None and top_dir != "", (
            "Please specify a valid top directory!"
        )

        self.top_dir = Path(top_dir)
        self._create_working_dir()

        # TODO remove exworking dir if different or copy contents???
        # Copy contents creates problems if the new directory already contains
        # files

    @property
    def df(self):
        """Returns the dataframe of this dataset."""
        if self._df is None:
            self._df = self._load()
        return self._df

    @property
    def natures(self):
        """If dataset columns are set, returns the list of natures
        corresponding to the columns of the dataset; else returns
        an empty list."""
        if self._columns is None or len(self._columns) == 0:
            logger.info("The columns for the dataset are not specified yet. "
                        "Please use set_columns to specify dataset columns.")
            return []
        return [column.nature for column in self._columns]

    @property
    def columns(self):
        """If dataset columns are set, returns a shallow copy of the list of
        columns of the dataset; else returns an empty list."""
        if self._columns is None:
            logger.info("The columns for the dataset are not specified yet. "
                        "Please use set_columns to specify dataset columns.")
            return []
        return self._columns.copy()

    @property
    def corpus(self):
        """If the dataset columns are set and contains a
        `cartodata.pipeline.columns.CorpusColumn`, returns the corpus from that
        column.

        Returns
        -------
        pandas.Series
        """
        if self.corpus_index == -1:
            logger.info("No column of corpus type is set.")
            return None
        return self._columns[self.corpus_index].get_corpus(self.df)

    @property
    def params(self):
        """Returns the parameters of this dataset as string concatenated with
        `_`."""
        # TODO complete for all columns and all fields
        params = "_".join(
            [self.key] + [column.params for column in self._columns]
        )

        return params

    def set_columns(self, columns):
        """Sets the columns of the dataset.

        Parameters
        ----------
        columns: list
            A list of columns of type `cartodata.pipeline.columns.Column`. The
        first column in the list is expected to be of type
        `cartodata.pipeline.columns.IdentityColumn`. If there is a column of
        type `cartodata.pipeline.columns.CorpusColumn`, its index will be set
        to the value of corpus_index of the dataset. If there exists more then
        one column of type `cartodata.pipeline.columns.CorpusColumn`, it will
        take the index of last CorpusColumn as `corpus_index`.

        """
        assert isinstance(columns, list) or columns is None, (
            "Please specify a list for columns."
        )

        self.main_index = -1
        self.corpus_index = -1
        self._columns = columns

        if columns is not None:
            for i, column in enumerate(self._columns):
                if column.type is Columns.IDENTITY:
                    if i != 0:
                        raise ValueError(
                            "The first column should be IdentityColumn!"
                        )
                    self.main_index = 0
                elif column.type is Columns.CORPUS:
                    self.corpus_index = i

    def generate_entity_matrices(self, dir_mat, force=False, dump=True,
                                 return_mat=True):
        logger.info("Generating entity matrices...")

        matrices = None
        scores = None

        # if not force to regenerate matrices and the matrices already exist
        if not force and self.matrices_exist(self.natures, dir_mat):
            logger.info("Matrices already exist, will not regenerate.")
            if return_mat:
                matrices = self.load_matrices(self.natures, dir_mat)
                scores = self.load_scores(self.natures, dir_mat)

        else:
            matrices, scores = self.create_matrices_and_scores()
            logger.info('Entity matrices generated.')

            if dump:
                self.dump_scores(self.natures, scores, dir_mat)
                self.dump_matrices(self.natures, matrices, dir_mat)

        return matrices, scores

    def create_matrices_and_scores(self):
        """Creates matrices and scores for each entity specified by the columns
        of the dataset.

        Returns
        -------
        (list, list)
        A tuple of list of matrices and list of scores for each column entity.

        """
        assert self._columns is not None and len(self._columns) > 0, (
            "There are no columns defined for the dataset. Please set "
            "columns and retry!"
        )

        matrices = []
        scores = []

        for column in self._columns:

            column_tab, column_scores = column.load(self)
            matrices.append(column_tab)
            scores.append(column_scores)

        return matrices, scores


class FileDataset(Dataset):
    """A base dataset class that loads the data to a pandas.Dataframe from a
    file.

    Attributes
    ----------
    key: str
        The key that is used when saving entity matrices generated from the
    dataset
    name: str
        The name of the dataset
    version: str
        The version of the dataset
    filename: str
        The name of the filename for the dataset
    fileurl: str
        The URL for the dataset. `filename`should also be specified
    together with the `fileurl`. If the specified file does not exist
    under `input_dir`, it will be downloaded from the `fileurl` and saved to
    `working_dir`.
    columns: list
        A list of columns of type `cartodata.pipeline.columns.Column`. The
    first column in the list is expected to be of type
    `cartodata.pipeline.columns.IdentityColumn`
    top_dir: str
        The parent directory where dataset should create its working directory.
    input_dir: str
        The directory to look for files necessary to load dataset columns.

    Methods
    -------

    set_columns(columns)
        Sets the columns of the dataset.
    create_matrices_and_scores()
        Creates matrices and scores for each entity specified by the columns of
        the dataset.
    """

    def __init__(self, name, version, filename=None, fileurl=None,
                 columns=None, top_dir="./dumps", input_dir="./datas"):
        """
        Parameters
        ----------
        name: str
            The name of the dataset
        version: str
            The version of the dataset
        filename: str, default = None
            The name of the filename for the dataset
        fileurl: str, default = None
            The URL for the dataset. `filename`should also be specified
        together with the `fileurl`. If the specified file does not exist
        under `working_dir`, it will be downloaded from the `fileurl`.
        columns: list, default = None
            A list of columns of type `cartodata.pipeline.columns.Column`. The
        first column in the list is expected to be of type
        `cartodata.pipeline.columns.IdentityColumn`
        top_dir: str, default = "./dumps"
            The parent directory where dataset should create its working
        directory.
        input_dir: str, default="./datas"
            The directory to look for files necessary to load dataset columns.

        """

        assert (filename is not None and filename != "") or (
            fileurl is not None and fileurl != ""), (
            "Either filename or fileurl should be specified!"
        )

        if fileurl is not None and fileurl != "":
            assert filename is not None and filename != "", (
                "Please specify filename for the dataset at "
                f"{fileurl}"
            )

        super().__init__(name, version, None, columns, top_dir, input_dir)

        self._filename = filename
        self._fileurl = fileurl

    @property
    def filename(self):
        return self._filename

    @filename.setter
    def filename(self, filename):
        self._filename = filename
        self._index = -1
        self._df = None

    @property
    def fileurl(self):
        return self._fileurl

    @fileurl.setter
    def fileurl(self, fileurl):
        self._fileurl = fileurl
        self._index = -1
        self._df = None

    def _load(self):
        """Loads the contents of the dataset from the specified `filename`
        under `working_dir` or `fileurl`.

        Returns
        -------
        pandas.DataFrame
            The dataset as dataframe.

        """
        super()._load()
        if self._filename is not None and self._filename != "" and (
                (self._fileurl is None or self._fileurl == "")
        ):
            assert ((self.working_dir / self._filename).exists() or
                    (self.input_dir is not None and
                     (self.input_dir / self._filename).exists()
                     )
                    ), (
                f"The file {self.working_dir.absolute() / self._filename} "
                        "does not exist locally, please specify fileurl."
                    )


class CSVDataset(FileDataset):
    """A dataset class that loads the data from a CSV file using
    `pandas.DataFrame.read_csv`.

    Attributes
    ----------
    key: str
        The key that is used when saving entity matrices generated from the
    dataset
    name: str
        The name of the dataset
    version: str
        The version of the dataset
    filename: str
        The name of the filename for the dataset
    fileurl: str
        The URL for the dataset. `filename`should also be specified
    together with the `fileurl`. If the specified file does not exist
    under `working_dir`, it will be downloaded from the `fileurl`.
    columns: list
        A list of columns of type `cartodata.pipeline.columns.Column`. The
    first column in the list is expected to be of type
    `cartodata.pipeline.columns.IdentityColumn`
    top_dir: str
        The parent directory to save downloaded dataset file and files
    generated by processing the dataset.
    top_dir: str
        The parent directory where dataset should create its working
    directory.
    input_dir: str
        The directory to look for files necessary to load dataset columns.

    Methods
    -------

    set_columns(columns)
        Sets the columns of the dataset.
    create_matrices_and_scores()
        Creates matrices and scores for each entity specified by the columns of
        the dataset.
    """

    yaml_tag = u'!CSVDataset'

    def __init__(self, name, version, filename=None, fileurl=None,
                 columns=None, top_dir="./dumps", input_dir="./datas",
                 **kwargs):
        """
        Parameters
        ----------
        name: str
            The name of the dataset
        version: str, default = "0.0.0"
            The version of the dataset
        filename: str, default = None
            The name of the filename for the dataset
        fileurl: str, default = None
            The URL for the dataset. `filename`should also be specified
        together with the `fileurl`. If the specified file does not exist
        under `working_dir`, it will be downloaded from the `fileurl`.
        columns: list, default = None
            A list of columns of type `cartodata.pipeline.columns.Column`. The
        first column in the list is expected to be of type
        `cartodata.pipeline.columns.IdentityColumn`
        top_dir: str, default = "./dumps"
            The parent directory where dataset should create its working
        directory.
        input_dir: str, default="./datas"
            The directory to look for files necessary to load dataset columns.
        kwargs:
           Additional parameters to be passed to pd.read_csv method.

        """

        self.kwargs = kwargs

        super().__init__(name, version, filename, fileurl, columns,
                         top_dir, input_dir)

    def _load(self):
        """Loads the contents of the dataset from the specified `filename`
        under `working_dir` or `input_dir`. If the file does not exist locally,
        downloads the file from `fileurl`.

        Returns
        -------
        pandas.DataFrame
            The dataset as dataframe.

        """

        super()._load()
        raw_file = self.working_dir / self._filename

        if not raw_file.exists():
            input_file = self.input_dir / self._filename

            if not input_file.exists():
                logger.info(
                    f"{raw_file} does not exist. Downloading from "
                    f"{self._fileurl}"
                )
                download(self._fileurl, raw_file, kind='file',
                         progressbar=True, replace=False)
            else:
                copy(input_file, raw_file)

        return pd.read_csv(raw_file, **self.kwargs)


class SliceDataset(Dataset):
    """A dataset class that holds the dataset to be processed in a
    pandas.DataFrame and slices from the dataset in a list of
    `pandas.DataFrame`s.

    Attributes
    ----------
    key: str
        The key that is used when saving entity matrices generated from the
    dataset
    name: str
        The name of the dataset
    version: str
        The version of the dataset
    df: pandas.DataFrame
        The dataset as `pandas.DataFrame`
    dfs: list of pandas.DataFrame
        The slices of the dataset `df`
    slice_count: int
        The number of slices to divide the dataset, if `dfs` is specified,
        this value is ignored and set equal to the length of `dfs`.
    slice_type: {"cumulative", "repetitive", "discrete"}
        cumulative: each slice will contain cumulative data
        repetitive: each slice will contain the whole dataset.
        discrete: each slice will contain ** number of rows / slice_count**
    data.
    overlap: int
        If `slice_type='discrete'`, `overlap` defines the number of rows that
    should overlap in consecutive slices.
    sort_asc: str
        The column name to be used for sorting the dataset in ascending
    order.
    columns: list
        A list of columns of type `cartodata.pipeline.columns.Column`. The
    first column in the list is expected to be of type
    `cartodata.pipeline.columns.IdentityColumn`
    top_dir: str
        The parent directory where dataset should create its working
    directory.
    input_dir: str
        The directory to look for files necessary to load dataset columns.

    Methods
    -------

    set_columns(columns)
        Sets the columns of the dataset.
    create_matrices_and_scores()
        Creates matrices and scores for each entity specified by the columns of
        the dataset.
    """

    yaml_tag = u'!SliceDataset'

    def __init__(self, name, version, df, dfs=None, slice_count=2,
                 slice_type="cumulative", overlap=0, sort_asc=None,
                 columns=None, top_dir="./dumps", input_dir="./datas"):
        """
        Parameters
        ----------
        name: str
            The name of the dataset
        version: str, default="0.0.0"
            The version of the dataset
        df: pandas.DataFrame
            The dataset as `pandas.DataFrame`
        dfs: list of pandas.DataFrame, default=None
            The slices of the dataset `df`
        slice_count: int, default=2
            The number of slices to divide the dataset, if `dfs` is specified,
            this value is ignored and set equal to the length of `dfs`.
        slice_type: {"cumulative", "repetitive", "discrete"},
                    default="cumulative"
            cumulative: each slice will contain cumulative data
            repetitive: each slice will contain the whole dataset.
            discrete: each slice will contain ** number of rows / slice_count**
        data.
        overlap: int, default=0
            If `slice_type='discrete'`, `overlap` defines the number of rows
        that should overlap in consecutive slices.
        sort_asc: str, default=None
            The column name to be used for sorting the dataset in ascending
        order.
        columns: list, default=None
            A list of columns of type `cartodata.pipeline.columns.Column`. The
        first column in the list is expected to be of type
        `cartodata.pipeline.columns.IdentityColumn`
        top_dir: str, default = "./dumps"
            The parent directory where dataset should create its working
        directory.
        input_dir: str, default="./datas"
            The directory to look for files necessary to load dataset columns.

        """
        super().__init__(name, version, df, columns,
                         top_dir=top_dir, input_dir=input_dir)

        assert slice_count > 0, (
            "Please specify a slice_count greater than 0!"
        )
        assert slice_type in ["cumulative", "repetitive", "discrete"], (
            "Please specify a valid slice_type!"
        )

        assert overlap >= 0, (
            "Overlap value should be greater than or equal to 0!"
        )
        assert dfs is None or (dfs is not None and len(dfs) > 0), (
            "Please specify a valid list for dfs!"
        )

        self._dfs = dfs

        self.slice_type = slice_type
        self.slice_count = slice_count
        self.overlap = overlap
        self.sort_asc = sort_asc

        if self._dfs is not None and len(self._dfs) > 0:
            self._index = 0
        else:
            self._index = -1

    @property
    def df(self):
        """Returns the dataframe of this dataset."""
        if self._index == -1:
            self._load()
            self.create_slices(self.slice_count, self.slice_type,
                               self.overlap, self.sort_asc)

        return self._dfs[self._index]

    @property
    def index(self):
        """Returns the index of the current slice."""
        return self._index

    @index.setter
    def index(self, i):
        """Sets the index to specify the current slice of the dataset.

        Parameters
        ----------
        i: int
            The zero-based index of the slice of the dataset to set as current

        Raises:
           ValueError if the value specified is less than 0 or greater than the
           number of slices.
        """
        assert i >= 0 and i < self.slice_count, f"Invalid index value {i}!"

        if self._index == -1:
            self._load()
            self.create_slices(self.slice_count, self.slice_type,
                               self.overlap, self.sort_asc)

        self._index = i

    def create_slices(self, slice_count, slice_type="cumulative", overlap=0,
                      sort_asc=None):
        if self._index == -1:
            self._load()
        assert slice_count > 0, (
            "Please specify a slice_count greater than 0!"
        )
        assert slice_type in ["cumulative", "repetitive", "discrete"], (
            "Please specify a valid slice_type!"
        )

        assert overlap >= 0, (
            "Overlap value should be greater than or equal to 0!"
        )

        if sort_asc is not None and sort_asc != "":
            assert sort_asc in self._df.columns, (
                f"Column name {sort_asc} does not exist in the dataset!"
            )
            self._df = self._df.sort_values(by=sort_asc)

        self.slice_count = slice_count
        self.slice_type = slice_type
        self.sort_asc = sort_asc
        self.overlap = overlap

        nb_rows = self._df.shape[0]
        slice_size = nb_rows // slice_count

        assert self.overlap < slice_size, (
            "overlap should be less than slice_size!"
        )

        if slice_type == "repetitive":
            self._dfs = [self._df] * slice_count
        else:
            self._dfs = []
            iloc_start = 0
            iloc_end = slice_size

            for i in range(slice_count):
                if self.slice_type == "cumulative":
                    df_i = self._df.iloc[iloc_start: iloc_end]

                # slice_type == "discrete"
                else:
                    if i != 0:
                        start = iloc_start - overlap
                    else:
                        start = 0

                    if i != slice_count - 1:
                        end = iloc_end + overlap
                    else:
                        end = iloc_end

                    df_i = self._df.iloc[start: end]

                    iloc_start = iloc_end

                iloc_end += slice_size

                if slice_count - i == 2:
                    iloc_end += nb_rows % slice_count

                self._dfs.append(df_i)

        self._index = 0


class CSVSliceDataset(SliceDataset, CSVDataset):

    """A dataset class that holds the dataset to be processed in a
    pandas.DataFrame and slices from the dataset in a list of
    `pandas.DataFrame`s.

    Attributes
    ----------
    key: str
        The key that is used when saving entity matrices generated from the
    dataset
    name: str
        The name of the dataset
    version: str
        The version of the dataset
    filename: str
        The name of the filename for the dataset
    fileurl: str
        The URL for the dataset. `filename`should also be specified
    together with the `fileurl`. If the specified file does not exist
    under `working_dir`, it will be downloaded from the `fileurl`.
    slice_count: int
        The number of slices to divide the dataset, if `dfs` is specified,
        this value is ignored and set equal to the length of `dfs`.
    slice_type: {"cumulative", "repetitive", "discrete"}
        cumulative: each slice will contain cumulative data
        repetitive: each slice will contain the whole dataset.
        discrete: each slice will contain ** number of rows / slice_count**
    data.
    overlap: int
        If `slice_type='discrete'`, `overlap` defines the number of rows that
    should overlap in consecutive slices.
    sort_asc: str
        The column name to be used for sorting the dataset in ascending
    order.
    columns: list
        A list of columns of type `cartodata.pipeline.columns.Column`. The
    first column in the list is expected to be of type
    `cartodata.pipeline.columns.IdentityColumn`
    top_dir: str
        The parent directory where dataset should create its working directory.
    input_dir: str
        The directory to look for files necessary to load dataset columns.

    Methods
    -------

    set_columns(columns)
        Sets the columns of the dataset.
    create_matrices_and_scores()
        Creates matrices and scores for each entity specified by the columns of
        the dataset.
    """

    yaml_tag = u'!CSVSliceDataset'

    def __init__(self, name, version, filename=None, fileurl=None,
                 slice_count=2, slice_type="cumulative", overlap=0,
                 sort_asc=None, columns=None, top_dir="./dumps",
                 input_dir="./datas", **kwargs):
        """
        Parameters
        ----------
        name: str
            The name of the dataset
        version: str, default="0.0.0"
            The version of the dataset
        filename: str
            The name of the filename for the dataset
        fileurl: str
            The URL for the dataset. `filename`should also be specified
        together with the `fileurl`. If the specified file does not exist
        under `working_dir`, it will be downloaded from the `fileurl`.
        slice_count: int, default=2
            The number of slices to divide the dataset, if `dfs` is specified,
            this value is ignored and set equal to the length of `dfs`.
        slice_type: {"cumulative", "repetitive", "discrete"},
                    default="cumulative"
            cumulative: each slice will contain cumulative data
            repetitive: each slice will contain the whole dataset.
            discrete: each slice will contain ** number of rows / slice_count**
        data.
        overlap: int, default=0
            If `slice_type='discrete'`, `overlap` defines the number of rows
        that should overlap in consecutive slices.
        sort_asc: str, default=None
            The column name to be used for sorting the dataset in ascending
        order.
        columns: list, default=None
            A list of columns of type `cartodata.pipeline.columns.Column`. The
        first column in the list is expected to be of type
        `cartodata.pipeline.columns.IdentityColumn`
        top_dir: str, default = "./dumps"
            The parent directory where dataset should create its working
        directory.
        input_dir: str, default="./datas"
            The directory to look for files necessary to load dataset columns.

        """
        CSVDataset.__init__(self, name=name, version=version,
                            filename=filename, fileurl=fileurl,
                            columns=columns, top_dir=top_dir,
                            input_dir=input_dir, **kwargs)

        self._dfs = None

        self.slice_type = slice_type
        self.slice_count = slice_count
        self.overlap = overlap
        self.sort_asc = sort_asc

        self._index = -1

    def _load(self):
        self._df = CSVDataset._load(self)
