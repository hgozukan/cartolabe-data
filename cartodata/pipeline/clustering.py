import logging

from cartodata.clustering import (
    create_kmeans_clusters, create_hdbscan_clusters
)
from cartodata.pipeline.base import BaseProjection

logger = logging.getLogger(__name__)


def get_executor_clus(key):

    if key == "kmeans":
        return KMeansClustering
    elif key == "hdbscan":
        return HDBSCANClustering


class Clustering(BaseProjection):

    def __init__(self, key, n, base_factor, natures, label_length,
                 weight_name_length, random_state=42):
        super().__init__(key)

        self.n = n
        self.base_factor = base_factor
        self.natures = natures
        self.label_length = label_length
        self.weight_name_length = weight_name_length
        self.random_state = random_state

        # TODO to be updated with necessary parameters depending on the
        # clustering
        self._add_to_params("base_factor")

    def load_cluster_matrices(self, key_nD, key_2D, dir_clus):
        clus_scores = self.load_scores(self.natures, dir_clus)
        clus_nD = self.load_matrices(self.natures, dir_clus, key_nD)
        clus_2D = self.load_matrices(self.natures, dir_clus, key_2D)
        eval_pos = self.load_scores(self.natures, dir_clus, suffix="eval_pos")
        eval_neg = self.load_scores(self.natures, dir_clus, suffix="eval_neg")
        labels = self.load_labels(self.natures, dir_clus)

        return clus_nD, clus_2D, clus_scores, labels, eval_pos, eval_neg

    def load_execute(self, dataset, dir_mat, key_nD, dir_nD, key_2D, dir_2D,
                     dir_clus, dump=True, force=False, return_mat=False):
        """Loads nD and 2D matrices and executes clustering on them.
        If ``force`` is False, checks if the clustering matrices are already
        generated. Loads and returns them if ``return_mat`` is True.

        Parameters
        ----------
        """
        # if not force to regenerate matrices and the matrices already exist
        if (not force and self.matrices_exist(self.natures, dir_clus) and
                self.matrices_exist(self.natures, dir_clus)):
            logger.info(
                f"{self.key} matrices already exist, will not regenerate."
            )
            if return_mat:
                self.load_cluster_matrices(key_nD, key_2D, dir_clus)
            else:
                return None

        natures = dataset.natures
        matrices = dataset.load_matrices(natures, dir_mat)
        scores = dataset.load_scores(natures, dir_mat)
        matrices_nD = self.load_matrices(natures, dir_nD, key_nD)
        matrices_2D = self.load_matrices(natures, dir_2D, key_2D)

        return self.create_clusters(
            matrices, matrices_2D, matrices_nD, scores,
            dataset.corpus_index, key_nD=key_nD, key_2D=key_2D,
            dir_clus=dir_clus, dump=dump
        )

    def _create_clusters(self, nb_clusters, clustering_table, naming_table,
                         natural_space_naming_table, naming_scores,
                         previous_cluster_labels, naming_profile_table=None,
                         label_length=2, max_iter=100, n_init=1,
                         weight_name_length=0.5, random_state=None):
        raise NotImplementedError()

    def create_clusters(self, matrices, matrices_2D, matrices_nD, scores,
                        words_index, key_nD=None, key_2D=None, dir_clus=None,
                        dump=False):
        """
        Parameters
        ----------
        matrices: list
            List that contains matrices for each entity.
        matrices_2D: list
            List that contains 2-dimensional projection matrices for each
        entity.
        matrices_nD: list
            List that contains n-dimensional projection matrices for each
            entity.
        scores: list
            List that contains scores for each entity.
        words_index: int
            Index of the entity that corresponds to text corpus in the list of
        matrices and scores.
        dir_clus: str, path
            the path to save the matrices and scores for clustering
        dump: bool
            boolean value to indicate if the matrices and scores generated for
            clustering should be saved

        Returns
        -------
        clus_nD: list of numpy.ndarray
        clus_2D: list of numpy.ndarray
        clus_scores: list of pandas.core.series.Series [float]
        labels: list of list
        eval_pos: list of pandas.core.series.Series [float]
        eval_neg: list of pandas.core.series.Series [float]
        """
        cluster_labels = []
        clus_nD = []
        clus_2D = []
        clus_scores = []
        eval_pos = []
        eval_neg = []
        clus_labels = []

        for idx, nature in enumerate(self.natures):

            nb_clusters = self.n * (self.base_factor ** idx)
            labels_dict = {}
            labels_dict["nb_clusters"] = nb_clusters
            labels_dict["initial_labels"] = clus_labels.copy()

            logger.info(
                f"Starting {self.key} clustering for {nature}. "
                f"Nb clusters = {nb_clusters}."
            )

            (c_nD, c_2D, c_scores, c_lbs, c_wscores, cluster_eval_pos,
             cluster_eval_neg) = self._create_clusters(
                nb_clusters=nb_clusters,
                clustering_table=matrices_2D[0],
                naming_table=matrices_2D[words_index],
                natural_space_naming_table=matrices[words_index],
                naming_scores=scores[words_index],
                previous_cluster_labels=cluster_labels,
                naming_profile_table=matrices_nD[words_index]
            )

            # clus_lbs: list of numpy.ndarray
            # clus_wscores: list of numpy.ndarray
            labels_dict["c_lbs"] = c_lbs.tolist()
            labels_dict["c_wscores"] = c_wscores.tolist()

            clus_nD.append(c_nD)
            clus_2D.append(c_2D)
            clus_scores.append(c_scores)
            eval_pos.append(cluster_eval_pos)
            eval_neg.append(cluster_eval_neg)
            clus_labels.append(labels_dict)

        if dump:
            self.dump_scores(self.natures, clus_scores, dir_clus)
            self.dump_matrices(self.natures, clus_nD, dir_clus, key=key_nD)
            self.dump_matrices(self.natures, clus_2D, dir_clus, key=key_2D)
            self.dump_labels(self.natures, clus_labels, dir_clus)
            self.dump_scores(self.natures, eval_pos, dir_clus,
                             suffix="eval_pos")
            self.dump_scores(self.natures, eval_neg, dir_clus,
                             suffix="eval_neg")

        return clus_nD, clus_2D, clus_scores, clus_labels, eval_pos, eval_neg


class KMeansClustering(Clustering):

    yaml_tag = u'!KMeansClustering'

    ALL_PARAMS = [
        "natures",
        "n",
        "base_factor",
        "label_length",
        "max_iter",
        "n_init",
        "weight_name_length",
        "random_state"
    ]

    def __init__(self, natures=["hl_clusters", "ml_clusters"], n=8,
                 base_factor=3, label_length=2, max_iter=100, n_init=1,
                 weight_name_length=0.5, random_state=None):

        super().__init__("kmeans", n, base_factor, natures, label_length,
                         weight_name_length, random_state)
        self.max_iter = max_iter
        self.n_init = n_init

    def _create_clusters(self, nb_clusters, clustering_table, naming_table,
                         natural_space_naming_table, naming_scores,
                         previous_cluster_labels, naming_profile_table=None,
                         random_state=None):
        """
        Parameters
        ----------
        nb_clusters: int
            The number of clusters to create.
        clustering_table: numpy.ndarray
            The matrix to fit to create the clusters.
        naming_table: numpy.ndarray
            The matrix used to name clusters. This matrix should have the same
        number of rows as ``clustering_table``.
        natural_space_naming_table: scipy.sparse._csr.csr_matrix
            The naming_table in it's natural space. It should have the same
        number of columns as ``naming_table``.
        naming_scores: pandas.core.series.Series
            The scores for the ``naming_table`` columns.
        previous_cluster_labels: list
            A list of previous cluster labels, used to prevent duplicates in
        naming.
        naming_profile_table: numpy.ndarray
            An optional naming_table in projected space to build the cluster's
        profile with. If it is not provided, the ``naming_table`` will be used
        instead.

        Returns
        -------
        cluster_profiles: ndarray
        cluster_centers: ndarray, shape (n, )
        cluster_scores: pandas.core.series.Series [float]
        labels: numpy.ndarray
        score_words: numpy.ndarray
        cluster_eval_pos: pandas.core.series.Series [float]
        cluster_eval_neg: pandas.core.series.Series [float]
        """
        if random_state is None:
            random_state = self.random_state
        return create_kmeans_clusters(
            nb_clusters, clustering_table, naming_table,
            natural_space_naming_table, naming_scores,
            previous_cluster_labels, naming_profile_table,
            label_length=self.label_length, max_iter=self.max_iter,
            n_init=self.n_init, weight_name_length=self.weight_name_length,
            random_state=random_state
        )


class HDBSCANClustering(Clustering):

    yaml_tag = u'!HDBSCANClustering'

    ALL_PARAMS = [
        "natures",
        "n",
        "base_factor",
        "label_length",
        "weight_name_length",
        "min_cluster_size",
        "min_samples",
        "cache",
        "use_memory",
        "prop_labeled_threshold",
        "random_state"
    ]

    def __init__(self, natures=["hl_clusters", "ml_clusters"], n=8,
                 base_factor=3, label_length=2, weight_name_length=0.5,
                 min_cluster_size=15, min_samples=None, cache=None,
                 use_memory=True, prop_labeled_threshold=0.75,
                 random_state=None):

        super().__init__("hdbscan", n, base_factor, natures, label_length,
                         weight_name_length, random_state)
        self.min_cluster_size = min_cluster_size
        self.min_samples = min_samples
        self.cache = cache
        self.use_memory = use_memory
        self.prop_labeled_threshold = prop_labeled_threshold

    def _create_clusters(self, nb_clusters, clustering_table, naming_table,
                         natural_space_naming_table, naming_scores,
                         previous_cluster_labels, naming_profile_table=None):

        return create_hdbscan_clusters(
            nb_clusters, clustering_table, naming_table,
            natural_space_naming_table, naming_scores,
            previous_cluster_labels, naming_profile_table,
            label_length=self.label_length,
            weight_name_length=self.weight_name_length,
            min_cluster_size=self.min_cluster_size,
            min_samples=self.min_samples, cache=self.cache,
            use_memory=self.use_memory,
            prop_labeled_threshold=self.prop_labeled_threshold
        )
