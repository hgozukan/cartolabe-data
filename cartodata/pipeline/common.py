import logging
from pathlib import Path

from cartodata.exporting import Exporter
from cartodata.plotting import plot_map, save_fig, close_plots

logger = logging.getLogger(__name__)


def get_pipeline(dataset, top_dir, input_dir, projection_nd=None,
                 projection_2d=None, clustering=None, neighboring=None,
                 export_natures=None, hierarchical_dirs=False,
                 optimisation_seed=-1, aligned=False):

    if aligned:
        return AlignedPipeline(dataset, top_dir=top_dir, input_dir=input_dir,
                               projection_nd=projection_nd,
                               projection_2d=projection_2d,
                               clustering=clustering,
                               neighboring=neighboring,
                               export_natures=export_natures,
                               hierarchical_dirs=hierarchical_dirs,
                               optimisation_seed=optimisation_seed)

    return Pipeline(dataset, top_dir=top_dir, input_dir=input_dir,
                    projection_nd=projection_nd, projection_2d=projection_2d,
                    clustering=clustering, neighboring=neighboring,
                    export_natures=export_natures,
                    hierarchical_dirs=hierarchical_dirs,
                    optimisation_seed=optimisation_seed)


class Pipeline():
    yaml_tag = u'!Pipeline'

    def __init__(self, dataset=None, top_dir="./dumps", input_dir="./datas",
                 projection_nd=None, projection_2d=None, clustering=None,
                 neighboring=None, export_natures=None,
                 hierarchical_dirs=False, optimisation_seed=-1):

        self.dataset = dataset
        self.input_dir = Path(input_dir)

        self.projection_nd = projection_nd
        self.projection_2d = projection_2d
        self.clustering = clustering

        self.neighboring = neighboring

        self._export_natures = export_natures
        self.hierarchical_dirs = hierarchical_dirs
        self.optimisation_seed = optimisation_seed

        # this should be set after hierarchical_dirs
        self.top_dir = top_dir

    @property
    def top_dir(self):
        return self._top_dir

    @top_dir.setter
    def top_dir(self, top_dir):
        self._top_dir = Path(top_dir)
        if self.dataset is not None:
            self.dataset.input_dir = self.input_dir
            self.dataset.update_top_dir(self._top_dir)
            self._set_workdir_from_dataset()

    def _set_workdir_from_dataset(self):
        working_dir = self.dataset.working_dir
        if self.optimisation_seed > -1:
            working_dir = working_dir / str(self.optimisation_seed)

        if self.hierarchical_dirs:
            working_dir = (
                working_dir / self.dataset.params
            )
        self.working_dir = working_dir

    @property
    def natures(self):
        return self.dataset.natures

    @property
    def data(self):
        return self.dataset.df

    @property
    def working_dir(self):
        return self._working_dir

    @working_dir.setter
    def working_dir(self, working_dir):
        self._working_dir = Path(working_dir).absolute()
        if not self._working_dir.exists():
            self._working_dir.mkdir(parents=True)

    @property
    def export_natures(self):
        if self._export_natures is None:
            return []
        return self._export_natures.copy()

    @property
    def is_aligned(self):
        return False

    def update_dataset(self, dataset):
        if dataset is None:
            raise ValueError("Please specify a dataset!")

        self.dataset = dataset
        self.dataset.input_dir = self.input_dir
        dataset.update_top_dir(self._top_dir)
        self._set_workdir_from_dataset()

    def set_projection_nd(self, projection_nd):
        self.projection_nd = projection_nd

    def set_projection_2d(self, projection_2d):
        self.projection_2d = projection_2d

    def set_clustering(self, clustering):
        self.clustering = clustering

    def set_neighboring(self, neighboring):
        self.neighboring = neighboring

    def get_nD_dir(self):
        obj_dir = self.working_dir

        if self.hierarchical_dirs:
            obj_dir = (
                obj_dir / self.projection_nd.params
            )
        obj_dir.mkdir(parents=True, exist_ok=True)

        return obj_dir

    def get_2D_dir(self):
        obj_dir = self.get_nD_dir()

        if self.hierarchical_dirs:
            obj_dir = (
                obj_dir / self.projection_2d.params
            )
        obj_dir.mkdir(parents=True, exist_ok=True)

        return obj_dir

    def get_clus_dir(self):
        obj_dir = self.get_2D_dir()

        if self.hierarchical_dirs:
            obj_dir = (
                obj_dir / self.clustering.params
            )
        obj_dir.mkdir(parents=True, exist_ok=True)

        return obj_dir

    def get_neighbors_dir(self):
        obj_dir = self.get_nD_dir()

        if self.hierarchical_dirs:
            obj_dir = (
                obj_dir / self.neighboring.params
            )
        obj_dir.mkdir(parents=True, exist_ok=True)

        return obj_dir

    def title_parts_2D(self):
        title_parts = [self.dataset.name,
                       self.dataset.version,
                       self.dataset.digest,
                       self.projection_nd.params,
                       self.projection_2d.params]

        return title_parts

    def title_parts_clus(self, nature):
        title_parts = self.title_parts_2D()
        title_parts.extend([self.clustering.key, nature])
        return title_parts

    def run(self, save_plots=True, dump=True, force=False):
        """Generates entity matrices, makes n-dimesional and 2-dimensional
        projection, creates clusters, neighbors and saves to export.feather
        file.
        """

        # generates entity matrices for all natures if they are not already
        # generated and saves them
        self.generate_entity_matrices(return_mat=False, force=force)

        # executes projection if not already executed and saves the results
        self.do_projection_nD(dump=dump, return_mat=False, force=force)

        # find neighbors
        self.find_neighbors()

        # executes 2D projection if not already executed and saves the results
        self.do_projection_2D(dump=dump, return_mat=False, force=force)

        # creates clusters
        self.do_clustering(dump=dump, return_mat=False, force=force)

        if save_plots:
            self.save_plots()

        # saves all data to export.feather file
        self.export()

    def generate_entity_matrices(self, return_mat=True, force=False):
        return self.dataset.generate_entity_matrices(
            dir_mat=self.working_dir, force=force, dump=True,
            return_mat=return_mat
        )

    def do_projection_nD(self, dump=True, return_mat=True, force=False):
        return self.projection_nd.load_execute(
            self.dataset, dir_mat=self.working_dir, dir_nD=self.get_nD_dir(),
            dump=dump, force=force, return_mat=return_mat
        )

    def do_projection_2D(self, dump=True, return_mat=True, force=False):

        return self.projection_2d.load_execute(
            self.dataset.natures, self.projection_nd.key, self.get_nD_dir(),
            self.get_2D_dir(), dump=dump, force=force, return_mat=return_mat
        )

    def do_clustering(self, dump=True, return_mat=True, force=False):
        return self.clustering.load_execute(
            self.dataset, self.working_dir, self.projection_nd.key,
            self.get_nD_dir(), self.projection_2d.key, self.get_2D_dir(),
            self.get_clus_dir(), dump=dump, force=force, return_mat=return_mat
        )

    def find_neighbors(self, natures=None, force=False):
        if natures is None:
            natures = self.natures
        return self.neighboring.load_execute(
            natures, self.working_dir, self.projection_nd.key,
            self.get_nD_dir(), self.get_neighbors_dir(), force=force
        )

    def export(self, export_natures=None):
        if export_natures is not None:
            self._export_natures = export_natures

        self.exporter = Exporter(self.get_clus_dir(),
                                 self.natures + self.clustering.natures,
                                 self.natures,
                                 self.projection_2d.key,
                                 self.working_dir,
                                 self.get_neighbors_dir(),
                                 self.get_2D_dir(),
                                 self.projection_nd.key)

        if self._export_natures is not None:
            for export_nature in self._export_natures:
                export_nature.update_exporter(self)

        self.exporter.export_to_feather()

    def plot_map(self, matrices, labels, colors=None, title=None,
                 annotations=None, annotation_mat=None,
                 annotation_color='black'):
        return plot_map(matrices, labels, colors, title, annotations,
                        annotation_mat, annotation_color)

    def save_2D_plots(self, name_suffix="", file_ext=".png"):
        dir_2D = self.get_2D_dir()
        matrices_2D = self.projection_2d.load_matrices(self.natures, dir_2D)

        labels = tuple(self.natures)
        title_parts = self.title_parts_2D()
        if name_suffix != "":
            title_parts.append(name_suffix)

        fig_title = " ".join(title_parts)

        fig, ax = self.plot_map(
            matrices_2D, labels, title=fig_title
        )
        filename = "_".join(title_parts) + file_ext
        self.save_plot(fig, filename, working_dir=dir_2D)

        return (matrices_2D, (fig, filename))

    def save_plots(self, name_suffix="", file_ext=".png", save_2D=True):
        if save_2D:
            matrices_2D, _ = self.save_2D_plots(name_suffix, file_ext)
        else:
            matrices_2D = self.projection_2d.load_matrices(self.natures,
                                                           self.get_2D_dir())

        labels = tuple(self.natures)
        dir_clus = self.get_clus_dir()
        (_, clus_2D, clus_scores,
         _, _, _) = self.clustering.load_cluster_matrices(
             self.projection_nd.key, self.projection_2d.key, dir_clus
        )

        figs = []
        for i, nature in enumerate(self.clustering.natures):
            clus_scores_i = clus_scores[i]
            clus_mat_i = clus_2D[i]

            title_parts = self.title_parts_clus(nature)
            if name_suffix != "":
                title_parts.append(name_suffix)

            fig_title = " ".join(title_parts)
            filename = "_".join(title_parts) + file_ext

            fig_i, ax_i = self.plot_map(
                matrices_2D, labels, title=fig_title,
                annotations=clus_scores_i.index, annotation_mat=clus_mat_i
            )
            self.save_plot(fig_i, filename, working_dir=dir_clus)

            figs.append((fig_i, filename))

        return figs

    def save_plot(self, fig, fname, working_dir=None, **kwargs):
        if working_dir is None:
            working_dir = self.working_dir
        save_fig(fig, (working_dir / fname), **kwargs)

    def close_plots(self):
        close_plots()


class AlignedPipeline(Pipeline):

    yaml_tag = u'!AlignedPipeline'

    def __init__(self, dataset=None, top_dir='dumps', input_dir="datas",
                 prev_version=None, projection_nd=None, projection_2d=None,
                 clustering=None, neighboring=None, export_natures=None,
                 hierarchical_dirs=False, optimisation_seed=-1):

        super().__init__(dataset, top_dir, input_dir, projection_nd,
                         projection_2d, clustering, neighboring,
                         export_natures, hierarchical_dirs, optimisation_seed)

        self.prev_version = prev_version
        self._working_dirs = None

    @property
    def working_dirs(self):
        if self._working_dirs is None:
            self._create_working_dirs()

        return self._working_dirs

    @property
    def is_aligned(self):
        return True

    def _create_working_dirs(self):
        self._working_dirs = []
        for i in range(self.dataset.slice_count):
            dir_i = self.working_dir / f"s{i+1}"
            self._working_dirs.append(dir_i)
        self.main_working_dir = self.working_dir

    def set_current_slice(self, i):
        self.dataset.index = i
        self.working_dir = self.working_dirs[i]

    def update_dataset(self, dataset):
        super().update_dataset(dataset)

        if self.working_dirs != []:
            self._create_working_dirs()

    def generate_entity_matrices(self, return_mat=True, force=False):
        matrices_all = []
        scores_all = []

        for i in range(self.dataset.slice_count):
            self.set_current_slice(i)
            matrices_i, scores_i = super().generate_entity_matrices(
                return_mat=return_mat, force=force
            )
            if return_mat:
                matrices_all.append(matrices_i)
                scores_all.append(scores_i)

        self.working_dir = self.main_working_dir

        if return_mat:
            return matrices_all, scores_all

    def do_projection_nD(self, dump=True, return_mat=True, force=False):
        matrices_nD_all = []

        for i in range(self.dataset.slice_count):
            self.set_current_slice(i)

            matrices_nD_i = super().do_projection_nD(
                dump=dump, return_mat=return_mat, force=force
            )
            matrices_nD_all.append(matrices_nD_i)

        self.working_dir = self.main_working_dir

        if return_mat:
            return matrices_nD_all

    def do_projection_2D(self, prev_version=None, dump=True, return_mat=True,
                         force=False, **kwargs):
        """
        """
        if prev_version is None:
            prev_version = self.prev_version

        dirs_mat = []
        dirs_nD = []
        dirs_2D = []

        for i in range(self.dataset.slice_count):
            self.set_current_slice(i)

            dirs_mat.append(self.working_dir)
            dirs_nD.append(self.get_nD_dir())
            dirs_2D.append(self.get_2D_dir())

        self.working_dir = self.main_working_dir

        return self.projection_2d.load_execute(
            self.dataset.natures, dirs_mat, self.projection_nd.key, dirs_nD,
            dirs_2D, self.main_working_dir,
            current_version=self.dataset.version, prev_version=prev_version,
            dump=dump, force=force, return_mat=return_mat, **kwargs
        )

    def do_clustering(self, dump=True, return_mat=True, force=False):
        clus_nD_all = []
        clus_2D_all = []
        clus_scores_all = []
        clus_labels_all = []
        clus_eval_pos_all = []
        clus_eval_neg_all = []

        for i in range(self.dataset.slice_count):
            self.set_current_slice(i)

            (clus_nD, clus_2D, clus_scores,
             clus_labels, clus_eval_pos,
             clus_eval_neg) = super().do_clustering(
                 dump=dump, return_mat=return_mat, force=force
            )

            if return_mat:
                clus_nD_all.append(clus_nD)
                clus_2D_all.append(clus_2D)
                clus_scores_all.append(clus_scores)
                clus_labels_all.append(clus_labels)
                clus_eval_pos_all.append(clus_eval_pos)
                clus_eval_neg_all.append(clus_eval_neg)

        self.working_dir = self.main_working_dir
        if return_mat:
            return (clus_nD_all, clus_2D_all, clus_scores_all,
                    clus_labels_all, clus_eval_pos_all, clus_eval_neg_all)

    def find_neighbors(self):

        for i in range(self.dataset.slice_count):
            self.set_current_slice(i)
            super().find_neighbors()

        self.working_dir = self.main_working_dir

    def save_2D_plots(self, name_suffix="", file_ext=".png"):
        # if called by save_plots function of pipeline
        if self.working_dir != self.main_working_dir:
            return super().save_2D_plots(name_suffix, file_ext)

        results = []
        for i in range(self.dataset.slice_count):
            self.set_current_slice(i)
            name_suffix_current = f"{name_suffix}s{i + 1}"
            results.append(super().save_2D_plots(name_suffix_current,
                                                 file_ext))

        self.working_dir = self.main_working_dir
        return results

    def save_plots(self, name_suffix="", file_ext=".png", save_2D=True):
        # if called by save_plots function of pipeline
        if self.working_dir != self.main_working_dir:
            return super().save_plots(name_suffix, file_ext, save_2D)
        results = []
        for i in range(self.dataset.slice_count):
            self.set_current_slice(i)
            name_suffix_current = f"{name_suffix}s{i + 1}"
            results.append(super().save_plots(name_suffix_current, file_ext,
                                              save_2D))
            name_suffix

        self.working_dir = self.main_working_dir
        return results

    def export(self, export_natures=None):

        for i in range(self.dataset.slice_count):
            self.set_current_slice(i)
            super().export(export_natures)

            if i == self.dataset.slice_count - 1:
                target = self.working_dir / "export.feather"

        self.working_dir = self.main_working_dir

        # create a symbolic link to the final export.feather created under the
        # dataset directory
        export_file_link = (self.working_dir / "export.feather")
        export_file_link.unlink(missing_ok=True)
        export_file_link.symlink_to(target)


class GuidedAlignedPipeline(AlignedPipeline):

    yaml_tag = u'!GuidedAlignedPipeline'

    def __init__(self, dataset=None, top_dir='dumps', input_dir="datas",
                 prev_version=None, projection_nd=None, projection_2d=None,
                 clustering=None, neighboring=None, export_natures=None,
                 hierarchical_dirs=False, optimisation_seed=-1):

        super().__init__(dataset, top_dir, input_dir, prev_version,
                         projection_nd, projection_2d, clustering, neighboring,
                         export_natures, hierarchical_dirs, optimisation_seed)

    def get_guided_dir(self):
        dir_guided = self.working_dir / "final"
        dir_guided.mkdir(parents=True, exist_ok=True)
        return dir_guided

    def get_guided_nD_dir(self):
        obj_dir = self.get_guided_dir()

        if self.hierarchical_dirs:
            obj_dir = (
                obj_dir / self.projection_nd.params
            )
        obj_dir.mkdir(parents=True, exist_ok=True)

        return obj_dir

    def get_guided_neighbors_dir(self):
        obj_dir = self.get_guided_nD_dir()

        if self.hierarchical_dirs:
            obj_dir = (
                obj_dir / self.neighboring.params
            )
        obj_dir.mkdir(parents=True, exist_ok=True)

        return obj_dir

    def get_guided_2D_dir(self):
        obj_dir = self.get_guided_nD_dir()

        if self.hierarchical_dirs:
            obj_dir = (
                obj_dir / self.projection_2d.params
            )
        obj_dir.mkdir(parents=True, exist_ok=True)

        return obj_dir

    def get_guided_clus_dir(self):
        obj_dir = self.get_guided_2D_dir()

        if self.hierarchical_dirs:
            obj_dir = (
                obj_dir / self.clustering.params
            )
        obj_dir.mkdir(parents=True, exist_ok=True)

        return obj_dir

    def generate_entity_matrices(self, return_mat=True, force=False):

        matrices_all, scores_all = super().generate_entity_matrices(
            return_mat=True, force=force
        )
        self.dataset.dump_matrices(self.dataset.natures, matrices_all[-1],
                                   self.get_guided_dir())
        self.dataset.dump_scores(self.dataset.natures, scores_all[-1],
                                 self.get_guided_dir())
        if return_mat:
            return matrices_all, scores_all

    def do_projection_nD(self, dump=True, return_mat=True, force=False):

        matrices_nD_all = super().do_projection_nD(dump=dump, return_mat=True,
                                                   force=force)

        self.projection_nd.dump_matrices(self.dataset.natures,
                                         matrices_nD_all[-1],
                                         self.get_guided_nD_dir())
        if return_mat:
            return matrices_nD_all

    def find_neighbors(self, natures=None, force=False):
        if natures is None:
            natures = self.natures
        return self.neighboring.load_execute(
            natures, self.get_guided_dir(), self.projection_nd.key,
            self.get_guided_nD_dir(), self.get_guided_neighbors_dir(),
            force=force
        )

    def do_projection_2D(self, prev_version=None, dump=True, return_mat=True,
                         force=False, **kwargs):
        if prev_version is None:
            prev_version = self.prev_version

        dirs_mat = []
        dirs_nD = []
        dirs_2D = []

        for i in range(self.dataset.slice_count):
            self.set_current_slice(i)

            dirs_mat.append(self.working_dir)
            dirs_nD.append(self.get_nD_dir())
            dirs_2D.append(self.get_2D_dir())

        self.working_dir = self.main_working_dir

        return self.projection_2d.load_execute(
            self.dataset.natures, dirs_mat, self.projection_nd.key, dirs_nD,
            dirs_2D, dir_reducer=self.dataset.working_dir,
            current_version=self.dataset.version,
            prev_version=prev_version, dump=dump, force=force,
            dir_guided=self.get_guided_nD_dir(),
            dir_guided_2D=self.get_guided_2D_dir(),
            return_mat=return_mat
        )

    def do_clustering(self, dump=True, return_mat=True, force=False):
        return self.clustering.load_execute(
            self.dataset, self.get_guided_dir(), self.projection_nd.key,
            self.get_guided_nD_dir(), self.projection_2d.key2,
            self.get_guided_2D_dir(), self.get_guided_clus_dir(),
            dump=dump, force=force, return_mat=return_mat
        )

    def save_2D_plots(self, name_suffix="", file_ext=".png"):
        dir_2D = self.get_guided_2D_dir()
        matrices_2D = self.projection_2d.load_matrices(
            self.natures, dir_2D, key=self.projection_2d.key2
        )

        labels = tuple(self.natures)
        title_parts = self.title_parts_2D()
        if name_suffix != "":
            title_parts.append(name_suffix)

        fig_title = " ".join(title_parts)

        fig, ax = self.plot_map(
            matrices_2D, labels, title=fig_title
        )
        filename = "_".join(title_parts) + file_ext
        self.save_plot(fig, filename, working_dir=dir_2D)

        return (matrices_2D, (fig, filename))

    def save_plots(self, name_suffix="", file_ext=".png", save_2D=True):
        if save_2D:
            matrices_2D, _ = self.save_2D_plots(name_suffix, file_ext)
        else:
            matrices_2D = self.projection_2d.load_matrices(
                self.natures, self.get_guided_2D_dir(),
                key=self.projection_2d.key2
            )

        labels = tuple(self.natures)
        dir_clus = self.get_guided_clus_dir()
        (_, clus_2D, clus_scores,
         _, _, _) = self.clustering.load_cluster_matrices(
             self.projection_nd.key, self.projection_2d.key2, dir_clus
        )

        figs = []
        for i, nature in enumerate(self.clustering.natures):
            clus_scores_i = clus_scores[i]
            clus_mat_i = clus_2D[i]

            title_parts = self.title_parts_clus(nature)
            if name_suffix != "":
                title_parts.append(name_suffix)

            fig_title = " ".join(title_parts)
            filename = "_".join(title_parts) + file_ext

            fig_i, ax_i = self.plot_map(
                matrices_2D, labels, title=fig_title,
                annotations=clus_scores_i.index, annotation_mat=clus_mat_i
            )
            self.save_plot(fig_i, filename, working_dir=dir_clus)

            figs.append((fig_i, filename))

        return figs

    def export(self, export_natures=None):
        if export_natures is not None:
            self._export_natures = export_natures

        self.exporter = Exporter(self.get_guided_clus_dir(),
                                 self.natures + self.clustering.natures,
                                 self.natures,
                                 self.projection_2d.key2,
                                 self.get_guided_dir(),
                                 self.get_guided_neighbors_dir(),
                                 self.get_guided_2D_dir(),
                                 self.projection_nd.key)

        # TODO this is a quick fix but not nice
        dir_temp = self.working_dir
        self.working_dir = self.get_guided_dir()

        if self._export_natures is not None:
            for export_nature in self._export_natures:
                export_nature.update_exporter(self)

        self.exporter.export_to_feather()

        self.working_dir = dir_temp
