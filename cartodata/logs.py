import logging
import time

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'server': {
            'format': '[%(asctime)s] %(message)s',
        },
        'standard': {
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        },
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'  # noqa
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
        'timeit': {
            'format': 'TIMEIT %(message)s'
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
        'timeit': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'timeit'
        },
        'log_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'cartodata.log',
            'formatter': 'standard'
        },
        'server': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'server'
        },
    },
    'loggers': {
        'cartodata': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'cartodata.api.api': {
            'handlers': ['server'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'gensim': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True
        },
        'cartodata.timeit': {
            'handlers': ['timeit'],
            'level': 'DEBUG',
            'propagate': False,
        }
    }
}

timeit_logger = logging.getLogger('cartodata.timeit')


def timeit(func):
    """This decorator prints the execution time for the decorated function."""

    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        total = end - start
        if total < 1:
            timeit_logger.debug('%r  %2.2f ms' % (func.__name__, total * 1000))
        else:
            timeit_logger.debug('%r  %2.2f s' % (func.__name__, total))
        return result

    return wrapper
