# -*- coding: utf-8 -*-
"""
Created on Sat Feb 16 18:11:04 2019

@author: Moi
"""

import logging
import os
import json

import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.preprocessing import binarize
from sklearn.metrics.cluster import contingency_matrix
import random
from cartodata.projection import (
    guided_umap_projection, lsa_projection
)
from cartodata.loading import (
    load_text_column, load_identity_column, load_comma_separated_column
)
from cartodata.operations import (
    normalize_tfidf, export_to_json, load_matrices_from_dumps, load_scores,
    load_objects, normalize_l2, dump_objects
)
from cartodata.workflows.common import (
    dump_scores, get_all_neighbors, do_projection,
    dump_matrices, create_kmeans_clusters
)

logger = logging.getLogger(__name__)


def debatD_workflow(dump_dir):
    df = pd.read_csv('./datas/DEMOCRATIE_ET_CITOYENNETEfin.csv', sep=',')
    df['ville'] = df['authorZipCode'].astype(str)
    df['question'] = df['updatedAt'].astype(str).replace(',', ' ', regex=True)
    df['text'] = df['reference.1'].astype(str)
    do_workflow(df, 20, 0.1, None, None, 200, dump_dir)


def debatF_workflow(dump_dir):
    df = pd.read_csv(
        './datas/LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUESfin.csv', sep=','
    )
    df['ville'] = df['authorZipCode'].astype(str)
    df['question'] = df['updatedAt'].astype(str).replace(',', ' ', regex=True)
    df['text'] = df['reference.1'].astype(str)
    do_workflow(df, 20, 0.1, None, None, 200, dump_dir)


def debatE_workflow(dump_dir):
    df = pd.read_csv('./datas/LA_TRANSITION_ECOLOGIQUEfin.csv', sep=',')
    df['ville'] = df['authorZipCode'].astype(str)
    df['question'] = df['updatedAt'].astype(str).replace(',', ' ', regex=True)
    df['text'] = df['reference.1'].astype(str)
    do_workflow(df, 20, 0.1, None, None, 200, dump_dir)


def debatO_workflow(dump_dir):
    df = pd.read_csv(
        './datas/ORGANISATION_DE_LETAT_ET_DES_SERVICES_PUBLICSfin.csv', sep=','
    )
    df['ville'] = df['authorZipCode'].astype(str)
    df['question'] = df['updatedAt'].astype(str).replace(',', ' ', regex=True)
    df['text'] = df['reference.1'].astype(str)
#    df=df[:30000]
#    dfril = pd.read_csv('./datas/ril/crilfilt.csv', sep=',',names=["c0","c1"])
#    dfril['ville']="RIL"
#    dfril['question']="RIL"
#    dfril['text']=dfril["c1"]
#    df=df.append(dfril)

#    df=dfril

    do_workflow(df, 20, 0.1, None, None, 200,
                dump_dir, guided=False, load_lsa=False)
#    do_workflow(df, 20, 0.2, None, None, 200, dump_dir,guided=True,
#                load_lsa=True)
# dump_dir="./dumps/debat/"


def debat_workflow(dump_dir):
    df = pd.read_csv('./datas/DEBATfin.csv', sep=',')
    df['ville'] = df['authorZipCode'].astype(str)
    df['question'] = df['updatedAt'].astype(str).replace(',', ' ', regex=True)
    df['text'] = df['reference.1'].astype(str)
#    df=df[:30000]
#    dfril = pd.read_csv('./datas/ril/crilfilt.csv', sep=',',names=["c0","c1"])
#    dfril['ville']="RIL"
#    dfril['question']="RIL"
#    dfril['text']=dfril["c1"]
#    df=df.append(dfril)

#    df=dfril

    do_workflow(df, 30, 0.1, None, None, 400,
                dump_dir, guided=False, load_lsa=False)


def debatS_workflow(dump_dir):
    df = pd.read_csv('./datas/DEBATfin.csv', sep=',')
    df['ville'] = df['authorZipCode'].astype(str)
    df['question'] = df['updatedAt'].astype(str).replace(',', ' ', regex=True)
    df['text'] = df['reference.1'].astype(str)
    df = df[:30000]
#    dfril = pd.read_csv('./datas/ril/crilfilt.csv', sep=',',names=["c0","c1"])
#    dfril['ville']="RIL"
#    dfril['question']="RIL"
#    dfril['text']=dfril["c1"]
#    df=df.append(dfril)

#    df=dfril

    do_workflow(df, 20, 0.1, None, None, 200,
                dump_dir, guided=False, load_lsa=False)


def eval_stability(dump_dir,
                   natures=['proposition', 'ville', 'question', 'words',
                            'hl_clusters', 'ml_clusters', 'll_clusters',
                            'vll_clusters'],
                   subnatures=['proposition', 'ville', 'question', 'words'],
                   natures_cl=['hl_clusters', 'ml_clusters', 'll_clusters',
                               'vll_clusters'],
                   nb_clutest=10, umap_test_nb=0, replace_names=True):

    dim_dec = 10

    mats = load_matrices_from_dumps(subnatures, 'mat', dump_dir)
    lsa_ques = load_matrices_from_dumps([subnatures[2]], 'lsa', dump_dir)[0]
    words_mat = mats[3]

    first_names, second_names, orig_names, cluster_predictions = first_clustering(  # noqa
        dump_dir, natures=natures, natures_cl=natures_cl
    )

    test_clustering(dump_dir, natures=natures, natures_cl=natures_cl,
                    first_names=first_names, second_names=second_names,
                    orig_names=orig_names,
                    cluster_predictions=cluster_predictions,
                    nb_clutest=nb_clutest, sample_prop=.8)

    scorecomp = load_objects(["eval"], "score_comp", dump_dir)[0]
    firstnamecomp = load_objects(["eval"], "firstnamecomp", dump_dir)[0]
    secondnamecomp = load_objects(["eval"], "secondnamecomp", dump_dir)[0]
    all_names = load_objects(["eval"], "all_names", dump_dir)[0]
    orig_names = load_objects(["eval"], "orig_names", dump_dir)[0]

    scoreavg = []
    firstnameavg = []
    secondnameavg = []
    comp_names = []
    best_alt = []
    best_score = []
    for idx, nature in enumerate(natures_cl):
        scoreavg.append(np.mean(np.asarray(scorecomp)[:, idx], axis=0))
        firstnameavg.append(np.mean(np.asarray(firstnamecomp)[:, idx], axis=0))
        secondnameavg.append(
            np.mean(np.asarray(secondnamecomp)[:, idx], axis=0)
        )
        comp_names.append(np.stack(np.asarray(all_names)[:, idx]))
        nbest_alt = [np.unique(comp_names[idx][:, idc])[0]
                     for idc in range(len(orig_names[idx]))]
        nbest_score = [
            np.count_nonzero(comp_names[idx][:, idc] == nbest_alt[idc]) / len(
                comp_names[idx][0, :]) for idc in range(len(orig_names[idx]))
        ]
        best_alt.append(nbest_alt)
        best_score.append(nbest_score)

    scores = load_scores(natures_cl, dump_dir)
    if (replace_names):
        for idx, nature in enumerate(natures_cl):
            for idc in range(len(orig_names[idx])):
                namesl = scores[idx].index.values
                if (best_score[idx][idc] > firstnameavg[idx][idc]):
                    namesl[idc] = best_alt[idx][idc]
                scores[idx].index = namesl
        dump_scores(natures_cl, scores, dump_dir)

    for idx, nature in enumerate(natures_cl):
        for idc in range(len(orig_names[idx])):
            if (best_score[idx][idc] > firstnameavg[idx][idc]):
                print(str(idx) + "/" + str(idc) + ":")
                print(orig_names[idx][idc] + " sc " +
                      str(firstnameavg[idx][idc]) + "/" +
                      str(secondnameavg[idx][idc]) + " stab " +
                      str(scoreavg[idx][idc]))
                print("==> " + best_alt[idx][idc] +
                      " sc " + str(best_score[idx][idc]))

    dim_init = lsa_ques.shape[0]
    dim_act = dim_init
    if umap_test_nb > 0:
        for umap_nb in range(umap_test_nb):
            print("umap "+str(umap_nb))
            new_dump = os.path.join(dump_dir, str(umap_nb) + "/")
            try:
                os.mkdir(new_dump)
            except OSError:
                print("Creation of the directory %s failed" % new_dump)
            print("las+umap...")
            normalized_matrices, umap_matrices = do_projection(
                subnatures, mats, words_mat, dim_act, dump_dir, n_neighbors=15,
                min_dist=0.1, max_size=50000
            )
            print("umap done!")
            dump_matrices(subnatures, normalized_matrices, 'lsa', new_dump)
            dump_matrices(subnatures, umap_matrices, 'umap', new_dump)

            test_clustering(
                new_dump, natures=subnatures, natures_cl=natures_cl,
                first_names=first_names, second_names=second_names,
                orig_names=orig_names, cluster_predictions=cluster_predictions,
                nb_clutest=2, sample_prop=.8
            )

            scorecomp.extend(load_objects(["eval"], "score_comp", new_dump)[0])
            firstnamecomp.extend(
                load_objects(["eval"], "firstnamecomp", new_dump)[0]
            )
            secondnamecomp.extend(
                load_objects(["eval"], "secondnamecomp", new_dump)[0]
            )
            all_names.extend(load_objects(["eval"], "all_names", new_dump)[0])

            scoreavg = []
            firstnameavg = []
            secondnameavg = []
            comp_names = []
            best_alt = []
            best_score = []
            for idx, nature in enumerate(natures_cl):
                scoreavg.append(np.mean(np.asarray(scorecomp)[:, idx], axis=0))
                firstnameavg.append(
                    np.mean(np.asarray(firstnamecomp)[:, idx], axis=0)
                )
                secondnameavg.append(
                    np.mean(np.asarray(secondnamecomp)[:, idx], axis=0)
                )
                comp_names.append(np.stack(np.asarray(all_names)[:, idx]))
                nbest_alt = [
                    np.unique(comp_names[idx][:, idc])[
                        np.argmax(np.unique(comp_names[idx][:, idc],
                                            return_counts=True)[1])
                    ] for idc in range(len(orig_names[idx]))
                ]
                nbest_score = [
                    np.count_nonzero(
                        comp_names[idx][:, idc] == nbest_alt[idc]) / len(
                        all_names) for idc in range(len(orig_names[idx]))
                ]
                best_alt.append(nbest_alt)
                best_score.append(nbest_score)

            for idx, nature in enumerate(natures_cl):
                for idc in range(len(orig_names[idx])):
                    if (best_score[idx][idc] > firstnameavg[idx][idc]):
                        print(str(idx) + "/" + str(idc) + ":")
                        print(orig_names[idx][idc] + " sc " +
                              str(firstnameavg[idx][idc]) + "/" +
                              str(secondnameavg[idx][idc]) + " stab " +
                              str(scoreavg[idx][idc]))
                        print("==> "+best_alt[idx][idc] +
                              " sc " + str(best_score[idx][idc]))
            for idx, nature in enumerate(natures_cl):
                print(" 10/25/50/75 ")
                print(str(idx) + str(np.round(
                    np.percentile(scoreavg[idx], (10, 25, 50, 75)), 2)) +
                    " stab ")
                print(str(idx) + str(np.round(
                    np.percentile(best_score[idx], (10, 25, 50, 75)), 2)) +
                    " alt ")
                print(str(idx) + str(np.round(
                    np.percentile(firstnameavg[idx], (10, 25, 50, 75)), 2)) +
                    " firstname ")

            dim_act = dim_act-dim_dec
    return scores


def first_clustering(dump_dir, natures, natures_cl,
                     nb_clutest=10, sample_prop=.8):

    umap_matrices = load_matrices_from_dumps(natures, 'umap', dump_dir)
    clust_scores = load_scores(natures_cl, dump_dir)
    clust_kms = load_objects(natures_cl, "km", dump_dir)
    cluster_predictions = []
    first_names = []
    second_names = []
    orig_names = []

    for idx, nature in enumerate(natures_cl):
        km = clust_kms[idx]
    #    names_prediction = km.predict(words_umap.T)
        cluster_prediction = km.predict(umap_matrices[0].T)
        cluster_predictions.append(cluster_prediction)
        nfirst_names = clust_scores[idx].index.str.split(",").str[0].values
        nsecond_names = clust_scores[idx].index.str.split(",").str[1].values
        nfirst_names[pd.isnull(nfirst_names)] = ""
        nsecond_names[pd.isnull(nsecond_names)] = ""
        first_names.append(nfirst_names)
        second_names.append(nsecond_names)
        orig_names.append(clust_scores[idx].index.values)

    dump_objects(["eval"], [orig_names], "orig_names", dump_dir)
    return first_names, second_names, orig_names, cluster_predictions


def test_clustering(dump_dir, natures, natures_cl,
                    first_names, second_names, orig_names, cluster_predictions,
                    nb_clutest=10, sample_prop=.8):
    #    natures = ['proposition', 'ville','question', 'words','hl_clusters',
    #              'ml_clusters', 'll_clusters', 'vll_clusters']

    umap_matrices = load_matrices_from_dumps(natures, 'umap', dump_dir)
    lsa_matrices = load_matrices_from_dumps(natures, 'lsa', dump_dir)
    scores = load_scores(natures, dump_dir)
    words_mat = load_matrices_from_dumps(['words'], 'mat', dump_dir)[0]
    sample_size = int(len(scores[0])*sample_prop)
    cluster_labels = []
    scorecomp = []
    firstnamecomp = []
    secondnamecomp = []
    all_names = []

    for clt in range(nb_clutest):
        print("iter "+str(clt))
        new_scores = []
        nfscores = []
        nsscores = []
        nnames = []
        sample_val = random.sample(range(len(scores[0])), sample_size)
        sub_mat = umap_matrices[0][:, sample_val]
        sub_words_mat = words_mat[sample_val, :]
        for idx, nature in enumerate(natures_cl):
            n_clusters = len(first_names[idx])

            print(str(nature))
            c_lsa, c_umap, c_scores, c_km = create_kmeans_clusters(
                n_clusters, sub_mat, umap_matrices[3], sub_words_mat,
                scores[3], cluster_labels, lsa_matrices[3]
            )
            cluster_labels = []

            new_prediction = c_km.predict(sub_mat.T)
            comp = contingency_matrix(
                cluster_predictions[idx][sample_val], new_prediction)
            prop_old = (
                # max per old cluster
                np.max(comp, axis=1)/np.sum(comp, axis=1)
            )
#                compm=np.divide(comp.T,np.sum(comp,axis=1)).T
#                compm=np.divide(compm,np.sum(comp,axis=0))
            best_old = np.argmax(comp, axis=1)
            new_scores.append(prop_old)

#         prop_new=np.max(comp,axis=0)/np.sum(comp,axis=0) #max per old cluster

            new_fnames = c_scores.index.str.split(",").str[0].values[best_old]
            new_snames = c_scores.index.str.split(",").str[1].values[best_old]
            new_fnames[pd.isnull(new_fnames)] = ""
            new_snames[pd.isnull(new_snames)] = ""

            ffscore = [
                first_names[idx][idc].strip() == new_fnames[idc].strip()
                for idc in range(len(first_names[idx]))
            ]
            fsscore = [
                first_names[idx][idc].strip() == new_snames[idc].strip()
                for idc in range(len(first_names[idx]))
            ]
            fscore = np.add(ffscore, fsscore)*1.0
            sfscore = [
                second_names[idx][idc].strip() == new_fnames[idc].strip()
                for idc in range(len(first_names[idx]))
            ]
            ssscore = [
                second_names[idx][idc].strip() == new_snames[idc].strip()
                for idc in range(len(first_names[idx]))
            ]
            sscore = np.add(sfscore, ssscore)*1.0

            nfscores.append(fscore)
            nsscores.append(sscore)

            nnames.append(c_scores.index.values[best_old])

        scorecomp.append(new_scores)
        firstnamecomp.append(nfscores)
        secondnamecomp.append(nsscores)
        all_names.append(nnames)

    dump_objects(["eval"], [scorecomp], "score_comp", dump_dir)
    dump_objects(["eval"], [firstnamecomp], "firstnamecomp", dump_dir)
    dump_objects(["eval"], [secondnamecomp], "secondnamecomp", dump_dir)
    dump_objects(["eval"], [all_names], "all_names", dump_dir)

    scoreavg = []
    firstnameavg = []
    secondnameavg = []
    comp_names = []
    best_alt = []
    best_score = []
    for idx, nature in enumerate(natures_cl):
        scoreavg.append(np.mean(np.asarray(scorecomp)[:, idx], axis=0))
        firstnameavg.append(
            np.mean(np.asarray(firstnamecomp)[:, idx], axis=0)
        )
        secondnameavg.append(
            np.mean(np.asarray(secondnamecomp)[:, idx], axis=0)
        )
        comp_names.append(
            np.stack(np.asarray(all_names)[:, idx])
        )
        nbest_alt = [
            np.unique(comp_names[idx][:, idc])[np.argmax(np.unique(
                comp_names[idx][:, idc], return_counts=True)[1])]
            for idc in range(len(orig_names[idx]))
        ]
        nbest_score = [
            np.count_nonzero(
                comp_names[idx][:, idc] == nbest_alt[idc]
            ) / len(all_names)
            for idc in range(len(orig_names[idx]))
        ]
        best_alt.append(nbest_alt)
        best_score.append(nbest_score)

    dump_objects(natures_cl, scoreavg, "scoreavg", dump_dir)
    dump_objects(natures_cl, firstnameavg, "firstnameavg", dump_dir)
    dump_objects(natures_cl, secondnameavg, "firstnamecomp", dump_dir)
    dump_objects(natures_cl, comp_names, "secondnamecomp", dump_dir)
    dump_objects(natures_cl, best_alt, "best_alt", dump_dir)
    dump_objects(natures_cl, best_score, "best_score", dump_dir)

    for idx, nature in enumerate(natures_cl):
        for idc in range(len(first_names[idx])):
            if (best_score[idx][idc] > firstnameavg[idx][idc]):
                print(str(idx) + "/" + str(idc) + ":")
                print(orig_names[idx][idc] + " sc " +
                      str(firstnameavg[idx][idc]) + "/" +
                      str(secondnameavg[idx][idc]) + " stab " +
                      str(scoreavg[idx][idc]))
                print("==> "+best_alt[idx][idc] +
                      " sc "+str(best_score[idx][idc]))


'''
def debat_workflow(dump_dir):
    natures = ['proposition', 'ville','question', 'words','hl_clusters',
'ml_clusters', 'll_clusters', 'vll_clusters']
    umap_matrices=load_matrices_from_dumps(natures, 'umap', dump_dir)
#    lsa_matrices=load_matrices_from_dumps(natures, 'lsa', dump_dir)
    scores = load_scores(natures, dump_dir)
    scores[0]=replace_publication_score(dump_dir,"ll_clusters")
#    dump_scores(['proposition'], [scores[0]], dump_dir)
    export_file = os.path.join(dump_dir, 'export-llsimple.json')
    export_to_json(natures, umap_matrices, scores, export_file, natures[:4],
dump_dir)
#    scores[0]=replace_publication_score_cluster(dump_dir,"hl_clusters",5,"hl_clusters")
#    dump_scores(['proposition'], [scores[0]], dump_dir)

#    scores[0]=replace_publication_score_cluster(dump_dir,"ml_clusters",5,"hl_clusters")
#    dump_scores(['proposition'], [scores[0]], dump_dir)
#    export_file = os.path.join(dump_dir, 'export-ml.json')
#    export_to_json(natures, umap_matrices, scores, export_file, natures[:4],
dump_dir)

    scores[0]=replace_publication_score_cluster(dump_dir,"ll_clusters",5,"ml_clusters")
    dump_scores(['proposition'], [scores[0]], dump_dir)
    export_file = os.path.join(dump_dir, 'export-ll.json')
    export_to_json(natures, umap_matrices, scores, export_file, natures[:4],
dump_dir)

    scores[0]=replace_publication_score_cluster(dump_dir,"vll_clusters",5,"ll_clusters")
    dump_scores(['proposition'], [scores[0]], dump_dir)
    export_file = os.path.join(dump_dir, 'export.json')
    # export_to_json(natures, umap_matrices, scores, export_file)
    export_to_json(natures, umap_matrices, scores, export_file, natures[:4],
dump_dir)
    add_theme_points(dump_dir)
'''

'''
def separe_publications(dump_dir,nature_sep,val_sep):
    logger.info('separate free publications')
#    ques_list=[7,51]
#    ques_list=[7]
    old_natures = ['proposition', 'ville','question', 'words','theme',
'hl_clusters', 'ml_clusters', 'll_clusters', 'vll_clusters']
    old_natures = ['proposition', 'ville','question', 'words','hl_clusters',
'ml_clusters', 'll_clusters', 'vll_clusters']
    umap_matrices=load_matrices_from_dumps(old_natures, 'umap', dump_dir)
    lsa_matrices=load_matrices_from_dumps(old_natures, 'lsa', dump_dir)
    scores = load_scores(old_natures, dump_dir)
#    ques_is=
    mat_ques=load_matrices_from_dumps([nature_sep], 'mat', dump_dir)[0]
    free_ques=mat_ques[:,val_sep]
    qfree=free_ques.getnnz(axis=1)

    natures = ['proposition','ville','question', 'words','theme','hl_clusters',
'ml_clusters', 'll_clusters', 'vll_clusters','free_prop']
    natures = ['proposition','ville','question', 'words','hl_clusters',
'ml_clusters', 'll_clusters', 'vll_clusters','free_prop']

    lsa_prop=lsa_matrices[0]
    lsa_matrices[0]=lsa_prop[:,qfree==0]
    lsa_matrices.extend([lsa_prop[:,qfree>0]])
    scores_prop=scores[0]
    scores[0]=scores_prop[qfree==0]
    nscore=scores_prop[qfree>0]
    nscore.index=nscore.index.str.strip()
    scores.extend([scores_prop[qfree>0]])

    umap_prop=umap_matrices[0]
    umap_matrices[0]=umap_prop[:,qfree==0]
    umap_matrices.extend([umap_prop[:,qfree>0]])
    export_file = os.path.join(dump_dir, 'export_separated.json')
    logger.info('separated... saving')
    export_to_json(natures, umap_matrices, scores, export_file,None, dump_dir)
'''
"""
matrices=load_matrices_from_dumps(natures[:4], 'mat', dump_dir)
lsa_matrices=load_matrices_from_dumps(natures[:4], 'lsa', dump_dir)
umap_matrices=load_matrices_from_dumps(natures[:4], 'umap', dump_dir)
scores = load_scores(natures[:4], dump_dir)
"""
'''
def add_theme_points(dump_dir):
    logger.info('add themes... loading')
    old_natures = ['proposition', 'ville','question', 'words', 'hl_clusters',
'ml_clusters', 'll_clusters', 'vll_clusters']
    old_umap_matrices=load_matrices_from_dumps(old_natures, 'umap', dump_dir)
    old_lsa_matrices=load_matrices_from_dumps(old_natures, 'lsa', dump_dir)
    old_scores = load_scores(old_natures, dump_dir)
    clus_lsa=old_lsa_matrices[4:]
    clus_pos=old_umap_matrices[4:]
    clus_scores=old_scores[4:]
    natures = ['proposition', 'ville','question', 'words','theme',
'hl_clusters', 'ml_clusters', 'll_clusters', 'vll_clusters']
    logger.info('computing themes')
    theme_lsa, theme_pos, theme_score = transfert_clusters(
natures[4],natures[5:],clus_lsa, clus_pos, clus_scores,dump_dir)
#    neighbors_natures=natures[:4]
    logger.info('append...'+str(len(old_lsa_matrices))+"/"+str(theme_lsa[0].shape))
    lsa_matrices=old_lsa_matrices[:4]
    lsa_matrices.extend(theme_lsa)
    lsa_matrices.extend(old_lsa_matrices[4:])
    scores=old_scores[:4]
    scores.extend(theme_score)
    scores.extend(old_scores[4:])
    umap_matrices=old_umap_matrices[:4]
    umap_matrices.extend(theme_pos)
    umap_matrices.extend(old_umap_matrices[4:])

    logger.info('recompute neighbors... computing'+str(len(lsa_matrices)))
    power_scores = [0, 0, 0,0,0]
    neighbors_natures=natures[:5]
    get_all_neighbors(lsa_matrices[:5], scores, power_scores, dump_dir,
natures[:5])

    export_file = os.path.join(dump_dir, 'export_new.json')
    logger.info('add themes... saving')
    export_to_json(natures, umap_matrices, scores, export_file,
neighbors_natures, dump_dir)

def transfert_clusters(transfert_to, natures,clus_lsa, clus_pos, clus_scores,
dump_dir):
    theme_lsa = []
    theme_pos = []
    theme_scores = []
    for idx, nature in enumerate(natures):
        if idx==0:
            theme_lsa=clus_lsa[idx].copy()
            theme_scores=clus_scores[idx].copy()
            theme_pos=clus_pos[idx].copy()
        else:
            theme_lsa=np.hstack((theme_lsa,clus_lsa[idx]))
            theme_scores=theme_scores.append(clus_scores[idx])
            theme_pos=np.hstack((theme_pos,clus_pos[idx]))
    return [theme_lsa], [theme_pos], [theme_scores]


def replace_and_export(dump_dir,cluster_name="ll_clusters"):
#    natures = ['proposition', 'ville','question', 'words', 'theme',
'hl_clusters', 'ml_clusters', 'll_clusters', 'vll_clusters']
    natures = ['proposition', 'ville','question', 'words', 'hl_clusters',
'ml_clusters', 'll_clusters', 'vll_clusters']
    neighbors_natures=natures[:4]
    umap_matrices=load_matrices_from_dumps(natures, 'umap', dump_dir)
    scores = load_scores(natures, dump_dir)

    scores[0]=replace_publication_score_cluster(dump_dir,cluster_name)
    export_file = os.path.join(dump_dir, 'export_updated.json')
    export_to_json(natures, umap_matrices, scores, export_file,
neighbors_natures, dump_dir)

'''


def theme_synthesis(dump_dir, natures, proplevel="ll_clusters",
                    alt_cluster=False, spec_cluster=False, test_cluster=False):
    logger.info('Synthesis.')
# natures=['hl_clusters', 'ml_clusters', 'll_clusters', 'vll_clusters']
# if True:

    scorestot = pd.DataFrame(columns=["score"])
    hltop = pd.DataFrame(columns=["top"])
    mltop = pd.DataFrame(columns=["top"])
    lltop = pd.DataFrame(columns=["top"])
    colsavespre = ["top", "partalt", "part",
                   "nbprop", "nbwords", "stability", "name_stab"]
    colsavespost = ["prop", "no", "tophl", "topml", "topll"]
    colsaves = np.concatenate((colsavespre, natures, colsavespost))
    save = pd.DataFrame(columns=colsaves)
    if test_cluster:
        prop_umap = load_matrices_from_dumps(
            ['proposition'], 'umap', dump_dir)[0]
        prop_scores = load_scores(['propositionimp'], dump_dir)[0]
    #    prop_scores_post = load_scores(['proposition'], dump_dir)[0]
    #    prop_scores=prop_scores_post
    #    prop_len=prop_scores.index.str.len()
    #    prop_ln=np.log(1+prop_len)
        clust_kms = load_objects(natures, "km", dump_dir)

    if alt_cluster:
        SSpropList = load_objects(
            natures, "inter_cluster_SSpropList", dump_dir)
        # BBsupRecList = load_objects(
        #     natures, "inter_cluster_BBsupRecList", dump_dir)
        BBsupRecPropList = load_objects(
            natures, "inter_cluster_BBsupRecPropList", dump_dir)
        # bestrecList = load_objects(
        #     natures, "inter_cluster_bestrecList", dump_dir)

    if spec_cluster:
        BBsupOrigProp = load_objects(
            natures, "inter_cluster_BBsupOrigProp", dump_dir)
        BBsupOrigPart = load_objects(
            natures, "inter_cluster_BBsupOrigPart", dump_dir)
        # BBquant = load_objects(natures, "inter_cluster_BBquant", dump_dir)
    idc = -1
    for clunat in natures:
        idc = idc+1
#    clunat=natures[0]
        filename = os.path.join(dump_dir, clunat+"_"+"simple.csv")
        fich = pd.read_csv(filename)
        sortf = fich.sort_values(
            by=['idcl', 'nbprop', 'subprop'], ascending=False)
        precname = ""
        for (idx, clu) in sortf.iterrows():
            part = 0
            partalt = 0
            idp = clu.iloc[0]
            vals = []
            idcl = clu["idcl"]
            nidcl = clu['subprop']
            typeclu = clu["type"]
            if not(idcl == precname):
                if alt_cluster:
                    part = SSpropList[idc][nidcl]
                    partalt = np.sum((BBsupRecPropList[idc])[nidcl, :])
                if spec_cluster:
                    part = BBsupOrigProp[idc][nidcl]
                    partalt = BBsupOrigPart[idc][nidcl]
#                    part=np.sum((BBsupOrigProp[idc])[nidcl,:])
#                    partalt=np.sum((BBsupOrigPart[idc])[nidcl,:])
                vals.extend([clu["topid"], partalt, part, clu["nbprop"],
                             clu["nbwords"], clu["prop"], clu["score"]])
                if typeclu == "hl_clusters":
                    scorestot.loc[idcl] = [clu["nbprop"]*1000000000000000000]
                    vals.extend([clu["name"], "", "", ""])
                if typeclu == "ml_clusters":
                    scorestot.loc[idcl] = [
                        clu["nbprop"]*1000000000000 +
                        (scorestot.loc[clu["topid"]]
                         ["score"] - 1 * 1000000000000000000)
                    ]
                    vals.extend(["", clu["name"], "", ""])
                if typeclu == "ll_clusters":
                    scorestot.loc[idcl] = [
                        clu["nbprop"] * 1000000 + scorestot.loc[
                            clu["topid"]]["score"] - 1 * 1000000000000
                    ]
                    vals.extend(["", "", clu["name"], ""])
                if typeclu == "vll_clusters":
                    scorestot.loc[idcl] = [
                        clu["nbprop"]*1 +
                        scorestot.loc[clu["topid"]]["score"]-1*1000000
                    ]
                    vals.extend(["", "", "", clu["name"]])
                vals.extend(["", scorestot.loc[idcl]["score"]])
                if typeclu == "hl_clusters":
                    hltop.loc[idcl] = idcl
                    vals.extend([idcl, "-", "-"])
                if typeclu == "ml_clusters":
                    hltop.loc[idcl] = clu['topid']
                    mltop.loc[idcl] = idcl
                    vals.extend([clu['topid'], idcl, "-"])
                if typeclu == "ll_clusters":
                    hltop.loc[idcl] = hltop.loc[clu['topid']]['top']
                    mltop.loc[idcl] = clu['topid']
                    lltop.loc[idcl] = idcl
                    vals.extend([hltop.loc[idcl]['top'], clu['topid'], idcl])
                if typeclu == "vll_clusters":
                    mltop.loc[idcl] = mltop.loc[clu['topid']]['top']
                    hltop.loc[idcl] = hltop.loc[mltop.loc[idcl]['top']]['top']
                    lltop.loc[idcl] = clu['top']
                    vals.extend([hltop.loc[idcl]['top'],
                                 mltop.loc[idcl]['top'], clu['topid']])
                save.loc[idcl] = vals
                precname = idcl
#            if (idcl==precname):
            precname = idcl
        savesort = save.sort_values(
            by=["tophl", "topml", "topll", "nbprop"], ascending=True)
        filename = os.path.join(dump_dir, "synth"+"_"+clunat+"_"+"props.csv")
        savesort.to_csv(filename)

    idc = -1
    for clunat in natures:
        idc = idc+1
#    clunat=natures[0]
        if proplevel == clunat:
            filename = os.path.join(dump_dir, clunat+"_"+"props.csv")

            fich = pd.read_csv(filename)
            sortf = fich.sort_values(
                by=['idcl', 'nbprop', 'subprop'], ascending=False)
            precname = ""
            idpu = 0
            for (idx, clu) in sortf.iterrows():
                idpu = idpu+1
                part = 0
                partalt = 0
                idcl = clu["idcl"]
#                if alt_cluster:
#                if alt_cluster:
#                    part=SSpropList[idc][nidcl]
#                    partalt=np.sum((BBsupRecPropList[idc])[nidcl,:])
                idp = clu.iloc[0]
                vals = []
                idcl = clu["idcl"]
                typeclu = clu["type"]
                vals = []
                if test_cluster:
                    try:
                        prop_id = prop_scores.index.get_loc(clu["prop"])
#                        if not (prop_scores.index[prop_id]==
#                                    prop_scores_post.index[prop_id]):
#                            print("old :"+prop_scores.index[prop_id])
#                            print("new :"+prop_scores_post.index[prop_id])
#                            print("difname...")
                        coord = prop_umap[:, prop_id]
                        if len(coord.shape) > 1:
                            prop_id = prop_scores.index.get_loc(
                                clu["prop"]).argmax()
                            coord = prop_umap[:, prop_id]
                        nc = -1
                        for idn in natures:
                            nc = nc+1
                            if idn == typeclu:
                                nclu = clust_kms[nc].predict([coord.T])[0]
                                vid = idcl.split("_")[2]
                                if vid == str(nclu):
                                    print(vid+" == "+str(nclu))
                                else:
                                    print(vid+" DIF "+str(nclu))
                                    idp = typeclu+"_" + \
                                        str(nclu)+"_"+str(idpu)+"0"
                                    idcl = typeclu+"_"+str(nclu)
                    except ValueError:
                        print("NOT found "+clu["prop"])

                vals.extend([clu["topid"], part, partalt,
                             clu["subprop"], clu["subwords"],
                             "", clu["score"]])
                vals.extend(["", "", "", "", ])
                myid = (scorestot.loc[clu["idcl"]]
                        ["score"]-1000000.0)+clu["subprop"]
                vals.extend([clu["prop"], myid])
                vals.extend([hltop.loc[idcl]['top'], mltop.loc[idcl]
                             ['top'], lltop.loc[idcl]['top']])

                save.loc[idp] = vals
                precname = idcl
            savesort = save.sort_values(
                by=["tophl", "topml", "topll", "nbprop"], ascending=True)
            filename = os.path.join(
                dump_dir, "synth"+"_"+clunat+"_"+"props.csv")
            savesort.to_csv(filename)

    savesort = save.sort_values(
        by=["tophl", "topml", "topll", "nbprop"], ascending=True)
    filename = os.path.join(dump_dir, "synth"+"_"+"props.csv")
    savesort.to_csv(filename)


def replace_publication_score_cluster(dump_dir, cluster_name, nbprop=5,
                                      top_cluster_name=None, nbsample=5,
                                      sample=.8, startweight=0):
    """
    This function modifies an export.json file located in the dump_dir to
    replace all score of publication for every author in the file by the
    number of similar publiction for a given cluster level.

    The dump_dir must be the path to a directory that contains:
        - **export.json** the export file with the list of entities
        - **authors_mat.npz** the authors matrix in natural space used to
    produce the export.json file

    :param dump_dir:
    :return:
    """

# dump_dir="../dumps/debatO/"
# cluster_name="vll_clusters"
# cluster_name="vll_clusters"
# top_cluster_name="ll_clusters"
# nbprop=2
# if 2>1:

    logger.info('Building publication score.')
    words_mat = load_matrices_from_dumps(['words'], 'mat', dump_dir)[0]
    words_umap = load_matrices_from_dumps(['words'], 'umap', dump_dir)[0]
    prop_umap = load_matrices_from_dumps(['proposition'], 'umap', dump_dir)[0]
    prop_lsa = load_matrices_from_dumps(['proposition'], 'lsa', dump_dir)[0]
    words_lsa = load_matrices_from_dumps(['words'], 'lsa', dump_dir)[0]
    prop_scores = load_scores(['proposition'], dump_dir)[0]
    words_scores = load_scores(['words'], dump_dir)[0]
    clust_kms = load_objects([cluster_name], "km", dump_dir)
    clust_scores = load_scores([cluster_name], dump_dir)[0]
    if top_cluster_name is not None:
        clust_umap = load_matrices_from_dumps(
            [cluster_name], 'umap', dump_dir
        )[0]
        km_top = load_objects([top_cluster_name], "km", dump_dir)[0]
        top_cluster_prediction = km_top.predict(clust_umap.T)

    km = clust_kms[0]
    names_prediction = km.predict(words_umap.T)
    orig_cluster_prediction = km.predict(prop_umap.T)
    cluster_prediction = orig_cluster_prediction
    nb_clusters = km.n_clusters
    colsaves = ["type", "idcl", "top", "topid", "name", "nbprop", "nbwords",
                "prop", "score", "oldscore", "scalt", "scword", "scsim",
                "subwords", "subprop"]

    save = pd.DataFrame(columns=colsaves)
    best_props = []
    best_props_scores = []
    for cluster_idx in range(nb_clusters):
        taille_sub = []
        words_sub = []
        idcl = cluster_name+"_"+str(cluster_idx)

        print("--------")
        print(str(clust_scores.index[cluster_idx]), " ", str(cluster_idx),
              "/", str(nb_clusters), " :", str(clust_scores[cluster_idx]))
        cluster_prediction = orig_cluster_prediction.copy()
        sub_proplsa = prop_lsa[:, cluster_prediction == cluster_idx]

        sample_size = int(sub_proplsa.shape[1]*(1-sample))

        nbest_props_cand = []
        for clt in range(nbsample):
            print("iter "+str(clt)+" removing "+str(sample_size))
            if (sample_size > 0):
                sample_val = random.sample(
                    range(sub_proplsa.shape[1]), sample_size)
                cluster_prediction = orig_cluster_prediction.copy()
                subprediction = cluster_prediction[
                    cluster_prediction == cluster_idx
                ]
                subprediction[sample_val] = -1
                cluster_prediction[
                    cluster_prediction == cluster_idx] = subprediction
                sub_proplsa = prop_lsa[:, cluster_prediction == cluster_idx]

            sub_mat = words_mat[cluster_prediction == cluster_idx]
            sub_prop_len = np.maximum(sub_mat.getnnz(axis=1), 4)
            sub_sub_mat = sub_mat[:, names_prediction == cluster_idx]
            sub_wordslsa = words_lsa[:, names_prediction == cluster_idx]
            sub_sub_prop_len = np.maximum(sub_sub_mat.getnnz(axis=1), .1)
            prop_len_in = np.minimum(sub_sub_prop_len/sub_prop_len, 1)
            sub_sub_prop_len = np.maximum(sub_sub_mat.getnnz(axis=1), 4)
            sub_words_scores = words_scores[names_prediction == cluster_idx]

            km = KMeans(n_clusters=nbprop, init='k-means++',  verbose=0)
            print("clustering...")
            km.fit(sub_proplsa.T)
            print("predicting props on ..."+str(sub_proplsa.T.shape))
            subcluster_prediction = km.predict(sub_proplsa.T)
            print("predicting words on..."+str(sub_wordslsa.T.shape))
            subnames_prediction = km.predict(sub_wordslsa.T)
            sub_prop_scores = prop_scores[cluster_prediction == cluster_idx]
            print("names "+str(subnames_prediction.shape) +
                  " / "+str(subcluster_prediction.shape))

            for sub_cluster_idx in range(nbprop):

                sub_sub_sub_mat = sub_sub_mat[
                    subcluster_prediction == sub_cluster_idx, :][
                        :, subnames_prediction == sub_cluster_idx
                ]
                if (sub_sub_sub_mat.shape[0] > 0):

                    sub_sub_sub_mat = sub_sub_mat[
                        subcluster_prediction == sub_cluster_idx
                    ]
                    sub_sub_sub_sub_mat = sub_sub_sub_mat[
                        :, subnames_prediction == sub_cluster_idx
                    ]

                    sub_sub_words_scores = sub_words_scores[
                        subnames_prediction == sub_cluster_idx
                    ]

                    if (sub_sub_sub_sub_mat.shape[1] == 0):
                        sub_sub_sub_sub_mat = sub_sub_sub_mat
                        sub_sub_words_scores = sub_words_scores
                        print("no words!")

                    sub_bin_sub = binarize(sub_sub_sub_sub_mat.todense())
                    sub_sub_words_prop = np.sum(
                        sub_bin_sub, axis=0)/sub_sub_words_scores
                    sub_sub_sub_prop_len = np.maximum(
                        sub_sub_sub_mat.getnnz(axis=1), 4)
                    sub_bin_sub_len = np.divide(
                        sub_bin_sub.T, sub_sub_sub_prop_len).T
                    lchunk = 1000
                    if (sub_bin_sub_len.shape[0] * sub_bin_sub_len.shape[1]) > 1000000:  # noqa
                        ssimlist = []
                        ndeb = 0
                        nfin = lchunk
                        simt = sub_bin_sub.T
                        for chunk in range(
                                int(sub_bin_sub_len.shape[0]/lchunk) + 1
                        ):
                            print(str(chunk) + "/" +
                                  str(int(sub_bin_sub_len.shape[0]/lchunk)))
                            subsim = sub_bin_sub_len[ndeb:nfin, :]

                            simc = np.dot(subsim, simt)
                            simsc = np.where(simc > 0.5, simc, 0)
                            ssimc = simsc.sum(axis=1)
                            ssimlist.extend(ssimc)
                            ndeb = ndeb+lchunk
                            nfin = min(nfin+lchunk, sub_bin_sub_len.shape[0])
                        ssim = np.asarray(ssimlist)
                    else:
                        sim = np.dot(sub_bin_sub_len, sub_bin_sub.T)
                        sims = np.where(sim > 0.5, sim, 0)
                        ssim = sims.sum(axis=1)
                    sub_prop_len_in = prop_len_in[
                        subcluster_prediction == sub_cluster_idx
                    ]
                    sub_pond_sub_mat = np.multiply(
                        sub_bin_sub, sub_sub_words_prop.values
                    )
                    sub_pond_sub_mat_sum = np.sum(sub_pond_sub_mat, axis=0)
                    if (sub_sub_sub_mat.shape[1] > 0):

                        words_max = np.max(
                            sub_pond_sub_mat_sum*sub_bin_sub, axis=1)
                        words_max = np.divide(
                            np.sum(sub_pond_sub_mat_sum * sub_bin_sub, axis=1),
                            np.sum(sub_bin_sub, axis=1) + 1
                        )
                        ssiml = ssim/2 + 5 * sub_prop_len_in + words_max / 2

                        best = np.argsort(ssiml)[-1]
                        pscorew = words_max[best]

                    else:
                        ssiml = ssim + 5*sub_prop_len_in
                        best = np.argsort(ssiml)[-1]
                        pscorew = 0
                    sub_sub_prop_scores = sub_prop_scores[
                        subcluster_prediction == sub_cluster_idx
                    ]

                    pscore = ssiml[best]
                    ssiml = np.rint(ssiml)
                    oscore = sub_sub_prop_scores.values[best]
                    ascore = ssim[best] + 10*sub_prop_len_in[best]
                    pscores = ssim[best]
                    idprop = (
                        cluster_name + "_" + str(cluster_idx) + "_" +
                        str(sub_cluster_idx)
                    )
                    nbpropclu = sub_sub_mat.shape[0]
                    nbwordsclu = sub_sub_mat.shape[1]
                    nbpropsub = sub_bin_sub.shape[0]
                    nbwordssub = sub_bin_sub.shape[1]

                    sub_sub_prop_scores[:] = np.maximum(
                        ssiml, sub_sub_prop_scores.values)
                    sub_sub_prop_scores[best] = np.maximum(
                        np.rint(pscore), sub_sub_prop_scores.values[best]
                    )
                    sub_prop_scores[
                        subcluster_prediction == sub_cluster_idx
                    ] = sub_sub_prop_scores
                    prop_scores[cluster_prediction ==
                                cluster_idx] = sub_prop_scores
                    nbest_props_cand.append(sub_sub_prop_scores.index[best])
                    taille_sub.append(nbpropsub)
                    words_sub.append(nbwordssub)

                    if top_cluster_name is not None:
                        if (nbsample < 2):
                            save.loc[idprop] = [
                                cluster_name, idcl, top_cluster_name,
                                top_cluster_name + "_" +
                                str(top_cluster_prediction[cluster_idx]),
                                clust_scores.index[cluster_idx], nbpropclu,
                                nbwordsclu, sub_sub_prop_scores.index[best],
                                pscore, oscore, ascore, pscorew, pscores,
                                nbpropsub, nbwordssub
                            ]

        nbest_alt = np.unique(nbest_props_cand)[
            np.argsort(
                -1 * np.unique(nbest_props_cand, return_counts=True)[1]
            )[0:nbsample]
        ]
        nbest_score = [
            nbest_props_cand.count(idc) / nbsample for idc in nbest_alt
        ]

        best_props.append(nbest_alt)
        best_props_scores.append(nbest_score)
        print(str(nbest_alt))
        print(str(nbest_score))
        if top_cluster_name is not None:
            if (nbsample > 1):
                for idp in range(len(nbest_alt)):
                    idprop = cluster_name + "_" + \
                        str(cluster_idx) + "_" + str(idp)
                    save.loc[idprop] = [
                        cluster_name, idcl, top_cluster_name,
                        top_cluster_name + "_" +
                        str(top_cluster_prediction[cluster_idx]),
                        clust_scores.index[cluster_idx], nbpropclu, nbwordsclu,
                        nbest_alt[idp], nbest_score[idp], 0,
                        0, 0, 0, words_sub[idp], taille_sub[idp]
                    ]

        if top_cluster_name is not None:
            filename = os.path.join(
                dump_dir, cluster_name+"_"+"props_temp.csv"
            )
            save.to_csv(filename)

        dump_matrices([cluster_name], [np.asarray(best_props)],
                      "best_props_temp", dump_dir)
        dump_matrices([cluster_name], [np.asarray(best_props_scores)],
                      "best_props_scores_temp", dump_dir)

    if top_cluster_name is not None:
        filename = os.path.join(dump_dir, cluster_name + "_" + "props.csv")
        save.to_csv(filename)

    dump_matrices([cluster_name], [np.asarray(best_props)],
                  "best_props", dump_dir)
    dump_matrices([cluster_name], [np.asarray(best_props_scores)],
                  "best_props_scores", dump_dir)
    return prop_scores


def boost_prop(dump_dir, cluster_name, dump_result=True, fact=10.0):
    best_props = load_matrices_from_dumps(
        [cluster_name], 'best_props', dump_dir)[0]
    prop_scores = load_scores(['proposition'], dump_dir)[0]
    dump_scores(['proposition_preboost'], [prop_scores], dump_dir)
    boost_list = []
    prop_score_orig = prop_scores.copy()
    for props in best_props.flatten():
        for prop in props.flatten():
            if prop in prop_scores.index:
                prop_scores[prop] = prop_score_orig[prop] * fact + .1
                boost_list.append(prop_scores[prop])
            else:
                print("not in index:"+str(prop))
    print("boosted :" + str(boost_list))
    prop_scores = load_scores(['proposition'], dump_dir)[0]
    if dump_result:
        dump_scores(['proposition'], [prop_scores], dump_dir)
    return prop_scores


def desc_cluster(dump_dir, cluster_name, top_cluster_name=None,
                 name_stability=False, natures_cl=[]):
    """
    This function modifies an export.json file located in the dump_dir to
    replace all score of publication for every author in the file by the
    number of similar publiction for a given cluster level.

    The dump_dir must be the path to a directory that contains:
        - **export.json** the export file with the list of entities
        - **authors_mat.npz** the authors matrix in natural space used to
    produce the export.json file

    :param dump_dir:
    :return:
    """
#    natures_cl=['hl_clusters', 'ml_clusters', 'll_clusters', 'vll_clusters']
#    dump_dir="../dumps/debatO/"
#    cluster_name="vll_clusters"
#    cluster_name="vll_clusters"
#    top_cluster_name="ll_clusters"
#    nbprop=2

    logger.info('Building publication score.')
    clust_kms = load_objects([cluster_name], "km", dump_dir)
    clust_scores = load_scores([cluster_name], dump_dir)[0]
    if top_cluster_name is not None:
        top_clust_scores = load_scores([top_cluster_name], dump_dir)[0]
        clust_umap = load_matrices_from_dumps(
            [cluster_name], 'umap', dump_dir)[0]
        km_top = load_objects([top_cluster_name], "km", dump_dir)[0]
        top_cluster_prediction = km_top.predict(clust_umap.T)

    km = clust_kms[0]
    nb_clusters = km.n_clusters
    colsaves = ["type", "idcl", "top", "topid", "name", "nbprop", "nbwords",
                "prop", "score", "oldscore", "scalt", "scword", "scsim",
                "subwords", "subprop"]

    save = pd.DataFrame(columns=colsaves)

    words_mat = load_matrices_from_dumps(['words'], 'mat', dump_dir)[0]
    words_umap = load_matrices_from_dumps(['words'], 'umap', dump_dir)[0]
    prop_umap = load_matrices_from_dumps(['proposition'], 'umap', dump_dir)[0]
    names_prediction = km.predict(words_umap.T)
    cluster_prediction = km.predict(prop_umap.T)
    idc = -1

    if (name_stability):
        scorecomp = load_objects(["eval"], "score_comp", dump_dir)[0]
        firstnamecomp = load_objects(["eval"], "firstnamecomp", dump_dir)[0]
        secondnamecomp = load_objects(["eval"], "secondnamecomp", dump_dir)[0]
        all_names = load_objects(["eval"], "all_names", dump_dir)[0]
        orig_names = load_objects(["eval"], "orig_names", dump_dir)[0]

        idc = natures_cl.index(cluster_name)

        scoreavg = []
        firstnameavg = []
        secondnameavg = []
        comp_names = []
        best_alt = []
        best_score = []
        for idx, nature in enumerate(natures_cl):
            scoreavg.append(np.mean(np.asarray(scorecomp)[:, idx], axis=0))
            firstnameavg.append(
                np.mean(np.asarray(firstnamecomp)[:, idx], axis=0)
            )
            secondnameavg.append(
                np.mean(np.asarray(secondnamecomp)[:, idx], axis=0)
            )
            comp_names.append(
                np.stack(np.asarray(all_names)[:, idx])
            )
            nbest_alt = [np.unique(comp_names[idx][:, idc])[0]
                         for idc in range(len(orig_names[idx]))]
            nbest_score = [
                np.count_nonzero(
                    comp_names[idx][:, idc] == nbest_alt[idc]
                ) / len(comp_names[idx][0, :])
                for idc in range(len(orig_names[idx]))
            ]
            best_alt.append(nbest_alt)
            best_score.append(nbest_score)

    for cluster_idx in range(nb_clusters):
        idcl = cluster_name+"_"+str(cluster_idx)
        if top_cluster_name is not None:
            submatp = prop_umap[:, cluster_prediction == cluster_idx]
            top_cluster_props = km_top.predict(submatp.T)
            v, nbc = np.unique(top_cluster_props, return_counts=True)
            best_c = v[np.argmax(nbc)]
            if (best_c != top_cluster_prediction[cluster_idx]):
                print(clust_scores.index[cluster_idx] + " dif: ")
                print(" ==> " + top_clust_scores.index[best_c] + " vs ")
                print("  <== " + top_clust_scores.index[
                    top_cluster_prediction[cluster_idx]
                ])
                print("   ")
#                top_cluster_prediction[cluster_idx]=best_c
#        print("--------")
        print(str(clust_scores.index[cluster_idx]),
              ":", str(clust_scores[cluster_idx]))
        sub_mat = words_mat[cluster_prediction == cluster_idx]

        sub_sub_mat = sub_mat[:, names_prediction == cluster_idx]
        nbpropclu = sub_sub_mat.shape[0]
        nbwordsclu = sub_sub_mat.shape[1]
        name_stab = -1
        clu_stab = -1
        if name_stability:
            name_stab = np.max(
                (best_score[idc][cluster_idx], firstnameavg[idc][cluster_idx]))
            clu_stab = scoreavg[idc][cluster_idx]

        save.loc[idcl] = [cluster_name, idcl, top_cluster_name,
                          top_cluster_name + "_" +
                          str(top_cluster_prediction[cluster_idx]),
                          clust_scores.index[cluster_idx], nbpropclu,
                          nbwordsclu, clu_stab, name_stab, "", "", "", "", "",
                          cluster_idx]
    if top_cluster_name is not None:
        filename = os.path.join(dump_dir, cluster_name+"_"+"simple.csv")
        save.to_csv(filename)


def debat_clusters(natures, articles_umap, words_umap, words_tab, words_scores,
                   words_lsa, dump_dir, base_factor=3):
    cluster_labels = []
    clus_lsa = []
    clus_umap = []
    clus_scores = []
    clus_km = []

    for idx, nature in enumerate(natures):
        n_clusters = 8 * (base_factor ** idx)
        logger.info(
            f"Starting clustering for {nature}. Nb clusters = {n_clusters}."
        )

        print(str(nature))
        c_lsa, c_umap, c_scores, c_km = create_kmeans_clusters(
            n_clusters, articles_umap, words_umap, words_tab,
            words_scores, cluster_labels, words_lsa, weight_name_length=0.2
        )
        cluster_labels = []
        clus_lsa.append(c_lsa)
        clus_umap.append(c_umap)
        clus_scores.append(c_scores)
        clus_km.append(c_km)

    dump_scores(natures, clus_scores, dump_dir)
    dump_matrices(natures, clus_lsa, 'lsa', dump_dir)
    dump_matrices(natures, clus_umap, 'umap', dump_dir)
    dump_objects(natures, clus_km, "km", dump_dir)

    return clus_lsa, clus_umap, clus_scores


def do_guided_projection(natures, matrices, words_tab, num_dims, dump_dir,
                         n_neighbors=25, min_dist=0.15, max_size=1500000,
                         keep_size=60000, load_lsa=False):
    logger.info('Starting LSA projection')
    if (load_lsa):
        normalized_matrices = load_matrices_from_dumps(
            natures, 'lsa', dump_dir)
    else:
        projected_matrices = lsa_projection(num_dims, words_tab, matrices)
        normalized_matrices = list(map(normalize_l2, projected_matrices))
        dump_matrices(natures, normalized_matrices, 'lsa', dump_dir)

    matrix = normalized_matrices[0]
    logger.info('Starting UMAP projection')
    perm = np.random.permutation(matrix.shape[1]-keep_size)
    fit_data = matrix[:, perm[:max_size]]
    fit_data = np.concatenate((fit_data, matrix[:, -keep_size:]), axis=1)
    umap_matrices = guided_umap_projection(
        normalized_matrices[0][:, -max_size:], normalized_matrices,
        n_neighbors, min_dist, max_size
    )

    dump_matrices(natures, umap_matrices, 'umap', dump_dir)

    return normalized_matrices, umap_matrices


def rename_props(prop_scores, ques_scores, ques_mat):
    logger.info('Adding questions to propositions.')

    noques = np.argmax(ques_mat, axis=1)
    ques_names = ques_scores.index.values[noques].flatten()
    prop_names = prop_scores.index.values
    prop_names = prop_names+"     ("+ques_names+")"
    prop_scores.index = prop_names
    return prop_scores


def clear_names(prop_scores, ques_scores, ques_mat):
    logger.info('Remove RIL prop names.')
    noques = np.asarray(np.argmax(ques_mat, axis=1)).flatten()
    noqril = np.max(noques)
    prop_names = prop_scores.index.values
    prop_names[noques == noqril] = "RIL proposition"
    prop_scores.index = prop_names

    return prop_scores


def add_metadata(export_file, dump_dir):
    logger.info('MetaData: Loading points.')
    import_file = os.path.join(dump_dir, export_file)

    with open(import_file, 'r') as file:
        points = json.load(file)

    nature_target = ["ville", "question", "words"]

    scores = load_scores(nature_target, dump_dir)
    matrices = load_matrices_from_dumps(nature_target, 'mat', dump_dir)

    mat_ville = matrices[0]
    sc_ville = scores[0]
    mat_ques = matrices[1]
    sc_ques = scores[1]
    mat_words = matrices[2]
    sc_words = scores[2]
    mw = mat_words.shape[0]
    subsc = sc_words[sc_words > mw/100000][sc_words < mw/2]
    crit = (sc_words > mw/100000).values & (sc_words < mw/2).values
    submat = mat_words[:, crit]
    collists = [np.nonzero(t)[1] for t in submat]
    count = 0
    ques = 0
    wordscount = 0
    wordstotal = 0
    logger.info('MetaData: Starting updates')
    for point in points:
        if point['nature'] == 'proposition':
            ques = np.argmax(mat_ques[count])
            ville = np.argmax(mat_ville[count])
            word = collists[count]

            point['ville'] = str(sc_ville.index[ville])
            point['question'] = str(sc_ques.index[ques])
            ques += 1
            if word.shape[0] > 0:
                point['words'] = list(set(subsc.index[word]))
                wordscount += 1
                wordstotal += word.shape[0]
            count += 1
            if count % 10000 == 0:
                logger.info(
                    f"MetaData: {count} publications updated {wordscount} "
                    f"with {wordstotal} words."
                )

    logger.info(
        f"MetaData finished: {count} publications updated {wordscount} with "
        f"{wordstotal} words."
    )

    logger.info('MetaData: Saving updated points.')
    export_to = os.path.join(dump_dir, export_file +
                             "_complet"+str(wordstotal)+".json")
    with open(export_to, 'w') as file:
        json.dump(points, file)


def reload_data_and_project(df, natures, num_dims, dump_dir, guided=False,
                            load_lsa=False, load_all=False,
                            clust_natures=['hl_clusters', 'ml_clusters',
                                           'll_clusters', 'vll_clusters']):

    logger.info('Reloading')

    matrices = load_matrices_from_dumps(natures, 'mat', dump_dir)

    if load_all:
        natures.extend(clust_natures)
        scores = load_scores(natures, dump_dir)
        umap_matrices = load_matrices_from_dumps(natures, 'umap', dump_dir)
        normalized_matrices = load_matrices_from_dumps(
            natures, 'lsa', dump_dir)
    else:
        scores = load_scores(natures, dump_dir)
        if guided:
            normalized_matrices, umap_matrices = do_guided_projection(
                natures, matrices, matrices[3], num_dims, dump_dir,
                n_neighbors=15, min_dist=0.1, max_size=350000, keep_size=60000,
                load_lsa=load_lsa
            )
        else:
            normalized_matrices, umap_matrices = do_projection(
                natures, matrices, matrices[3], num_dims, dump_dir,
                n_neighbors=15, min_dist=0.1, max_size=500000,
                load_lsa=load_lsa
            )
    return matrices, normalized_matrices, umap_matrices, scores


def load_data_and_project(df, natures, min_df, max_df, max_words, vocab_sample,
                          num_dims, dump_dir, guided=False):
    logger.info('Starting data loading')

    prop_mat, prop_scores = load_identity_column(df, 'text')
    ville_mat, ville_scores = load_comma_separated_column(df, 'ville')
    question_mat, question_scores = load_comma_separated_column(df, 'question')
    logger.info('Starting words loading')
    with open('datas/stopwordsfr.txt', 'r') as stop_file:
        stopwords = set(stop_file.read().splitlines())
    stopwords = []
    words_mat, words_scores = load_text_column(
        df['text'], 6, min_df, max_df, vocab_sample, max_words,
        stopwords=stopwords, min_word_length=1
    )
    words_scores.head()

    words_mat = normalize_tfidf(words_mat)

    matrices = [prop_mat, ville_mat, question_mat, words_mat]
    scores = [prop_scores, ville_scores, question_scores, words_scores]

    dump_scores(natures, scores, dump_dir)
    dump_matrices(natures, matrices, 'mat', dump_dir)

    if guided:
        normalized_matrices, umap_matrices = do_guided_projection(
            natures, matrices, words_mat, num_dims, dump_dir, n_neighbors=15,
            min_dist=0.1, max_size=350000, keep_size=60000
        )
    else:
        normalized_matrices, umap_matrices = do_projection(
            natures, matrices, words_mat, num_dims, dump_dir, n_neighbors=15,
            min_dist=0.1, max_size=500000
        )
    return matrices, normalized_matrices, umap_matrices, scores


def do_workflow(df, min_df, max_df, max_words, vocab_sample, num_dims,
                dump_dir, guided=False, load_lsa=False):
    natures = ['proposition', 'ville', 'question', 'words',
               'hl_clusters', 'ml_clusters', 'll_clusters', 'vll_clusters']
    if load_lsa:
        matrices, lsa_matrices, umap_matrices, scores = reload_data_and_project(  # noqa
            df, natures[:4], num_dims, dump_dir, guided, load_lsa
        )
    else:
        matrices, lsa_matrices, umap_matrices, scores = load_data_and_project(
            df, natures[:4], min_df, max_df, max_words,
            vocab_sample, num_dims, dump_dir, guided=guided
        )
    dump_scores(['proposition_pre_rename'], [scores[0]], dump_dir)
    scores[0] = rename_props(scores[0], scores[2], matrices[2])

    dump_scores(['proposition'], [scores[0]], dump_dir)
    clus_lsa, clus_pos, clus_scores = debat_clusters(
        natures[4:], umap_matrices[0], umap_matrices[3], matrices[3],
        scores[3], lsa_matrices[3], dump_dir
    )

    umap_matrices.extend(clus_pos)
    scores.extend(clus_scores)
    lsa_matrices.extend(clus_lsa)

    scores[4:] = eval_stability(dump_dir,
                                natures=natures,
                                subnatures=natures[:4],
                                natures_cl=natures[4:],
                                nb_clutest=10, umap_test_nb=0,
                                replace_names=True)

    desc_cluster(dump_dir, "vll_clusters", top_cluster_name="ll_clusters",
                 name_stability=True, natures_cl=natures[4:])
    desc_cluster(dump_dir, "ll_clusters", top_cluster_name="ml_clusters",
                 name_stability=True, natures_cl=natures[4:])
    desc_cluster(dump_dir, "ml_clusters", top_cluster_name="hl_clusters",
                 name_stability=True, natures_cl=natures[4:])
    desc_cluster(dump_dir, "hl_clusters", top_cluster_name="hl_clusters",
                 name_stability=True, natures_cl=natures[4:])

    # indirects = [False, True, True, True, False]
    # get_all_neighbors_indirect(lsa_matrices, scores, matrices, indirects,
    # dump_dir, natures[:5])

    export_file = os.path.join(dump_dir, 'export_prepropscore.json')

    export_to_json(natures, umap_matrices, scores, export_file, None, dump_dir)

    dump_scores(['proposition_pre_score'], [scores[0]], dump_dir)
    scores[0] = replace_publication_score_cluster(
        dump_dir, "ll_clusters", 10, "ml_clusters")
    dump_scores(['proposition_pre_boost'], [scores[0]], dump_dir)
    dump_scores(['proposition'], [scores[0]], dump_dir)

    export_file = os.path.join(dump_dir, 'export_prevoisins.json')
    export_to_json(natures, umap_matrices, scores, export_file, None, dump_dir)

    power_scores = [0.5, 0, 0, 0.5, 0, 0, 0, 0]
    get_all_neighbors(lsa_matrices[:8], scores,
                      power_scores, dump_dir, natures[:8])

    theme_synthesis(dump_dir, natures[4:], proplevel="ll_clusters")
    scores[0] = boost_prop(dump_dir, "ll_clusters",
                           dump_result=True, fact=10.0)
    dump_scores(['proposition'], [scores[0]], dump_dir)
    export_file = os.path.join(dump_dir, 'export.json')
    export_to_json(natures, umap_matrices, scores,
                   export_file, natures[:8], dump_dir)
    add_metadata('export.json', dump_dir)
