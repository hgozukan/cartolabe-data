import codecs
import glob
import json
import logging
import os

import pandas as pd
from sklearn.feature_extraction import text as sktxt

from cartodata.exporting import Exporter
from cartodata.loading import (
    load_identity_column, load_comma_separated_column, load_text_column
)
from cartodata.operations import (
    normalize_tfidf, filter_min_score, load_matrices_from_dumps, load_scores
)
from cartodata.workflows.common import (
    create_clusters, get_all_neighbors, dump_scores, dump_matrices,
    do_projection, all_matrices_exist
)

logger = logging.getLogger(__name__)


def bib_workflow(dump_dir):
    df = build_df('datas/bibliolabs')
    # do_workflow(df, 25, 0.1, 50000, None, 300, dump_dir)
    load_workflow(dump_dir, df)


def load_workflow(dump_dir, df):
    natures = ['articles', 'authors', 'words', 'teams', 'labs', 'hl_clusters',
               'ml_clusters', 'll_clusters', 'vll_clusters']

    export(dump_dir, df, natures, natures[:5])


def do_workflow(df, min_df, max_df, max_words, vocab_sample, num_dims,
                dump_dir, filt_min_score=4, n_neighbors=10):

    natures = ['articles', 'authors', 'words', 'teams', 'labs', 'hl_clusters',
               'ml_clusters', 'll_clusters', 'vll_clusters']

    load_matrices(natures[:5], df, min_df, max_df, max_words, vocab_sample,
                  dump_dir, filt_min_score, force=False)

    matrices = load_matrices_from_dumps(natures[:5], 'mat', dump_dir)
    do_projection(natures[:5], matrices, matrices[2], num_dims, dump_dir,
                  force=False)

    umap_matrices = load_matrices_from_dumps(natures[:5], 'umap', dump_dir)
    lsa_matrices = load_matrices_from_dumps(natures[:5], 'lsa', dump_dir)
    scores = load_scores(natures[:5], dump_dir)

    clus_lsa, clus_pos, clus_scores = create_clusters(
        natures[5:], umap_matrices[0], umap_matrices[2], matrices[2],
        scores[2], lsa_matrices[2], dump_dir, base_factor=3
    )
    umap_matrices.extend(clus_pos)
    scores.extend(clus_scores)

    power_scores = [0, 0.5, 0.5, 0, 0]
    get_all_neighbors(
        lsa_matrices, scores, power_scores, dump_dir, natures[:5], n_neighbors
    )

    export(dump_dir, df, natures, natures[:5])


def load_matrices(natures, df, min_df, max_df, max_words, vocab_sample,
                  dump_dir, filt_min_score, force=False):

    if all_matrices_exist(natures, 'mat', dump_dir) and not force:
        logger.info(
            f"Matrices for {', '.join(natures)} already exist. Skipping "
            "creation."
        )
        return

    logger.info('Starting data loading')
    # Create term-document matrices for the entites we need
    articles_tab, articles_scores = load_identity_column(df, 'Title')
    cited_count = df['CitedByCount']
    cited_count.index = articles_scores.index
    articles_scores = articles_scores + cited_count
    authors_tab, authors_scores = load_comma_separated_column(
        df, 'Authors', comma=';'
    )

    with open('datas/inria-teams.csv') as teams_file:
        inria_teams = teams_file.read().split(',')

    teams_tab, teams_scores = load_comma_separated_column(
        df, 'Labs', whitelist=inria_teams, comma=';'
    )
    labs_tab, labs_scores = load_comma_separated_column(
        df, 'Labs', blacklist=inria_teams, comma=';'
    )

    logger.info('Starting words loading')
    with open('datas/stopwords.txt', 'r') as stop_file:
        stopwords = sktxt.ENGLISH_STOP_WORDS.union(
            set(stop_file.read().splitlines())
        )

    df['text'] = df['Abstract'] + '.' + df['Title'] + '.' + \
        df['Keywords'].astype(str) + '.' + df['Classifications'].astype(str)
    words_tab, words_scores = load_text_column(
        df['text'], 4, min_df, max_df, vocab_sample, max_words,
        stopwords=stopwords
    )

    # Normalise words_tab
    words_tab = normalize_tfidf(words_tab)

    matrices = [articles_tab, authors_tab, words_tab, teams_tab, labs_tab]
    scores = [articles_scores, authors_scores, words_scores, teams_scores,
              labs_scores]

    # filter labs and authors with min score = 4
    filt_authors, filt_auth_scores = filter_min_score(
        matrices[1], scores[1], filt_min_score
    )
    matrices[1] = filt_authors
    scores[1] = filt_auth_scores

    filt_labs, filt_lab_scores = filter_min_score(
        matrices[4], scores[4], filt_min_score
    )
    matrices[4] = filt_labs
    scores[4] = filt_lab_scores

    dump_scores(natures, scores, dump_dir)
    dump_matrices(natures, matrices, 'mat', dump_dir)


def export(dump_dir, df, natures, neighbor_natures):

    df['url'] = df['HalId'].fillna(0).astype(int)
    df['url'] = 'hal-' + df['url'].astype(str).str.pad(width=8, side='left',
                                                       fillchar='0')
    df['url'] = df['url'].replace('hal-00000000', '')
    df['year'] = df['CoverDate'].str[:4]

    exporter = Exporter(dump_dir, natures, neighbor_natures)
    exporter.add_reference('articles', 'labs')
    exporter.add_reference('articles', 'teams')
    exporter.add_reference('authors', 'labs')
    exporter.add_reference('authors', 'teams')
    exporter.merge_metadata_values('articles', ['teams', 'labs'], 'labs')
    exporter.merge_metadata_values('authors', ['teams', 'labs'], 'labs')
    exporter.add_metadata_values('articles', df[['year', 'url']])
    exporter.add_metadata_values('authors',
                                 build_authors_years_list(dump_dir, df))
    exporter.export_to_json()


def build_authors_years_list(dump_dir, df):
    matrix = load_matrices_from_dumps(['authors'], 'mat', dump_dir)[0]
    rows, cols = matrix.T.nonzero()
    col = 0
    row = 0
    years = []
    for idx in range(len(rows)):
        if rows[idx] == row:
            continue
        years.append(','.join(y for y in set(
            df['year'].iloc[cols[col:idx]].values))
        )
        row = rows[idx]
        col = idx
    years.append(','.join(y for y in set(
        df['year'].iloc[cols[col:-1]].values))
    )
    return pd.DataFrame(years, columns=['year'])


def build_df(data_dir):
    if os.path.isfile(os.path.join(data_dir, 'bibliolabs.csv')):
        df = pd.read_csv(
            os.path.join(data_dir, 'bibliolabs.csv'), index_col=None
        )
        return df

    articles = []
    labs = {}
    authors = {}
    for file in glob.glob(os.path.join(data_dir, '*.json')):
        with codecs.open(file, 'r', 'utf-8-sig') as f:
            data = json.load(f)

            for article in data:
                if is_empty(article['Abstract']) or is_empty(article['Title']):
                    continue

                kw = map(lambda x: x['Label'], article['Keywords'])
                classifications = map(
                    lambda x: x['Label'], article['Classifications']
                )
                article_authors, article_labs = extract_authors(
                    authors, labs, article['Authors']
                )

                article.pop('Keywords')
                article.pop('Classifications')
                article.pop('Authors')
                if kw:
                    article['Keywords'] = ';'.join(kw)
                if classifications:
                    article['Classifications'] = ';'.join(classifications)
                article['Authors'] = article_authors
                article['Labs'] = article_labs
                articles.append(article)

    df = pd.DataFrame(articles)
    with open(os.path.join(data_dir, 'bibliolabs.csv'), 'w') as f:
        df.to_csv(f, index=None)

    return df


def extract_authors(authors_idx, labs_idx, article_authors):
    authors_list = []
    author_names = []
    labs_list = []
    lab_names = []

    for author in article_authors:
        if (not is_empty(author['LastName']) and not
                is_empty(author['FirstName'])):
            author_labs = extract_labs(author['Affiliations'])
            for lab in author_labs:
                if lab['Label'] not in lab_names:
                    lab_names.append(lab['Label'])
                    labs_list.append(lab)
                if 'lab' not in author and lab['Label'] != (
                        'CNRS Centre National de la Recherche Scientifique'
                ):
                    author['lab'] = lab
            authors_list.append(author)

    for lab in labs_list:
        lv = labs_idx.setdefault(lab['Label'], lab)
        lv.setdefault('count', 0)
        lv['count'] += 1

    for author in authors_list:
        author_name = '{} {}'.format(author['FirstName'], author['LastName'])
        author_names.append(author_name)
        av = authors_idx.setdefault(author_name, author)
        av.setdefault('count', 0)
        av['count'] += 1

    return ';'.join(author_names), ';'.join(lab_names)


def extract_labs(labs):
    labs_lst = []
    for lab in labs:
        if not is_empty(lab['Label']):
            labs_lst.append(lab)
    return labs_lst


def is_empty(value):
    return value is None or value == 'N/A'
