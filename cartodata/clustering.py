import logging

from bisect import insort_left, bisect_left
from timeit import default_timer as timer

import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.preprocessing import binarize
from joblib import Memory
try:
    import hdbscan
except ImportError:
    pass

from cartodata.logs import timeit
logger = logging.getLogger(__name__)


@timeit
def create_kmeans_clusters(nb_clusters, clustering_table, naming_table,
                           natural_space_naming_table, naming_scores,
                           previous_cluster_labels, naming_profile_table=None,
                           label_length=2, max_iter=100, n_init=1,
                           weight_name_length=0.5, random_state=None,
                           verbose=0):
    """
    Creates a correspondance matrix for ``nb_clusters`` clusters. Clusters are
    created using the KMeans algorithm.

    Parameters
    ----------
    nb_clusters: int
        The number of clusters to create.
    clustering_table: numpy.ndarray
        The matrix to fit to create the clusters.
    naming_table: numpy.ndarray
        The matrix used to name clusters. This matrix should have the same
    number of rows as ``clustering_table``.
    natural_space_naming_table: scipy.sparse._csr.csr_matrix
        The naming_table in it's natural space. It should have the same number
    of columns as ``naming_table``.
    naming_scores: pandas.core.series.Series
        The scores for the ``naming_table`` columns.
    previous_cluster_labels: list
        A list of previous cluster labels, used to prevent duplicates in
    naming.
    naming_profile_table: numpy.ndarray
        An optional naming_table in projected space to build the cluster's
    profile with. If it is not provided, the ``naming_table`` will be used
    instead.
    label_length: int, default=2
        Length of cluster labels
    max_iter: int, default=100
        Maximum number of iterations of the k-means algorithm for a single run.
    n_init: int, default=1
        Number of times the k-means algorithm is run with different centroid
    seeds.
    weight_name_length: float, default=0.5
    random_state: int, default=None
        Determines random number generation for centroid initialization. Use an
    int to make the randomness deterministic.
    verbose: int, default=None

    Returns
    -------
    cluster_profiles: ndarray
    cluster_centers: ndarray, shape (n, )
    cluster_scores: pandas.core.series.Series [float]
    labels: numpy.ndarray
    score_words: numpy.ndarray
    cluster_eval_pos: pandas.core.series.Series [float]
    cluster_eval_neg: pandas.core.series.Series [float]
    """

    km = KMeans(n_clusters=nb_clusters, init='k-means++', max_iter=max_iter,
                n_init=n_init, random_state=random_state, verbose=verbose)

    # computes cluster centers and predicts cluster index for each entity
    cluster_prediction = km.fit_predict(clustering_table.T)

    # predicts cluster index for words
    names_prediction = km.predict(naming_table.T)

    (cluster_profiles, cluster_scores,
     score_words, cluster_eval_pos, cluster_eval_neg) = find_labels(
         nb_clusters, label_length, cluster_prediction, names_prediction,
         naming_profile_table, naming_table,
        natural_space_naming_table, naming_scores,
        previous_cluster_labels, weight_name_length
    )

    return (
        np.vstack(cluster_profiles).T, km.cluster_centers_.T, cluster_scores,
        km.labels_, score_words, cluster_eval_pos, cluster_eval_neg
    )


def create_hdbscan_clusters(nb_clusters, clustering_table, naming_table,
                            natural_space_naming_table, naming_scores,
                            previous_cluster_labels, naming_profile_table=None,
                            label_length=2, weight_name_length=0.5,
                            min_cluster_size=15, min_samples=None,
                            cache=None, use_memory=True,
                            prop_labeled_threshold=0.75):
    """
    Create clusters of articles and their labels using the hdbscan algorithm.

    Parameters
    ----------
    nb_clusters: int
        The number of clusters to create.
    clustering_table: numpy.ndarray
        The matrix to fit to create the clusters.
    naming_table: numpy.ndarray
        The matrix used to name clusters. This matrix should have the same
    number of rows as ``clustering_table``.
    natural_space_naming_table: scipy.sparse._csr.csr_matrix
        The naming_table in it's natural space. It should have the same number
    of columns as ``naming_table``.
    naming_scores: pandas.core.series.Series
        The scores for the ``naming_table`` columns.
    previous_cluster_labels: list
        A list of previous cluster labels, used to prevent duplicates in
    naming.
    naming_profile_table: numpy.ndarray
        An optional naming_table in projected space to build the cluster's
    profile with. If it is not provided, the ``naming_table`` will be used
    instead.
    label_length: int, default=2
        Length of cluster labels
    weight_name_length: float, default=0.5
    min_cluster_size: int, default=15
        The minimum size of clusters used by hdbscan.
    min_samples: int, default=None
        The minimum samples size used by hdbscan.
    cache:
        A cache of already computed hdbscan results.
    use_memory: booli default=True
    prop_labeled_threshold: float, default=0.75

    Returns
    -------
    cluster_profiles: ndarray
    cluster_centers: ndarray, shape (n, )
    cluster_scores: pandas.core.series.Series [float]
    labels: numpy.ndarray
    score_words: numpy.ndarray
    cluster_eval_pos: pandas.core.series.Series [float]
    cluster_eval_neg: pandas.core.series.Series [float]
    """
    memory = str(min_samples) if use_memory else Memory(location=None)

    if nb_clusters < 1:
        hdb = hdbscan.HDBSCAN(
            min_cluster_size=min_cluster_size,
            min_samples=min_samples,
            memory=memory
        )
        hdb.fit(clustering_table.T)
        clusters = hdb.labels_.max() + 1
    else:
        clusters, hdb = best_hdbscan_parameters(
            clustering_table.T,
            nb_clusters,
            min_cluster_size=min_cluster_size,
            min_samples=min_samples,
            cache=cache)
    cluster_prediction = hdb.labels_
    cluster_centers = np.vstack(
        # Compute the centroid of the cluster examplars
        [np.mean(e, axis=0).T for e in hdb.exemplars_]
    ).T

    names_prediction, strengths = hdbscan.approximate_predict(hdb,
                                                              naming_table.T)

    (cluster_profiles, cluster_scores,
     score_words, cluster_eval_pos, cluster_eval_neg) = find_labels(
         clusters, label_length, cluster_prediction, names_prediction,
         naming_profile_table, naming_table,
        natural_space_naming_table, naming_scores,
        previous_cluster_labels, weight_name_length
    )

    return (
        np.vstack(cluster_profiles).T, cluster_centers, cluster_scores,
        hdb.labels_, score_words, cluster_eval_pos, cluster_eval_neg
    )


def find_labels(nb_clusters, label_length, cluster_prediction,
                names_prediction, naming_profile_table, naming_table,
                natural_space_naming_table, naming_scores,
                previous_cluster_labels, weight_name_length):
    """
    Parameters
    ----------
    nb_clusters: int
        The number of clusters to create.
    label_length: int
        Length of cluster labels
    cluster_prediction: numpy.ndarray
    names_prediction: numpy.ndarray
    naming_profile_table: numpy.ndarray
        An optional naming_table in projected space to build the cluster's
    profile with. If it is not provided, the ``naming_table`` will be used
    instead.
    naming_table: numpy.ndarray
        The matrix used to name clusters. This matrix should have the same
    number of rows as ``clustering_table``.
    natural_space_naming_table: scipy.sparse._csr.csr_matrix
        The naming_table in it's natural space. It should have the same number
    of columns as ``naming_table``.
    naming_scores: pandas.core.series.Series
        The scores for the ``naming_table`` columns.
    previous_cluster_labels: list
        A list of previous cluster labels, used to prevent duplicates in
    naming.
    weight_name_length: float, default=0.5

    Returns
    -------
    cluster_profiles: ndarray
    cluster_scores: pandas.core.series.Series [float]
    score_words: numpy.ndarray
    cluster_eval_pos: pandas.core.series.Series [float]
    cluster_eval_neg: pandas.core.series.Series [float]
    """

    if nb_clusters > naming_table.shape[1]:
        logger.warn(
            f"{nb_clusters} clusters is greater than the total number words: "
            f"{naming_table.shape[1]} words."
            "Will add labels from outside of the cluster when necessary."
        )

    # binarizes entity x words matrix
    binary_naming_table = binarize(natural_space_naming_table)

    naming_profile = naming_table

    # names_prediction contains a list of cluster numbers and -1 for
    # not assigned. It is not filtered since the loop below only checks
    # the valid cluster_idx so -1 will be ignored
    if naming_profile_table is not None:
        # if ndimensional feature matrix exists, use it
        naming_profile = naming_profile_table

    cluster_scores = pd.Series(dtype=np.float64)
    cluster_eval_pos = pd.Series(dtype=np.float64)
    cluster_eval_neg = pd.Series(dtype=np.float64)
    cluster_profiles = []
    score_words = None

    for cluster_idx in range(nb_clusters):

        (cluster_label, max_nb_articles_in_cluster,
         cluster_labels_indices, previous_cluster_labels,
         score_words, score_pos, score_neg) = find_labels_idx(
            cluster_idx, label_length, cluster_prediction,
             names_prediction, binary_naming_table, naming_scores,
             previous_cluster_labels, score_words, weight_name_length
        )

        if cluster_label in cluster_scores.index.to_numpy(dtype=str):
            print("NAME DUPLICATE: had to reuse cluster label " +
                  cluster_label + " trick: add some space to it...")
            while cluster_label in cluster_scores.index.to_numpy(dtype=str):
                cluster_label = cluster_label + " "

        cluster_scores[cluster_label] = max_nb_articles_in_cluster
        cluster_eval_pos[cluster_label] = score_pos
        cluster_eval_neg[cluster_label] = score_neg
        cluster_profile = (
            np.sum(naming_profile[:, cluster_labels_indices], axis=1) /
            len(cluster_labels_indices)
        )
        cluster_profiles.append(cluster_profile)

    return (cluster_profiles, cluster_scores, score_words,
            cluster_eval_pos, cluster_eval_neg)


def find_labels_idx(cluster_idx, label_length, cluster_prediction,
                    names_prediction, binary_naming_table, naming_scores,
                    previous_cluster_labels, score_words, weight_name_length):
    """
    Generates label for the cluster specified by ``cluster_idx``.

    Parameters
    ----------
    cluster_idx: int
        Cluster id.
    label_length: int
        Length of cluster labels in the cluster specified by ``cluster_idx``.
    cluster_prediction: numpy.ndarray
    names_prediction: numpy.ndarray
    binary_naming_table:
    naming_scores: pandas.core.series.Series
        The scores for the ``naming_table`` columns.
    previous_cluster_labels: list
        A list of previous cluster labels, used to prevent duplicates in
    naming.
    score_words: numpy.ndarray
    weight_name_length: float, default=0.5

    Returns
    -------
    cluster_label: str
        Generated cluster label.
    max_nb_articles_in_cluster: int
        Maximum number of articles in the cluster with id ``cluster_idx``.
    cluster_labels_indices: list
    previous_cluster_labels: list
    score_words: numpy.ndarray
    score_pos: float
    score_neg: float
    """
    cluster_labels = []
    cluster_labels_indices = []

    score_pos = 0.0
    score_neg = 0.0

    # Vector of articles in cluster
    articles_in_cluster = np.where(cluster_prediction == cluster_idx, 1, 0)
    max_nb_articles_in_cluster = np.sum(articles_in_cluster)
    names_in_cluster = np.where(names_prediction == cluster_idx, 1, 0)

    if np.sum(names_in_cluster) < label_length:
        logger.warning(
            f"Cluster {cluster_idx} does not have enough number of labels!\n"
            f"Number of labels in cluster {np.sum(names_in_cluster)}."
        )

    for i in range(label_length):
        nb_of_articles_in_cluster_per_word = (
            articles_in_cluster * binary_naming_table
        )
        if i == 0:
            max_nb_articles_in_cluster = np.sum(articles_in_cluster)

        nb_of_articles_outside_cluster_per_word = (
            (1 - articles_in_cluster) * binary_naming_table
        )
        max_nb_of_articles_outside_cluster = np.sum(
            1 - articles_in_cluster
        )

        score_for_all_words = (
            (nb_of_articles_in_cluster_per_word /
             max_nb_articles_in_cluster) -
            (nb_of_articles_outside_cluster_per_word /
             max_nb_of_articles_outside_cluster)
        )
        score_for_words_in_cluster = score_for_all_words * names_in_cluster

        if np.sum(names_in_cluster) < label_length:
            score_for_words_in_cluster = score_for_all_words
            print(f"Warning: Less than {label_length} words in cluster "
                  f"{str(cluster_idx)} with "
                  f"({str(np.sum(names_in_cluster))}) words.")

        label_idx, label_name = choose_best_cluster_label(
            previous_cluster_labels, score_for_words_in_cluster,
            naming_scores, weight_name_length=weight_name_length
        )
        if label_name is None:
            print(
                "label_name is None"
                f"cluster index = {cluster_idx}\n i = {i}\n"
                f"label_name = {label_name}"
            )
            label_idx, label_name = choose_best_cluster_label(
                cluster_labels, score_for_words_in_cluster,
                naming_scores, weight_name_length=weight_name_length
            )
            print(f"Warning: had to reuse cluster name {label_name} "
                  f"(add to {str(cluster_labels)})")

        cluster_labels.append(label_name)
        cluster_labels_indices.append(label_idx)
        previous_cluster_labels.append(label_name)

        # If more than 75% articles are labeled by this term, stop
        prop_articles_labeled = (
            nb_of_articles_in_cluster_per_word[label_idx] /
            max_nb_articles_in_cluster
        )
        score_pos = score_pos + prop_articles_labeled

        prop_outside = (nb_of_articles_outside_cluster_per_word[label_idx]
                        / max_nb_of_articles_outside_cluster)

        score_neg = score_neg + prop_outside

        if prop_articles_labeled > 0.75:
            break

        # Otherwirse, remove articles in cluster that are labeled by this
        # term
        articles_in_cluster = articles_in_cluster - (
            articles_in_cluster *
            np.ravel(binary_naming_table.T[label_idx, :].todense())
        )

    if cluster_idx == 0:
        score_words = score_for_words_in_cluster
    else:
        score_words = score_words + score_for_words_in_cluster

    cluster_label = ", ".join(cluster_labels)

    return (cluster_label, max_nb_articles_in_cluster,
            cluster_labels_indices, previous_cluster_labels,
            score_words, score_pos, score_neg)


def choose_best_cluster_label(previous_cluster_labels,
                              score_for_words_in_cluster, naming_scores,
                              weight_name_length):
    """
    Pick the best label to name a cluster. We look at the 20 best labels by
    score, and update their score to favor longer n-grams. We then pick the
    first label which is not in previous_cluster_labels.

    Parameters
    ----------
    previous_cluster_labels: list
        A list of previous cluster labels, used to prevent duplicates in
    naming.
    score_for_words_in_cluster: numpy.ndarray
    naming_scores: pandas.core.series.Series
        The scores for the ``naming_table`` columns.
    weight_name_length: float

    Returns
    -------
    label_idx: int
        Index of the label.
    label_name: str
        Label as string.
    """
    best_words_in_cluster = np.argsort(score_for_words_in_cluster)[::-1]

    # Look at the best 20 words for the cluster
    sample_size = 20

    # Update their score based on word length
    updated_scores = [(
        score_for_words_in_cluster[idx] *
        (len(str(naming_scores.index[idx]).split(" ")) ** weight_name_length)
    ) for idx in best_words_in_cluster[:sample_size]
    ]
    updated_best_words_in_cluster = np.argsort(updated_scores)[::-1]
    label = None
    label_idx = 0

    for idx in updated_best_words_in_cluster:
        name = naming_scores.index[best_words_in_cluster[idx]]
        if name not in previous_cluster_labels:
            label = name
            label_idx = idx
            break

    # If no name worked, take the next best not already used...
    while label is None and sample_size < len(best_words_in_cluster):
        name = naming_scores.index[best_words_in_cluster[sample_size]]

        if name not in previous_cluster_labels:
            label = name
            label_idx = sample_size
        sample_size += 1

    return best_words_in_cluster[label_idx], label


def best_hdbscan_parameters(data,
                            nb_cluster,
                            min_cluster_size=15,
                            min_samples=None,
                            cache=None,
                            debug=True):  # TODO: change to False when tested
    """
    Select the best value of `min_cluster_size` to get approximatively
    `nb_cluster` cluster when applying hdbscan to a specified dataset.

    :param data: the data table.
    :param nb_cluster: the desired number of clusters.
    :param min_cluster_size : int, optional (default=15)
    The minimum number of samples in a group for that group to be
    considered a cluster; groupings smaller than this size will be
    left as noise.
    :param min_samples : int, optional (default=None)
        The number of samples in a neighborhood for a point
        to be considered as a core point. This includes the point itself.
        defaults to the min_cluster_size.

    :return:
      - actual_nb_cluster : int - The number of clusters computed, close to the
        desired nb_cluster
      - hdb : HDBSCAN - the hdb object with the desired number of clusters
    """
    if type(nb_cluster) != int or nb_cluster < 1:
        raise ValueError(
            "nb_cluster should be a positive integer"
        )
    if min_samples is None:
        min_samples = min_cluster_size

    if cache is None:
        cache = []

    hdb = hdbscan.HDBSCAN(
        min_cluster_size=min_cluster_size-1,  # avoid edge case
        min_samples=min_samples,
        memory=str(min_samples)
    )

    index = bisect_left(cache, (nb_cluster, min_cluster_size))
    max_info = None

    if len(cache) == 0:
        hdb.min_cluster_size = min_cluster_size  # avoid edge case
        if debug:
            print("Nothing in cache, initial Fitting with "
                  f"min_cluster_size={hdb.min_cluster_size} ", end="")
        start = timer()
        hdb.fit(data)
        duration = timer()-start
        cur_cluster_size = hdb.labels_.max()+1

        # Initialize best value
        best_info = (
            cur_cluster_size,
            hdb.min_cluster_size
        )
        if debug:
            print(f"Found {best_info[0]} clusters in {duration}s")
        cache.append(best_info)
    elif index < len(cache):
        best_info = cache[index]
        if debug:
            print(f"Already found best_info in cache: {best_info[0]}")
        if index != 0:
            max_info = cache[index-1]
            if debug:
                print(f"Already found max_info in cache: {max_info[0]}")
    else:
        best_info = cache[index-1]
        if debug:
            print(f"Too many clusters requested, max is: {best_info[0]}")

    # First, iterate to get under nb_cluster
    while max_info is None and best_info[0] > nb_cluster:
        hdb.min_cluster_size = best_info[1] * 2
        if debug:
            print("Max Fitting with "
                  f"min_cluster_size={hdb.min_cluster_size} ", end="")
        start = timer()
        hdb.fit(data)
        duration = timer()-start
        cur_cluster_size = hdb.labels_.max()+1
        if debug:
            print(f"Found {cur_cluster_size} clusters in {duration}s")

        if cur_cluster_size < nb_cluster:
            max_info = (
                cur_cluster_size,
                hdb.min_cluster_size
            )
            insort_left(cache, max_info)
        else:
            best_info = (
                cur_cluster_size,
                hdb.min_cluster_size
            )
            insort_left(cache, best_info)

    # Binary search between best and max
    while (best_info[0] > nb_cluster
           and abs(best_info[1] - max_info[1]) >= min_samples):
        hdb.min_cluster_size = (max_info[1] + best_info[1]) // 2
        if debug:
            print("Midpoint Fitting with "
                  f"min_cluster_size={hdb.min_cluster_size} ", end="")
        start = timer()
        hdb.fit(data)
        duration = timer()-start
        cur_cluster_size = hdb.labels_.max()+1
        if debug:
            print(f"Found {cur_cluster_size} clusters in {duration}s")

        if cur_cluster_size >= nb_cluster:
            # Still not under nb_cluster
            best_info = (
                cur_cluster_size,
                hdb.min_cluster_size
            )
            insort_left(cache, best_info)
        else:
            max_info = (
                cur_cluster_size,
                hdb.min_cluster_size
            )
            insort_left(cache, max_info)

    # If result was not the lastest computed, recompute the hdbscan object
    if hdb.min_cluster_size != best_info[1]:
        hdb.min_cluster_size = best_info[1]
        if debug:
            print("Re-Fitting with "
                  f"min_cluster_size={hdb.min_cluster_size} ", end="")
        start = timer()
        hdb.fit(data)
        duration = timer()-start
        cur_cluster_size = hdb.labels_.max()+1
        if debug:
            print(f"Found {cur_cluster_size} clusters in {duration}s")
        assert cur_cluster_size == best_info[0]
    elif debug:
        print("No need Re-Fitting with "
              f"min_cluster_size={hdb.min_cluster_size} ")

    if debug:
        print(f"Clusters cached: {[info[0] for info in cache]}")
    return best_info[0], hdb
