from abc import ABC, abstractmethod

from cartodata.api.stores import ModelStore


class APIConfiguration(ABC):

    def __init__(self, store: ModelStore, dataset_key: str):
        self.store = store
        self.dataset_key = dataset_key

    @abstractmethod
    def project(self, query):
        pass

    @abstractmethod
    def neighbors(self, vector, nature, n_neighbors):
        pass
