from typing import Dict, Type

import falcon
import numpy as np

from cartodata.api.configurations.configurations import APIConfiguration
from cartodata.api.stores import ModelStore


class DatasetContextMixin(object):
    def __init__(self, store: ModelStore,
                 configurations: Dict[str, Type[APIConfiguration]]):
        self.store = store
        self.configurations = configurations

    def get_conf(self, key: str):
        if key not in self.configurations:
            raise falcon.HTTPNotImplemented(
                description=(f"API Configuration for {key} dataset has not "
                             "been implemented")
            )

        return self.configurations[key](self.store, key)


class PositionsResource(DatasetContextMixin):

    def on_get(self, req, resp, key):
        conf = self.get_conf(key)
        query = req.get_param('query')
        latent, projection = conf.project(query)

        if latent is None or projection is None:
            resp.media = {'error': 'Unknown tokens'}
        else:
            resp.media = {'latent': latent.tolist(
            ), 'projection': projection.tolist()}
        resp.status = falcon.HTTP_200


class NeighborsResource(DatasetContextMixin):

    def on_post(self, req, resp, key):
        conf = self.get_conf(key)
        if (req.media is None or 'vector' not in req.media or 'nature'
                not in req.media):
            raise falcon.HTTPBadRequest(
                title='Missing body.',
                description=(
                    'The request body for neighbors must contain a '
                    'JSON object with "vector" and "nature" attributes.'
                )
            )

        vector = req.media.get('vector')
        nature = req.media.get('nature')
        n_neighbors = req.media.get('n') if 'n' in req.media else 10
        vector = np.array(vector)

        neighbors = conf.neighbors(vector, nature, n_neighbors)

        resp.media = {'neighbors': neighbors}
        resp.status = falcon.HTTP_200
