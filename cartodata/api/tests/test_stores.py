import os

from pyfakefs.fake_filesystem_unittest import TestCase
from unittest.mock import MagicMock

from cartodata.api.stores import ModelStore


class TestStores(TestCase):

    def setUp(self) -> None:
        self.dump_dir = 'dumps/mock'
        self.key = 'mydataset'
        self.store = ModelStore(self.dump_dir)
        self.setUpPyfakefs()

    def test_get_model_matrix(self):
        """

        :return:
        """
        mock_matrix = MagicMock()
        mock_matrix.nbytes = 1234567
        self.store._get_matrix = MagicMock(return_value=mock_matrix)
        model = self.store.get_model(self.key, 'articles', 'umap')

        self.assertEqual(model, mock_matrix)
        self.store._get_matrix.assert_called_with(self.key, 'articles', 'umap')
        self.store._get_matrix.reset_mock()

        model = self.store.get_model(self.key, 'articles', 'umap')
        self.assertEqual(model, mock_matrix)
        self.store._get_matrix.assert_not_called()
        self.assertEqual(self.store.size, 1234567)

    def test_get_model_scores(self):
        """

        :return:
        """
        mock_scores = MagicMock()
        mock_scores.nbytes = 1234567
        self.store._get_score_vector = MagicMock(return_value=mock_scores)
        model = self.store.get_model(self.key, 'words', 'scores')

        self.assertEqual(model, mock_scores)
        self.store._get_score_vector.assert_called_with(self.key, 'words')
        self.store._get_score_vector.reset_mock()

        model = self.store.get_model(self.key, 'words', 'scores')
        self.assertEqual(model, mock_scores)
        self.store._get_score_vector.assert_not_called()
        self.assertEqual(self.store.size, 1234567)

    def test_get_model_index(self):
        """

        :return:
        """
        mock_index = MagicMock()
        self.fs.create_file(os.path.join(
            self.dump_dir, self.key, 'words.index'), contents='test'
        )
        self.store._get_nn_index = MagicMock(return_value=mock_index)
        model = self.store.get_model(self.key, 'words', 'index')

        self.assertEqual(model, mock_index)
        self.store._get_nn_index.assert_called_with(self.key, 'words')
        self.store._get_nn_index.reset_mock()

        model = self.store.get_model(self.key, 'words', 'index')
        self.assertEqual(model, mock_index)
        self.store._get_nn_index.assert_not_called()
        self.assertEqual(self.store.size, 4)
